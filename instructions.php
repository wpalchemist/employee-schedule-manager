<?php

function wpaesm_instructions() { ?>
	<div class="wrap instructions">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h2><?php _e('Instructions for using the Employee Schedule Manager', 'wpaesm'); ?></h2>

		<a href="<?php echo plugin_dir_url( __FILE__ ) . 'Instructions_for_using_the_Employee_Schedule_Manager.pdf'; ?>" class="button primary-button"><?php _e( 'Download Instructions', 'wpaesm' ); ?></a>

		<h3><?php _e('Initial Set Up', 'wpaesm'); ?></h3>

		<h4><?php _e('Plugin Settings', 'wpaesm'); ?></h4>
		<p><?php _e('The plugin has a few settings that you might want to adjust.  In the WordPress dashboard menu, click on <a href="' . admin_url( 'page=employee-schedule-manager/options.php' ) . '">Employee Schedule Manager.</a>', 'wpaesm'); ?></p>
		<p><?php _e('The first several settings relate to the email notifications sent to employees. You can change the sender name, sender email, and message subject.', 'wpaesm'); ?></p>
		<p><?php _e('The "Admin Notifications" settings let you decide whether or not to receive a notification when an employee leaves a note on a shift, or when a shift changes status.  You can change the email address that receives these notifications.', 'wpaesm'); ?></p>
		<p><?php _e('You can select what day of the week your work-week starts on, whether or not you want to record employees\' location when they clock in and out, and other settings related to overtime hours and pay.  You can also turn on and off the email sent to employees to ask them to verify their payroll report.', 'wpaesm'); ?></p>
		<p><?php _e('You can choose whether the payroll report will be generated based on scheduled hours, or on actual hours worked.  If you choose "scheduled hours," then the payroll report will be based on the scheduled start and end times for the shift.  The following shift statuses will be included in the report: assigned, confirmed, tentative, worked.  If you choose to base the payroll on actual hours worked, then the clock-in and clock-out times will be used to calculate the payroll, and only shifts with the "worked" shift status will be added into the report.', 'wpaesm'); ?></p>

		<h4><?php _e('Set up shift types', 'wpaesm'); ?></h4>
		<p><?php _e('Shifts can be organized into shift types, such as "home", "school", or whatever names you want to give them.', 'wpaesm'); ?></p> 
		<p><?php _e('To create your shift types, go to <a href="' . admin_url( 'edit-tags.php?taxonomy=shift_type&post_type=shift') . '">Shifts --> Shift Types</a>.  Two shift types have already been created for you:', 'wpaesm'); ?>
			<ul>
				<li>
					<?php _e('Extra: automatically assigned to shifts that employees create themselves (such as when they\'re recording time they spend on documentation, etc.).', 'wpaesm'); ?>
				</li>
				<li>	
					<?php _e('Paid Time Off: If you want an employee to get paid time off, you can create a shift in this category, with a duration of the number of hours you want them to be paid for.', 'wpaesm'); ?>
				</li>
			</ul>
		</p>
		<p><?php _e('To create more shift types, fill in the "Add New Shift Type" form on the left side of the page.  You only need to fill in the name - you can ignore the "slug" field, and the "description" field is optional.', 'wpaesm'); ?></p>
		<p><?php _e('If you want to keep track of different kinds of "Extra" work, you can make shift types with "Extra" as their parent.  If you do this, the "Record Extra Work" form will let employees select what kind of extra work they are recording.', 'wpaesm'); ?></p> 

		<h4><?php _e('Set up shift statuses', 'wpaesm'); ?></h4>
		<p><?php _e('Shifts are also organized into statuses, such as "tentative," "assigned," and "worked."', 'wpaesm'); ?></p> 
		<p><?php _e('Several shift statuses have already been created for you:', 'wpaesm'); ?>
			<ul>
				<li>
					<?php _e('Assigned: Default status for a shift, indicates that this shift has been assigned to an employee', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Confirmed: Employee has confirmed this shift with the client\'s parents/school', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Tentative: Shift has been assigned, but not cast in stone', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Unassigned: No one has been assigned to work this shift', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Worked: Employee has worked this shift', 'wpaesm'); ?>
				</li>
			</ul>
		</p>
		<p><?php _e('Only shifts with the "Worked" shift status will appear in payroll reports and timesheets.', 'wpaesm'); ?>
		<p><?php _e('If you need additional shift statuses, you can create them by going to go to <a href="' . admin_url( 'edit-tags.php?taxonomy=shift_status&post_type=shift') . '">Shifts --> Shift Statuses</a> and using the form on the left side of the page.', 'wpaesm'); ?></p>
		<p><?php _e('Shift statuses have an extra field for color.  If you assign a color to a shift status, then that color will be used as the background color for shifts with that status on the schedule.  So you might want to give the "Tentative" status a light grey color, so that employees can easily see that the shift isn\'t cast in stone.  You will probably also want to assign a color to shifts that have been worked, so that you can easily see if someone missed a shift.  These colors are entirely optional.', 'wpaesm'); ?></p>
		<p><?php _e('To assign a color to an existing shift, click on the status name in the list of shift statuses.  Click in the "color" field, and a color wheel will appear.  When you like the color, click "Update."', 'wpaesm'); ?></p>

		<h4><?php _e('Create employees', 'wpaesm'); ?></h4>
		<p><?php _e('Next you need to set up your employees.  To do this, you will create User accounts for them.', 'wpaesm'); ?></p>
		<p><?php _e('Go to <a href="' . admin_url( 'user-new.php') . '">Users --> Add New.</a>', 'wpaesm'); ?></p>
		<p><?php _e('Enter the information about an employee: username, name, email, password (the employee will be able to change the password).  In the dropdown menu labeled "Role," make sure you select "Employee."', 'wpaesm'); ?></p>
		<p><?php _e('Click "Add New User."', 'wpaesm'); ?></p>
		<p><?php _e('Once you have created a user, there are some more fields you can fill in.  Click on <a href="' . admin_url( 'users.php') . '">Users --> All Users</a>.  Click on a user to edit them.  Scroll down to the section labeled "Employee Information."  There you can enter their address, phone number, and information about their pay rate, deductions, etc.', 'wpaesm'); ?></p>

		<h4><?php _e('Create clients', 'wpaesm'); ?></h4>
		<p><?php _e('Next you need to set up your clients.', 'wpaesm'); ?></p>
		<p><?php _e('You can create categories for your clients if you want - it is just like create shift types or shift statuses.  It might be useful to create categories for clients who attend camps.', 'wpaesm'); ?></p>
		<p><?php _e('To create a client, go to <a href="' . admin_url( 'post-new.php?post_type=client') . '">Clients --> Add New</a>.  For the title, enter the client\'s name.  You can enter a description about the client if you want.  ', 'wpaesm'); ?></p>
		<p><?php _e('Under the description, you will see a box labeled "Client Details."  This box has fields where you can enter informatino about the client.  You can upload a PDF file to the "Intake Form" field.  You can enter the diagnosis, triggers, and activities.  You can create as many behavior goals as you want, and select which ones are currently active - the goals you mark as active will appear on the documentation form so that employees can rank the client\'s progress.  There are also fields for the crisis plan and sub plan.', 'wpaesm'); ?></p>
		<p><?php _e('Under the "Client Details" box is a box for "Contact Information" where you can enter the contact details for the client.', 'wpaesm'); ?></p>
		<p><?php _e('In the right-hand sidebar, you will see several boxes.  Most of these boxes will be auto-populated as you create shifts, documentation, and expenses.  However, you might want to fill in some of these boxes.  Here are descriptions of these boxes:', 'wpaesm'); ?>
			<ul>
				<li>
					<?php _e('Client Category: If you are grouping your clients into categories, here is where you select what category or categories this client belongs to.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Connected Shifts: This box will auto-populate, so you do not need to enter anything into it.  This box will show you all of the shifts associated with a client.  This box is likely to get very long, so you might want to click the little triangle next to the box title to collapse the box.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Connected Expenses: This box will auto-populate, so you do not need to enter anything into it.  When employees enter expenses, they can enter the client associated with that expense: those expenses will show up here.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Connected Users: This box will <strong>not</strong> auto-populate.  To designate the primary employee assigned to a client, select the employee in this box.  Then, when that employee views their own profile, this client will show up as their client in their profile.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Connected Documents: This box will auto-populate, so you do not need to enter anything into it.  When employees write documentation about clients, the documents will show up in this box.  When you want to view all documentation associated with a particular employee, this is a good place to find it.  This box is also likely to get quite long, so you might want to click the triangle to collapse this box.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Featured Image: This is entirely optional, but you might want to upload a picture of the client here.  If you do, employees will see the picture when they view information about the client.', 'wpaesm'); ?>
				</li>
			</ul>

		</p>

		<h4><?php _e('Create pages', 'wpaesm'); ?></h4>
		<p><?php _e('You need to set up several pages to allow your employees to see information on the website.  On each of these pages, you will enter a shortcode.', 'wpaesm'); ?>
			<ul>
				<li>
					<?php _e('Master Schedule.  Create a page to display the master schedule.  The page title can be whatever you want, although "Master Schedule" makes sense.  On this page, enter the following shortcode:<br /><code>[master_schedule]</code>', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Your Schedule.  This page will display the schedule for the employee who is viewing the page (so if John Smith is viewing the site, this page will show him his schedule).  On this page, enter the following shortcode:<br /><code>[your_schedule]</code>', 'wpaesm'); ?>
				</li>
				<li>	
					<?php _e('Your Schedule.  This page will display the employee\'s user profile let them edit their password.  On this page, enter the following shortcode:<br /><code>[employee_profile]</code>', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Today. This page will display today\'s shifts to the employee who is viewing the page.  If they only have one shift scheduled for today, they will be automatically redirected to view that shift so they can clock in/out.  If they have more than one shift, they will see a list of today\'s shifts and can select the relevant one.  On this page, enter the following shortcode:<br /><code>[today]</code>', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Extra Work.  This page will display a form where employees can enter the date, start time, end time, and description of extra work they do that is not a part of a scheduled shift.  On this page, enter the following shortcode: <br /><code>[extra_work]</code>', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Record Expense. This page will let employees enter mileage and other expenses.  On this page, enter the following shortcode:<br /><code>[record_expense]</code>', 'wpaesm'); ?>
				</li>
			</ul>
		</p>

		<h3><?php _e('Creating the Schedule', 'wpaesm'); ?></h3>
		<p><?php _e('To create a single shift:', 'wpaesm'); ?>
			<ul>
				<li>
					<?php _e('Go to <a href="' . admin_url( 'post-new.php?post_type=shift') . '">Shifts --> Add Single Shift</a>.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Enter a title (this doesn\'t really matter and won\'t display anywhere on the website, but it\'s a good idea to make an informative title for the page that lists all shifts by title)', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Enter a description if you want - employees will see this description when they view the shift detail.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('In the shift details box, enter the date and times of the shift.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('If you check the box next to "notify employee", the employee will receive an email telling them this shift has been created.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('In the right sidebar, look for the box labeled "Connected Clients."  Click "Add Client" to choose a client.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('In the right sidebar, look for the box labeled "Connected Users."  Click "Add Employee" to choose the employee who will work this shift.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('You can choose a Shift Status in the right sidebar.  If you do not choose a status, the shift will default to "Tentative."', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('You can also choose a Shift Type in the right sidebar.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('When you are happy with all of these details, click "Publish."', 'wpaesm'); ?>
				</li>
			</ul>
		</p>
		<p><?php _e('To create a single shift:', 'wpaesm'); ?>
			<ul>
				<li>
					<?php _e('Go to <a href="' . admin_url( 'edit.php?post_type=shift&page=add-repeating-shifts' ) . '">Shifts --> Add Repeating Shifts</a>.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Enter the appropriate information.', 'wpaesm'); ?>
				</li>
				<li>
					<?php _e('Click "Create Shifts."', 'wpaesm'); ?>
				</li>
			</ul>
		</p>

		<h3><?php _e('Payroll Reports', 'wpaesm'); ?></h3>

		<p><?php _e('To generate a payroll report, go to <a href="' . admin_url('page=payroll-report') . '">Employee Schedule Manager --> Payroll Report</a>.  Select the date range for the report, and click "Generate Report".', 'wpaesm'); ?></p>
		<p><?php _e('Payroll reports will be emailed to employees on the 1st and 16th of every month.  To turn this feature on and off, view the "Email Payroll Report to Employees" setting on the plugin settings page.', 'wpaesm'); ?></p>

	</div>
<?php }