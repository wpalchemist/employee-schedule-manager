<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://ran.ge
 * @since      1.0.0
 *
 * @package    Shiftee_Text_Notifications
 * @subpackage Shiftee_Text_Notifications/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Shiftee_Text_Notifications
 * @subpackage Shiftee_Text_Notifications/includes
 * @author     Morgan Kay <morgan@ran.ge>
 */
class Shiftee_Text_Notifications {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Shiftee_Text_Notifications_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'shiftee-text-notifications';
		$this->version = '1.0.0';
		$this->wpaesm_options = get_option( 'wpaesm_options' );
		$this->shiftee_tn_options = get_option( 'shiftee_text_notifications_options' );

		$this->load_dependencies();
		$this->define_admin_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Shiftee_Text_Notifications_Loader. Orchestrates the hooks of the plugin.
	 * - Shiftee_Text_Notifications_i18n. Defines internationalization functionality.
	 * - Shiftee_Text_Notifications_Admin. Defines all hooks for the admin area.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-text-notifications-loader.php';

		/**
		 * The class responsible for the options page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-text-notifications-options.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-text-notifications-admin.php';

		$this->loader = new Shiftee_Text_Notifications_Loader();

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Shiftee_Text_Notifications_Admin( $this->get_plugin_name(), $this->get_version() );

		// Notifications from Shiftee Basic
//		$this->loader->add_action( 'shiftee_notify_employee_about_shift', $plugin_admin, 'employee_shift_notification', 3, 9 );
//		$this->loader->add_action( 'shiftee_save_employee_note_action', $plugin_admin, 'shift_note_notification', 5, 3 );
//		$this->loader->add_action( 'shiftee_clock_out_action', $plugin_admin, 'clock_out_notification', 5, 2 );
//		$this->loader->add_action( 'shiftee_add_extra_work_action', $plugin_admin, 'notify_admin_extra_shift', 5, 3 );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Shiftee_Text_Notifications_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
