<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://ran.ge
 * @since      1.0.0
 *
 * @package    Shiftee_Text_Notifications
 * @subpackage Shiftee_Text_Notifications/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Shiftee_Text_Notifications
 * @subpackage Shiftee_Text_Notifications/admin
 * @author     Morgan Kay <morgan@ran.ge>
 */
class Shiftee_Text_Notifications_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		if( class_exists( 'Shiftee_Helper' ) ) {
			$helper        = new Shiftee_Helper();
		}

		$this->shiftee_tn_options = get_option( 'shiftee_text_notifications_options' );
		$this->wpaesm_options = get_option( 'wpaesm_options' );

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/twilio/Services/Twilio.php';

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/shiftee-text-notifications-admin.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'estn_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	}


	/**
	 * Send a text message
	 *
	 * @param $recipient
	 * @param $message
	 */
	public function send_text_message( $recipient, $message ) {

		$AccountSid = $this->shiftee_tn_options['twilio_sid'];
		$AuthToken = $this->shiftee_tn_options['twilio_token'];

		$client = new Services_Twilio( $AccountSid, $AuthToken );

		if( 'admin' == $recipient ) {
			$formatted_to = $this->format_phone_number( $this->shiftee_tn_options['admin_number'] );
		} else {
			$user = get_user_by( 'id', $recipient );
			$phone = get_user_meta( $user->ID, 'phone', true );
			if( !$phone || '' == $phone ) {
			    return;
            }
			$formatted_to = $this->format_phone_number( $phone );
		}

		$formatted_from = $this->format_phone_number( $this->shiftee_tn_options['from_number'] );

		$client->account->messages->create(array(
			'To'       => $formatted_to,
			'From'     => $formatted_from,
			'Body'     => $message,
		));

	}

	/**
	 * Format a phone number to work with the Twilio API
	 *
	 * @param $phone_number
	 *
	 * @return string
	 */
	private function format_phone_number( $phone_number ) {

		$country_code = 1;

		$clean_phone = preg_replace( "/[^0-9]/","", $phone_number );

		$formatted_phone = '+' . $country_code . $clean_phone;

		return $formatted_phone;

	}


	/**
	 * Notify employee that a shift has been created/updated
	 *
	 * @see Shiftee
	 *
	 * @param $shift_id
	 */
	public function employee_shift_notification( $shift_id ) {

		$employeeid = $this->helper->get_shift_connection( $shift_id, 'employee', 'ID' );

		if( 'email' == $this->get_notification_preference( $employeeid ) ) {
			return;
		}

		if( 'text' == $this->get_notification_preference( $employeeid ) ) {
			global $Shiftee_Basic;
			remove_action( 'save_post', array( $Shiftee_Basic, 'run_that_action_last', 0 ) );
		}

		$date = get_post_meta( $shift_id, '_wpaesm_date', true );
		$starttime = get_post_meta( $shift_id, '_wpaesm_starttime', true );
		$endtime = get_post_meta( $shift_id, '_wpaesm_endtime', true );

		$message = sprintf( __( 'You have been scheduled to work on %s from %s to %s.  ', 'shiftee-text-notifications' ),
			$date,
			$starttime,
			$endtime );

		$children = get_pages( array( 'child_of' => $shift_id, 'post_type' => 'shift' ) );
		if( count( $children ) !== 0 ) {
			$message .= '<p>' . __( 'This is a repeating shift.', 'shiftee-text-notifications' ) . '</p>';
		}

		$message .= __( 'View this shift: ', 'shiftee-text-notifications' ) . esc_url( get_the_permalink( $shift_id ) );

		$this->send_text_message( $employeeid, $message );

	}

	/**
	 * Notify admin that employee left a note on a shift
	 *
	 * @see Shiftee
	 *
	 * @param $post
	 * @param $employee
	 * @param $oldstatus
	 */
	public function shift_note_notification( $post, $employee, $oldstatus ) {

		if( 'email' == $this->get_notification_preference( 'admin' ) ) {
			return;
		}

		if( 'text' == $this->get_notification_preference( 'admin' ) ) {
			global $Shiftee_Basic;
			remove_action( 'shiftee_save_employee_note_action', array( $Shiftee_Basic, 'employee_note_admin_notification', 10 ) );
		}

		if( isset( $this->wpaesm_options['admin_notify_status'] ) && 1 == $this->wpaesm_options['admin_notify_status'] ) {

			$message = sprintf( __( '%s just changed the status of a shift from %s to %s.  View shift: %s', 'shiftee-text-notifications' ),
				$employee,
				$oldstatus,
				sanitize_text_field( ['status'] ),
				esc_url( get_edit_post_link( $post->ID ) )
			);

			$this->send_text_message( 'admin', $message );
		}

		if( isset( $this->wpaesm_options['admin_notify_note']) && 1 == $this->wpaesm_options['admin_notify_note'] ) {

			$message = sprintf( __( '%s left a note on a shift.  View the shift: %s', 'shiftee-text-notifications' ),
				$employee,
				esc_url( get_edit_post_link( $post->ID ) )
				);

			$this->send_text_message( 'admin', $message );

		}

	}

	/**
	 * Notify admin that an employee clocked out
	 *
	 * @see Shiftee
	 *
	 * @param $shift_id
	 */
	public function clock_out_notification( $shift_id ) {

		if( 'email' == $this->get_notification_preference( 'admin' ) ) {
			return;
		}

		if( 'text' == $this->get_notification_preference( 'admin' ) ) {
			global $Shiftee_Basic;
			remove_action( 'shiftee_clock_out_action', array( $Shiftee_Basic, 'clock_out_notification', 10 ) );
		}

		if( isset( $this->wpaesm_options['admin_notify_clockout'] ) && 1 == $this->wpaesm_options['admin_notify_clockout'] ) {
			$users = get_users( array(
				'connected_type' => 'shifts_to_employees',
				'connected_items' => $shift_id
			) );
			foreach( $users as $user ) {
				$employeename = $user->display_name;
			}
			if( !isset( $employeename ) ) {
				$employeename = __( 'An Employee', 'wpaesm' );
			}

			$scheduled = esc_attr( get_post_meta( $shift_id, '_wpaesm_starttime', true ) ) . ' - ' . esc_attr( get_post_meta( $shift_id, '_wpaesm_endtime', true ) );
			$worked =  esc_attr( get_post_meta( $shift_id, '_wpaesm_clockin', true ) ) . ' - ' . esc_attr( get_post_meta( $shift_id, '_wpaesm_clockout', true ) );

			$message = sprintf( __( '%s has just clocked out of their shift.  Scheduled hours: %s.  Worked hours: %s.  View shift: %s', 'shiftee-text-notifications' ),
				$employeename,
				$scheduled,
				$worked,
				esc_url( get_the_permalink( $shift_id ) )
				);

			$this->send_text_message( 'admin', $message );

		}

	}

	/**
	 * Notify admin that an employee has entered an extra shift
	 *
	 * @see Shiftee
	 *
	 * @param $shift_id
	 * @param $employee
	 */
	public function notify_admin_extra_shift( $shift_id, $employee ) {

		if( 'email' == $this->get_notification_preference( 'admin' ) ) {
			return;
		}

		if( 'text' == $this->get_notification_preference( 'admin' ) ) {
			global $Shiftee_Basic;
			remove_action( 'shiftee_add_extra_work_action', array( $Shiftee_Basic, 'notify_admin_extra_shift', 10 ) );
		}

		if( isset( $this->wpaesm_options['extra_shift_approval'] ) && '1' == $this->wpaesm_options['extra_shift_approval'] ) {

			$message = sprintf( __( '%s has entered an extra shift that is awaiting your approval: %s', 'shiftee-text-notifications' ),
				$employee->display_name,
				esc_url( get_edit_post_link( $shift_id ) )
			);

			$this->send_text_message( 'admin', $message );
		}

	}



}
