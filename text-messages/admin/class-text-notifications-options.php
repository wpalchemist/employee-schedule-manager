<?php
/**
 * The options class.
 *
 * Creates the Witlee options page and saves the data
 *
 * @since      1.0.0
 * @package    Shiftee_Text_Notifications
 * @subpackage Shiftee_Text_Notifications/admin
 */

class Text_Notification_Options {
	private $shiftee_tn_options;

	/**
	 * Text_Notification_Options constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'text_notification_settings_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'text_notification_settings_page_init' ) );

		$this->shiftee_tn_options = get_option( 'shiftee_text_notifications_options' );
	}

	/**
	 * Add the settings page to the Shiftee menu
	 *
	 * @since 1.0.0
	 */
	public function text_notification_settings_add_plugin_page() {
		add_submenu_page(
			'/employee-schedule-manager/options.php',
			__( 'Text Notification Settings', 'shiftee-text-notifications' ),
			__( 'Text Notification Settings', 'shiftee-text-notifications' ),
			'manage_options',
			'shiftee-tn-settings',
			array( $this, 'text_notification_settings_create_admin_page' )
		);
	}

	public function text_notification_settings_create_admin_page() {
		 ?>

		<div class="wrap">
			<h1><?php _e( 'Text Notification Settings', 'shiftee-text-notifications' ); ?></h1>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
				settings_fields( 'text_notification_settings_option_group' );
				do_settings_sections( 'text-notification-settings-admin' );
				submit_button();
				?>
			</form>
		</div>
	<?php }

	public function text_notification_settings_page_init() {
		register_setting(
			'text_notification_settings_option_group', // option_group
			'shiftee_text_notifications_options', // option_name
			array( $this, 'text_notification_settings_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'text_notification_settings_setting_section', // id
			'Settings', // title
			array( $this, 'text_notification_settings_section_info' ), // callback
			'text-notification-settings-admin' // page
		);

		add_settings_field(
			'twilio_sid', // id
			__( 'Twilio Account SID', 'shiftee-text-notifications' ), // title
			array( $this, 'twilio_sid_callback' ), // callback
			'text-notification-settings-admin', // page
			'text_notification_settings_setting_section', // section
			array(
				'description' => sprintf(
					__( 'You must have a <a href="%s">Twilio account</a> to use Text Notifications.  Enter your LIVE <a href="%s">Account SID</a> here.', 'shiftee-text-notifications' ),
					'https://www.twilio.com',
					'https://www.twilio.com/console'
				),
			)
		);

		add_settings_field(
			'twilio_token', // id
			__( 'Twilio Auth Token', 'shiftee-text-notifications' ), // title
			array( $this, 'twilio_token_callback' ), // callback
			'text-notification-settings-admin', // page
			'text_notification_settings_setting_section', // section
			array(
				'description' => sprintf(
					__( 'Enter your LIVE Twilio <a href="%s">Auth Token</a> here.', 'shiftee-text-notifications' ),
					'https://www.twilio.com/console'
				),
			)
		);

		add_settings_field(
			'from_number', // id
			__( 'From Phone Number', 'shiftee-text-notifications' ), // title
			array( $this, 'from_number_callback' ), // callback
			'text-notification-settings-admin', // page
			'text_notification_settings_setting_section', // section
			array(
				'description' => __( 'Enter the phone number that text notifications will come from.  This number must be a valid Twilio number.', 'shiftee-text-notifications' ),
			)
		);

//		add_settings_field(
//			'admin_number', // id
//			__( 'Admin Phone Number', 'shiftee-text-notifications' ), // title
//			array( $this, 'admin_number_callback' ), // callback
//			'text-notification-settings-admin', // page
//			'text_notification_settings_setting_section', // section
//			array(
//				'description' => __( 'Enter the phone number that will receive text notifications sent to the site admin', 'shiftee-text-notifications' ),
//			)
//		);
	}

	public function text_notification_settings_sanitize( $input ) {

		if( isset( $input['twilio_sid'] ) ) {
			$input['twilio_sid'] = sanitize_text_field( $input['twilio_sid'] );
		}

		if( isset( $input['twilio_token'] ) ) {
			$input['twilio_token'] = sanitize_text_field( $input['twilio_token'] );
		}

		// validate Twilio credentials
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/twilio/Services/Twilio.php';
		$client = new Pricing_Services_Twilio( $input['twilio_sid'], $input['twilio_token'] );
		$country = $client->phoneNumberCountries->get("US");
		if( !is_object( $country ) ) {
			$message = __( 'Invalid Twilio credentials. Please check your Twilio SID and Account Token and try again.', 'shiftee-text-notifications' );
			add_settings_error( 'text_notification_settings_option_group', 'twilio-auth', $message, 'error' );
			$input['twilio_sid'] = '';
			$input['twilio_token'] = '';
		} else {
			$input['twilio_credentials'] = 'valid';
		}

		if( isset( $input['from_number'] ) ) {
			$input['from_number'] = sanitize_text_field( $input['from_number'] );
//			$client = new Lookups_Services_Twilio( $input['twilio_sid'], $input['twilio_token'] );
//			$number = $client->phone_numbers->get ($input['from_number'],  array( "Type" => "carrier" ) );
//			wp_die( '<pre>' . print_r( $number, true ));
//			if( 'Twilio' !== $number->carrier->name ) {
//				$message = __( 'Invalid "From" phone number. Please check your phone number is a valid Twilio number and try again.', 'shiftee-text-notifications' );
//				add_settings_error( 'text_notification_settings_option_group', 'twilio-phone', $message, 'error' );
//				$input['from_number'] = '';
//			} else {
//				$input['twilio_phone'] = 'valid';
//			}
		}

		if( isset( $input['admin_number'] ) ) {
			$input['admin_number'] = sanitize_text_field( $input['admin_number'] );
		}

		return $input;
	}

	public function text_notification_settings_section_info() {

	}

	public function twilio_sid_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="text" name='shiftee_text_notifications_options[twilio_sid]'
			       value='<?php echo $this->shiftee_tn_options['twilio_sid']; ?>'
			       id="estn-twilio-sid">
		</p>
		<?php
	}

	public function twilio_token_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="text" name='shiftee_text_notifications_options[twilio_token]'
			       value='<?php echo $this->shiftee_tn_options['twilio_token']; ?>'
			       id="estn-twilio-token">
		</p>
		<?php
	}


	public function from_number_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="text" name='shiftee_text_notifications_options[from_number]'
			       value='<?php echo $this->shiftee_tn_options['from_number']; ?>'
			       id="estn-from-number">
		</p>
		<?php
	}

	public function admin_number_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="text" name='shiftee_text_notifications_options[admin_number]'
			       value='<?php echo $this->shiftee_tn_options['admin_number']; ?>'
			       id="estn-admin-number">
		</p>
		<?php
	}

}

if ( is_admin() ) {
	$text_notification_settings = new Text_Notification_Options();
}


