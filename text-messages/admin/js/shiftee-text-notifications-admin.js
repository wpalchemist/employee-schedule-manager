(function( $ ) {
	'use strict';

	$( window ).load(function() {
		jQuery(document).on( 'click', '.estn-invalid-twilio-creds .notice-dismiss', function() {

			jQuery.ajax({
				url: shiftee_tn_ajax.ajaxurl,
				data: {
					action: 'dismiss_twilio_creds_nag'
				}
			})

		})

		jQuery(document).on( 'click', '.estn-invalid-twilio-phone .notice-dismiss', function() {

			jQuery.ajax({
				url: shiftee_tn_ajax.ajaxurl,
				data: {
					action: 'dismiss_twilio_phone_nag'
				}
			})

		})
	});


})( jQuery );
