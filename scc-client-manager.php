<?php

// ------------------------------------------------------------------------
// CREATE CUSTOM POST TYPES
// ------------------------------------------------------------------------

// Register Custom Taxonomy
function wpaesm_register_tax_customer() {

    $labels = array(
        'name'                       => _x( 'Customers', 'Taxonomy General Name', 'wpaesm' ),
        'singular_name'              => _x( 'Customer', 'Taxonomy Singular Name', 'wpaesm' ),
        'menu_name'                  => __( 'Customers/School Districts', 'wpaesm' ),
        'all_items'                  => __( 'All Customers', 'wpaesm' ),
        'parent_item'                => __( 'Parent Customer', 'wpaesm' ),
        'parent_item_colon'          => __( 'Parent Customer:', 'wpaesm' ),
        'new_item_name'              => __( 'New CustomerName', 'wpaesm' ),
        'add_new_item'               => __( 'Add New Customer', 'wpaesm' ),
        'edit_item'                  => __( 'Edit Customer', 'wpaesm' ),
        'update_item'                => __( 'Update Customer', 'wpaesm' ),
        'view_item'                  => __( 'View Customer', 'wpaesm' ),
        'separate_items_with_commas' => __( 'Separate Customers with commas', 'wpaesm' ),
        'add_or_remove_items'        => __( 'Add or remove Customers', 'wpaesm' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'wpaesm' ),
        'popular_items'              => __( 'Popular Customers', 'wpaesm' ),
        'search_items'               => __( 'Search Customers', 'wpaesm' ),
        'not_found'                  => __( 'Not Found', 'wpaesm' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'customer', array( 'client' ), $args );

}
add_action( 'init', 'wpaesm_register_tax_customer', 0 );

// Add custom fields to client
$config = array(
   'id' => 'customer_meta_box',                         // meta box id, unique per meta box
   'title' => 'Customer Meta Box',                      // meta box title
   'pages' => array('customer'),                    // taxonomy name, accept categories, post_tag and custom taxonomies
   'context' => 'normal',                           // where the meta box appear: normal (default), advanced, side; optional
   'fields' => array(),                             // list of meta fields (can be added by field arrays)
   'local_images' => false,                         // Use local or hosted images (meta box images for add/remove)
   'use_with_theme' => false                        //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
);

$customer_meta = new Tax_Meta_Class($config);

$customer_meta->addText('hourly_rate',array('name'=> 'Hourly Rate '));


// Register Custom Post Type
function wpaesm_register_cpt_document() {

    $labels = array(
        'name'                => _x( 'Documents', 'Post Type General Name', 'wpaesm' ),
        'singular_name'       => _x( 'Document', 'Post Type Singular Name', 'wpaesm' ),
        'menu_name'           => __( 'Documents', 'wpaesm' ),
        'parent_item_colon'   => __( 'Parent Document:', 'wpaesm' ),
        'all_items'           => __( 'All Documents', 'wpaesm' ),
        'view_item'           => __( 'View Document', 'wpaesm' ),
        'add_new_item'        => __( 'Add New Document', 'wpaesm' ),
        'add_new'             => __( 'Add New', 'wpaesm' ),
        'edit_item'           => __( 'Edit Document', 'wpaesm' ),
        'update_item'         => __( 'Update Document', 'wpaesm' ),
        'search_items'        => __( 'Search Document', 'wpaesm' ),
        'not_found'           => __( 'Not found', 'wpaesm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'wpaesm' ),
    );
    $args = array(
        'label'               => __( 'document', 'wpaesm' ),
        'description'         => __( 'Documentation about Clients', 'wpaesm' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', ),
        'hierarchical'        => true,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => true,
        'menu_position'       => 70,
        'menu_icon'           => 'dashicons-media-text',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'page',
    );
    register_post_type( 'document', $args );

}

// Hook into the 'init' action
add_action( 'init', 'wpaesm_register_cpt_document', 0 );

// register document status
function wpaesm_register_tax_doc_status() {

    $labels = array(
        'name'                       => _x( 'Document Statuses', 'Taxonomy General Name', 'wpaesm' ),
        'singular_name'              => _x( 'Document Status', 'Taxonomy Singular Name', 'wpaesm' ),
        'menu_name'                  => __( 'Document Statuses', 'wpaesm' ),
        'all_items'                  => __( 'All Document Statuses', 'wpaesm' ),
        'parent_item'                => __( 'Parent Document Status', 'wpaesm' ),
        'parent_item_colon'          => __( 'Parent Document Status:', 'wpaesm' ),
        'new_item_name'              => __( 'New Document Status Name', 'wpaesm' ),
        'add_new_item'               => __( 'Add New Document Status', 'wpaesm' ),
        'edit_item'                  => __( 'Edit Document Status', 'wpaesm' ),
        'update_item'                => __( 'Update Document Status', 'wpaesm' ),
        'separate_items_with_commas' => __( 'Separate Document Statuses with commas', 'wpaesm' ),
        'search_items'               => __( 'Search Document Statuses', 'wpaesm' ),
        'add_or_remove_items'        => __( 'Add or remove Document Statuses', 'wpaesm' ),
        'choose_from_most_used'      => __( 'Choose from the most used Document Statuses', 'wpaesm' ),
        'not_found'                  => __( 'Not Found', 'wpaesm' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'doc_status', array( 'document' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'wpaesm_register_tax_doc_status', 0 );


// ------------------------------------------------------------------------
// CREATE CUSTOM METABOXES
// ------------------------------------------------------------------------

$client_details = new WPAlchemy_MetaBox(array
(
    'id' => 'client_details',
    'title' => 'Client Details',
    'types' => array('client'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/clientdetails.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_'
));

$client_metabox = new WPAlchemy_MetaBox(array
(
    'id' => 'client_meta',
    'title' => 'Contact Information',
    'types' => array('client'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/clientinfo.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_'
));

$document_metabox = new WPAlchemy_MetaBox(array
(
    'id' => 'document_meta',
    'title' => 'Behavior Goal Ratings',
    'types' => array('document'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/documentmeta.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_'
));

$document_history = new WPAlchemy_MetaBox(array
(
    'id' => 'document_history',
    'title' => 'Document History',
    'types' => array('document'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/documenthistory.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_'
));


// ------------------------------------------------------------------------
// CONNECT DOCUMENTS TO CLIENTS, SHIFTS, AND EMPLOYEES
// https://github.com/scribu/wp-posts-to-posts/blob/master/posts-to-posts.php
// ------------------------------------------------------------------------


function wpaesm_documents_create_connections() {
    // create the connection between documents and employees
    p2p_register_connection_type( array(
        'name' => 'documents_to_employees',
        'from' => 'document',
        'to' => 'user',
        'cardinality' => 'many-to-one',
//        'admin_column' => 'from',
        'to_labels' => array(
            'singular_name' => __( 'Employee', 'wpaesm' ),
            'search_items' => __( 'Search employees', 'wpaesm' ),
            'not_found' => __( 'No employees found.', 'wpaesm' ),
            'create' => __( 'Add Employee', 'wpaesm' ),
        ),
    ) );

    // create the connection between clients and employees
    p2p_register_connection_type( array(
        'name' => 'clients_to_employees',
        'from' => 'client',
        'to' => 'user',
        'cardinality' => 'many-to-many',
        'admin_column' => 'any',
        'to_labels' => array(
            'singular_name' => __( 'Employee', 'wpaesm' ),
            'search_items' => __( 'Search employees', 'wpaesm' ),
            'not_found' => __( 'No employees found.', 'wpaesm' ),
            'create' => __( 'Add Employee', 'wpaesm' ),
        ),
    ) );

    // create the connection between documents and clients
    p2p_register_connection_type( array(
        'name' => 'documents_to_clients',
        'from' => 'document',
        'to' => 'client',
        'cardinality' => 'many-to-one',
        'admin_column' => 'from',
        'to_labels' => array(
            'singular_name' => __( 'Client', 'wpaesm' ),
            'search_items' => __( 'Search clients', 'wpaesm' ),
            'not_found' => __( 'No clients found.', 'wpaesm' ),
            'create' => __( 'Add Client', 'wpaesm' ),
        ),
    ) );

    // create the connection between documents and shifts
    p2p_register_connection_type( array(
        'name' => 'documents_to_shifts',
        'from' => 'document',
        'to' => 'shift',
        'cardinality' => 'many-to-one',
        'admin_column' => 'any',
        'to_labels' => array(
            'singular_name' => __( 'Shift', 'wpaesm' ),
            'search_items' => __( 'Search shifts', 'wpaesm' ),
            'not_found' => __( 'No shifts found.', 'wpaesm' ),
            'create' => __( 'Add Shift', 'wpaesm' ),
        ),
        'from_labels' => array(
            'singular_name' => __( 'Document', 'wpaesm' ),
            'search_items' => __( 'Search documents', 'wpaesm' ),
            'not_found' => __( 'No documents found.', 'wpaesm' ),
            'create' => __( 'Add Document', 'wpaesm' ),
        ),
    ) );

    }
add_action( 'p2p_init', 'wpaesm_documents_create_connections' );



// Add shift type column to document overview page
add_filter( 'manage_document_posts_columns', 'wpaesm_add_shift_type_column_to_documents', 10, 2 );
function wpaesm_add_shift_type_column_to_documents( $columns ) {

    $columns[ 'shift_type' ] = 'Shift Type';
        
    return $columns;
    
}


add_filter( 'manage_edit-document_sortable_columns', 'wpaesm_make_document_shift_type_column_sortable' );
function wpaesm_make_document_shift_type_column_sortable( $sortable_columns ) {

    $sortable_columns[ 'shift_type' ] = 'shift_type';
    
    return $sortable_columns;
    
}

add_action( 'manage_document_posts_custom_column', 'wpaesm_display_document_shift_type', 10, 2 );
function wpaesm_display_document_shift_type( $column_name, $post_id ) {

    switch( $column_name ) {
    
        case 'shift_type':
        
            $shift_type = '';
            // Find connected pages
            $connected = new WP_Query( array(
              'connected_type' => 'documents_to_shifts',
              'connected_items' => $post_id,
              'nopaging' => true,
            ) );

            // Display connected pages
            if ( $connected->have_posts() ) :
                while ( $connected->have_posts() ) : $connected->the_post();
                    $statuses = wp_get_post_terms( get_the_id(), 'shift_type' );
                    foreach( $statuses as $status ) {
                        $shift_type .= $status->name . '<br />';
                    }
                endwhile; 
            wp_reset_postdata();

            endif;
            echo '<div id="shift_type-' . $post_id . '">' . $shift_type . '</div>';
            break;
            
    }
    
}


function wpaesm_exclude_archived_clients( $query ) {
    if ( $query->is_post_type_archive( 'client' ) && $query->is_main_query() && is_admin() ) {

        $tax_query = array(
            array(
                'taxonomy' => 'client_category',
                'field'    => 'slug',
                'terms'    => 'archived',
                'operator' => 'NOT IN'
            ),
        );

        $query->set( 'tax_query', $tax_query );
    }
}
add_action( 'pre_get_posts', 'wpaesm_exclude_archived_clients' );


add_action( 'gform_after_submission_8', 'wpaesm_create_client', 10, 2 );
function wpaesm_create_client( $entry, $form ) {

	// $entry[2] should be the camper name
	if( isset( $entry[2] ) && '' !== $entry[2] ) {
		$args = array(
			'post_title' => esc_html( $entry[2] ),
			'post_content' => 'Client created automatically from Camp Registration Form entry #' . $entry['id'],
			'post_status' => 'draft',
			'post_type' => 'client'
		);

		$new_client = wp_insert_post( $args );

		if( !is_wp_error( $new_client ) ) {
			$client_details_array = wpaesm_get_client_details_array();
			add_post_meta( $new_client, 'client_details_fields', $client_details_array );

			// diagnosis
			if( isset( $entry[25] ) && '' !== $entry[25] ) {
				add_post_meta( $new_client, '_wpaesm_diagnosis', sanitize_text_field( $entry[25] ) );
			}

			// activities
			if( isset( $entry[54] ) && '' !== $entry[54] ) {
				add_post_meta( $new_client, '_wpaesm_activities', sanitize_text_field( $entry[54] ) );
			}

			// goals
			if( isset( $entry[60] ) && '' !== $entry[60] ) {
				$goals = maybe_unserialize( $entry[60] );
				$goals_to_save = array();
				foreach( $goals as $goal ) {
					$goals_to_save[] = array(
						'goal_name' => sanitize_text_field( $goal ),
						'active' => 'active',
					);
				}
				add_post_meta( $new_client, '_wpaesm_goal', $goals_to_save );
			}

			// crisis plan
			if( isset( $entry[57] ) && '' !== $entry[57] ) {
				add_post_meta( $new_client, '_wpaesm_crisis', sanitize_text_field( $entry[57] ) );
			}


			$client_meta_array = wpaesm_get_client_meta_array();
			add_post_meta( $new_client, 'client_meta_fields', $client_meta_array );

			// address
			if( isset( $entry['5.1'] ) && '' !== $entry['5.1'] ) {
				$address = array(
					'addressname' => 'home',
					'address' => sanitize_text_field( $entry['5.1'] ),
				);
				if( isset( $entry['5.3'] ) ) {
					$address['city'] = sanitize_text_field( $entry['5.3'] );
				}
				if( isset( $entry['5.4'] ) ) {
					$address['state'] = sanitize_text_field( $entry['5.4'] );
				}
				if( isset( $entry['5.5'] ) ) {
					$address['zip'] = sanitize_text_field( $entry['5.5'] );
				}
				add_post_meta( $new_client, '_wpaesm_address', array( $address ) );
			}

			// parent
			if( isset( $entry[7] ) ) {
				$parent1 = array(
					'name'     => sanitize_text_field( $entry[7] ),
					'relation' => 'Parent/Guardian 1',
					'phone' => array(),
				);

				if( isset( $entry[8] ) ) {
					$parent1['phone'][] = array(
						'phonenumber' => sanitize_text_field( $entry[8] ),
						'phonetype'   => 'home',
					);
				}

				if( isset( $entry[10] ) ) {
					$parent1['phone'][] = array(
						'phonenumber' => sanitize_text_field( $entry[10] ),
						'phonetype'   => 'cell',
					);
				}

				$parent2 = '';
				if( isset( $entry[11] ) ) {
					$parent2 = array(
						'name'     => sanitize_text_field( $entry[11] ),
						'relation' => 'Parent/Guardian 2',
						'phone' => array(),
					);

					if( isset( $entry[12] ) ) {
						$parent2['phone'][] = array(
							'phonenumber' => sanitize_text_field( $entry[12] ),
							'phonetype'   => 'home',
						);
					}

					if( isset( $entry[13] ) ) {
						$parent2['phone'][] = array(
							'phonenumber' => sanitize_text_field( $entry[13] ),
							'phonetype'   => 'cell',
						);
					}
				}

				$emergency = '';
				if( isset( $entry[17] ) ) {
					$emergency = array(
						'name'     => sanitize_text_field( $entry[17] ),
						'relation' => 'Emergency Contact',
						'phone' => array(),
					);

					if( isset( $entry[18] ) ) {
						$emergency['phone'][] = array(
							'phonenumber' => sanitize_text_field( $entry[18] ),
							'phonetype'   => 'cell',
						);
					}
				}

				$parents = array( $parent1, $parent2, $emergency );
				add_post_meta( $new_client, '_wpaesm_parent', $parents );
			}

			// email
			if( isset( $entry[14] ) && ''!== $entry[14] ) {
				$email = array(
					'emailaddress' => sanitize_text_field( $entry[14] )
				);
				add_post_meta( $new_client, '_wpaesm_clientemail', $email );
			}
		}
	}

}

function wpaesm_get_client_details_array() {
	$client_details = array(
		'_wpaesm_intake',
		'_wpaesm_diagnosis',
		'_wpaesm_triggers',
		'_wpaesm_activities',
		'_wpaesm_restraint',
		'_wpaesm_restraint_details',
		'_wpaesm_goal',
		'_wpaesm_sub',
		'_wpaesm_crisis'
    );
	$str = $client_details;
	return $str;
}

function wpaesm_get_client_meta_array() {
	$client_meta = array(
		'_wpaesm_school',
		'_wpaesm_funder_name',
		'_wpaesm_funder_shcool',
		'_wpaesm_funder_organization',
		'_wpaesm_address',
		'_wpaesm_clientphone',
		'_wpaesm_clientemail',
		'_wpaesm_parent'
	);
	$str = $client_meta;
	return $str;
}

?>