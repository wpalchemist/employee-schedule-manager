=== Employee Schedule Manager: SCC Version ===
Contributors: gwendydd
Tags: employees, schedule, timesheet
Requires at least: 3.6
Tested up to: 3.9.2
Stable tag: 0.1


== Description ==

This version of the plugin is created specifically for Seattle Community Care to meet their needs.


== Changelog ==
= 2.5.11 =
* new dates on spreadsheet

= 2.5.10 =
* fix bug that broke spreadsheet downloads

= 2.5.9.=
* add field for CIC miles to clock out

= 2.5.8 =
* add more camp dates to Google Sheets integration with camp registration form

= 2.5.7 =
* Additional fields to clock in/out to determine why employee is early/late
* Add Google Sheets integration to camp registration form

= 2.5.6 =
* add "am/pm" to Your Schedule
* limit payroll report and invoice report to Eric, Yolanda, and Morgan

= 2.5.5 =
* add debugging to clock in/out text messages

= 2.5.4 =
* changes to print stylesheet

= 2.5.3 =
* add "undo" button to "this shift does not need documentation"

= 2.5.2 =
* change permalink structure of clients

= 2.5.1 =
* update text of shift notification email

= 2.5.0 =
* camp registration form will create new client

= 2.4.9 =
* change text of shift notification email

= 2.4.8 =
* send text messages to employees when they clock in or out late, or when documentation is late

= 2.4.7 =
* change PTO calculation to accommodate sick leave

= 2.4.6 =
* don't archive clients

= 2.4.5 =
* change documentation due date to 24 hours instead of one week

= 2.4.2 =
* add hours scheduled and hours worked to master schedule
* hide client archives

= 2.4.1 =
* Fix array_keys error in WP 4.7.1

= 2.4.0 =
* New incident report procedure

= 2.3.7 =
* make sure all lists of employees are alphabetized by first name
* do not send extra shift notification to admin

= 2.3.6 =
* Exclude employees from payroll report if they have not worked any hours in report timeframe

= 2.3.5 =
* correct former employee role slug on payroll report

= 2.3.4 =
* payroll report includes former employees

= 2.3.3 =
* invoice report now correctly reports hours if there is more than one shift per client in a day

= 2.3.2 =
* ability to limit invoice report by customer

= 2.3.1 =
* tweaks to make document doc display correctly

= 2.3.0 =
* added ability to download a Word doc of client documents
* remove documentation shifts from invoice report

= 2.2.11 =
* adjusted date format on master schedule

= 2.2.10 =
* fixed bug in dashboard widget

= 2.2.9 =
* added client category selection to invoice report form

= 2.2.8 =
* added random characters to slug of shifts created with bulk shift creator to make sure there aren't any duplicate permalinks

= 2.2.7 =
* all extra shifts are now documentation shifts

= 2.2.6 =
* fix scheduled/worked calculation on OT calculation

= 2.2.5 =
* fix scheduled/worked calculation

= 2.2.4 =
* make employee timesheets calculate the same was as admin timesheets

= 2.2.3 =
* archive now gets anything older than 6 months

= 2.2.2 =
* stop timesheet from displaying extra day (theme)
* connection between shift and employee no longer limited to employee user role

= 2.2.1 =
* documentation page only shows worked shifts

= 2.2 =
* send email reminder if employee hasn't submitted timesheet 
* added "former employee" user role
* added archiving function: once a month, everything more than a year old will be deleted
* "delete all shifts" now only deletes subsequent shifts
* documentation page shows all shifts in the past, not just worked shifts
* fixed bug that prevented employee queries from working

= 2.1.4 =
* made timesheet activity column sortable

= 2.1.3 =
* added column to users page to display most recent timesheet activity

= 2.1.2 =
* hopefully fixed disappearing clockin/out data once and for all
* made unapproved docs and undocumented shifts pages sortable
* only users with employee role show up in "connected employee" box

= 2.1.1=
* fixed bug in overtime calculation

= 2.1.0 =
* single shift creator and bulk shift creator check for client before saving shift(s)

= 2.0.0 =
* Added option to require admin approval for extra shifts

= 1.9.8 =
* timesheet and payroll only show worked shifts

= 1.9.7 =
* removed expense category dropdown from expense report form

= 1.9.6 =
* fixed bug that made clockin/out data disappear when documents are saved

= 1.9.5 =
* remove company vehicle miles from expense report form
* bulk shift updater no longer changes shift status
* fixed bug on "view shifts" page that prevented all days from showing up

= 1.9.4 =
* made timesheets pick up assigned shifts so that timesheets match payroll reports

= 1.9.3 =
* moved break functions from theme to plugin
* temporarily sending emails to me every time someone clocks out

= 1.9.2 =
* made personal miles total include wrap to home miles
* fixed bugs in payroll report overtime calculation

= 1.9.1 =
* made sure undocumented shifts page won't misreport client

= 1.9 =
* removed backup shifts from payroll
* created page in admin to show schedules (Shifts --> View Schedules)

= 1.8 =
* improved error messages more
* payroll report is sorted alphabetically, adminwpa and community excluded

= 1.7 =
* improved error messages for clock in and clock out

= 1.6 =
* error messages if clock in and clock out times are not saved
* undocumented shifts page shows when last reminder was sent

= 1.5 =
* Finished unapproved documents page
* Added undocumented shifts page

= 1.4 =
* totally revamped documentation
* email function now handles attachments

= 1.3.3 =
* extra and pto shifts now show up on schedules

= 1.3.2 =
* shift title displays on master schedule and your schedule
* archived clients don't show up on client overview page

= 1.3 =
* payroll report is downloadable as .xlsx

= 1.2.1 =
* fixed bug in invoice report that made shifts show up repeatedly
* fixed wpp2p compatibility with WP 4.3

= 1.2 =
* HTML emails
* fixed bug on PTO minutes calculation
* fixed bug that sometimes prevented invoice report from downloading
* make timesheet/payroll only show personal vehicle mileage

= 1.1 =
* make shifts show on schedule even if they have no job or no employee