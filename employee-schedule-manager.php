<?php
/*
Plugin Name: Employee Schedule Manager
Plugin URI: http://wpalchemists.com/plugins
Description: Manage your employees' schedules, let employees view their schedule online, generate timesheets and payroll reports
Version: 2.5.11
Author: Morgan Kay
Author URI: http://wpalchemists.com
Text Domain: wpaesm
*/

/*  Copyright 2016 Morgan Kay (email : morgan@wpalchemists.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
register_activation_hook(__FILE__, 'wpaesm_add_defaults');
register_uninstall_hook(__FILE__, 'wpaesm_delete_plugin_options');
add_action('admin_init', 'wpaesm_options_init' );
add_action('admin_menu', 'wpaesm_add_options_page');
add_action('admin_menu', 'wpaesm_add_bulk_creator_page');

// Require options stuff
require_once( plugin_dir_path( __FILE__ ) . 'options.php' );
// Require bulk shift creator
require_once( plugin_dir_path( __FILE__ ) . 'bulk-shift-creator.php' );
// Require views
require_once( plugin_dir_path( __FILE__ ) . 'views.php' );
// Require dashboard views
require_once( plugin_dir_path( __FILE__ ) . 'dashboard-views.php' );
// Require reports
require_once( plugin_dir_path( __FILE__ ) . 'reports.php' );
// Require filter
require_once( plugin_dir_path( __FILE__ ) . 'filter.php' );
// Require instructions
require_once( plugin_dir_path( __FILE__ ) . 'instructions.php' );
// Require dashboard widgets
require_once( plugin_dir_path( __FILE__ ) . 'dashboard-widgets.php' );
// Require SCC special things
require_once( plugin_dir_path( __FILE__ ) . 'scc/scc-functions.php');
// Require pretty email
require_once( plugin_dir_path( __FILE__ ) . 'email.php' );


// Initialize language so it can be translated
function wpaesm_language_init() {
  load_plugin_textdomain( 'wpaesm', false, 'employee-schedule-manager/languages' );
}
add_action('init', 'wpaesm_language_init');




/**
 * Run text notifications
 */
require plugin_dir_path( __FILE__ ) . 'text-messages/includes/class-text-notifications.php';
function run_shiftee_text_notifications() {

	$plugin = new Shiftee_Text_Notifications();
	$plugin->run();

}
run_shiftee_text_notifications();


// ------------------------------------------------------------------------
// REGISTER CUSTOM POST TYPES AND TAXONOMIES:
// ------------------------------------------------------------------------

// Shift Type
function wpaesm_register_tax_shift_type() {

	$labels = array(
		'name'                       => _x( 'Shift Types', 'Taxonomy General Name', 'wpaesm' ),
		'singular_name'              => _x( 'Shift Type', 'Taxonomy Singular Name', 'wpaesm' ),
		'menu_name'                  => __( 'Shift Types', 'wpaesm' ),
		'all_items'                  => __( 'All Shift Types', 'wpaesm' ),
		'parent_item'                => __( 'Parent Shift Type', 'wpaesm' ),
		'parent_item_colon'          => __( 'Parent Shift Type:', 'wpaesm' ),
		'new_item_name'              => __( 'New  Shift Type', 'wpaesm' ),
		'add_new_item'               => __( 'Add New Shift Type', 'wpaesm' ),
		'edit_item'                  => __( 'Edit Shift Type', 'wpaesm' ),
		'update_item'                => __( 'Update Shift Type', 'wpaesm' ),
		'separate_items_with_commas' => __( 'Separate Shift Types with commas', 'wpaesm' ),
		'search_items'               => __( 'Search Shift Types', 'wpaesm' ),
		'add_or_remove_items'        => __( 'Add or remove Shift Types', 'wpaesm' ),
		'choose_from_most_used'      => __( 'Choose from the most used Shift Types', 'wpaesm' ),
		'not_found'                  => __( 'Not Found', 'wpaesm' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'shift_type', array( 'shift' ), $args );

	// Create default statuses
	wp_insert_term(
		'Extra', // the term 
		'shift_type', // the taxonomy
		array(
			'description'=> 'Work done outside of a scheduled shift',
			'slug' => 'extra',
		)
	);

	wp_insert_term(
		'Paid Time Off', // the term 
		'shift_type', // the taxonomy
		array(
			'description'=> 'Paid time that is not a work shift',
			'slug' => 'pto',
		)
	);

}
add_action( 'init', 'wpaesm_register_tax_shift_type', 0 );

// Shift Status
function wpaesm_register_tax_shift_status() {

	$labels = array(
		'name'                       => _x( 'Shift Statuses', 'Taxonomy General Name', 'wpaesm' ),
		'singular_name'              => _x( 'Shift Status', 'Taxonomy Singular Name', 'wpaesm' ),
		'menu_name'                  => __( 'Shift Statuses', 'wpaesm' ),
		'all_items'                  => __( 'All Shift Statuses', 'wpaesm' ),
		'parent_item'                => __( 'Parent Shift Status', 'wpaesm' ),
		'parent_item_colon'          => __( 'Parent Shift Status:', 'wpaesm' ),
		'new_item_name'              => __( 'New Shift Status', 'wpaesm' ),
		'add_new_item'               => __( 'Add New Shift Status', 'wpaesm' ),
		'edit_item'                  => __( 'Edit Shift Status', 'wpaesm' ),
		'update_item'                => __( 'Update Shift Status', 'wpaesm' ),
		'separate_items_with_commas' => __( 'Separate Shift Statuses with commas', 'wpaesm' ),
		'search_items'               => __( 'Search Shift Statuses', 'wpaesm' ),
		'add_or_remove_items'        => __( 'Add or remove Shift Statuses', 'wpaesm' ),
		'choose_from_most_used'      => __( 'Choose from the most used Shift Statuses', 'wpaesm' ),
		'not_found'                  => __( 'Not Found', 'wpaesm' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'shift_status', array( 'shift' ), $args );

	wp_insert_term(
		'Unassigned', // the term 
		'shift_status', // the taxonomy
		array(
			'description'=> 'No one has been assigned to work this shift',
			'slug' => 'unassigned',
		)
	);

	wp_insert_term(
		'Assigned', // the term 
		'shift_status', // the taxonomy
		array(
			'description'=> 'Default status for a shift, indicates that this shift has been assigned to an employee',
			'slug' => 'assigned',
		)
	);

	wp_insert_term(
		'Worked', // the term 
		'shift_status', // the taxonomy
		array(
			'description'=> 'Employee has worked this shift',
			'slug' => 'worked',
		)
	);

	// if admin must approve extra shifts, then we need a "pending approval" status and a "not approved" status
	$options = get_option( 'wpaesm_options' );
	if( '1' == $options['extra_shift_approval'] ) {
		wp_insert_term(
			'Pending Approval', // the term 
			'shift_status', // the taxonomy
			array(
				'description'=> 'Employee has worked the shift, but it is pending admin approval',
				'slug' => 'pending-approval',
			)
		);

		wp_insert_term(
			'Not Approved', // the term 
			'shift_status', // the taxonomy
			array(
				'description'=> 'Employee reported an extra shift, but admin did not approve it',
				'slug' => 'not-approved',
			)
		);
	}

}
add_action( 'init', 'wpaesm_register_tax_shift_status', 0 );

// If no other status has been selected, shift will default to 'tentative'
// http://wordpress.mfields.org/2010/set-default-terms-for-your-custom-taxonomies-in-wordpress-3-0/
function wpaesm_default_shift_status( $post_id, $post ) {
    if ( 'publish' === $post->post_status ) {
        $defaults = array(
            'shift_status' => array( 'tentative' ),
            );
        $taxonomies = get_object_taxonomies( $post->post_type );
        foreach ( (array) $taxonomies as $taxonomy ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy );
            if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
                wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
            }
        }
    }
}
add_action( 'save_post', 'wpaesm_default_shift_status', 100, 2 );

// Job Category
// SCC - this is client category, not job category
function wpaesm_register_tax_client_category() {

    $labels = array(
        'name'                       => _x( 'Client Category', 'Taxonomy General Name', 'wpaesm' ),
        'singular_name'              => _x( 'Client Category', 'Taxonomy Singular Name', 'wpaesm' ),
        'menu_name'                  => __( 'Client Categories', 'wpaesm' ),
        'all_items'                  => __( 'All Client Categories', 'wpaesm' ),
        'parent_item'                => __( 'Parent Client Category', 'wpaesm' ),
        'parent_item_colon'          => __( 'Parent Client Category:', 'wpaesm' ),
        'new_item_name'              => __( 'New Client Category', 'wpaesm' ),
        'add_new_item'               => __( 'Add New Client Category', 'wpaesm' ),
        'edit_item'                  => __( 'Edit Client Category', 'wpaesm' ),
        'update_item'                => __( 'Update Client Category', 'wpaesm' ),
        'separate_items_with_commas' => __( 'Separate Client Categories with commas', 'wpaesm' ),
        'search_items'               => __( 'Search Client Categories', 'wpaesm' ),
        'add_or_remove_items'        => __( 'Add or remove Client Categories', 'wpaesm' ),
        'choose_from_most_used'      => __( 'Choose from the most used Client Categories', 'wpaesm' ),
        'not_found'                  => __( 'Not Found', 'wpaesm' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'client_category', array( 'client' ), $args );

}
add_action( 'init', 'wpaesm_register_tax_client_category', 0 );

// Shift
function wpaesm_register_cpt_shift() {

	$labels = array(
		'name'                => _x( 'Shifts', 'Post Type General Name', 'wpaesm' ),
		'singular_name'       => _x( 'Shift', 'Post Type Singular Name', 'wpaesm' ),
		'menu_name'           => __( 'Shifts', 'wpaesm' ),
		'parent_item_colon'   => __( 'Parent Shift:', 'wpaesm' ),
		'all_items'           => __( 'All Shifts', 'wpaesm' ),
		'view_item'           => __( 'View Shift', 'wpaesm' ),
		'add_new_item'        => __( 'Add New Shift', 'wpaesm' ),
		'add_new'             => __( 'Add Single Shift', 'wpaesm' ),
		'edit_item'           => __( 'Edit Shift', 'wpaesm' ),
		'update_item'         => __( 'Update Shift', 'wpaesm' ),
		'search_items'        => __( 'Search Shifts', 'wpaesm' ),
		'not_found'           => __( 'Not found', 'wpaesm' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'wpaesm' ),
	);
	$args = array(
		'label'               => __( 'shift', 'wpaesm' ),
		'description'         => __( 'Shifts you can assign to employees', 'wpaesm' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'shift_type' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 70,
		'menu_icon'           => 'dashicons-calendar',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'shift', $args );

}
add_action( 'init', 'wpaesm_register_cpt_shift', 0 );

// Jobs
// SCC - this is client, not job
function wpaesm_register_cpt_client() {

    $labels = array(
        'name'                => _x( 'Clients', 'Post Type General Name', 'wpaesm' ),
        'singular_name'       => _x( 'Client', 'Post Type Singular Name', 'wpaesm' ),
        'menu_name'           => __( 'Clients', 'wpaesm' ),
        'parent_item_colon'   => __( 'Parent Client:', 'wpaesm' ),
        'all_items'           => __( 'All Clients', 'wpaesm' ),
        'view_item'           => __( 'View Client', 'wpaesm' ),
        'add_new_item'        => __( 'Add New Client', 'wpaesm' ),
        'add_new'             => __( 'Add New', 'wpaesm' ),
        'edit_item'           => __( 'Edit Client', 'wpaesm' ),
        'update_item'         => __( 'Update Client', 'wpaesm' ),
        'search_items'        => __( 'Search Clients', 'wpaesm' ),
        'not_found'           => __( 'Not found', 'wpaesm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'wpaesm' ),
    );
    $args = array(
        'label'               => __( 'client', 'wpaesm' ),
        'description'         => __( 'clients', 'wpaesm' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
        'taxonomies'          => array( 'client_category', 'customer' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => false,
        'menu_position'       => 70,
        'menu_icon'           => 'dashicons-universal-access',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite' => array(
	        'with_front' => false,
	        'slug' => '/client'
        )
    );
    register_post_type( 'client', $args );

}
add_action( 'init', 'wpaesm_register_cpt_client', 0 );

// Register Expense Category
function wpaesm_register_tax_expense_category() {

	$labels = array(
		'name'                       => _x( 'Expense Categories', 'Taxonomy General Name', 'wpaesm' ),
		'singular_name'              => _x( 'Expense Category', 'Taxonomy Singular Name', 'wpaesm' ),
		'menu_name'                  => __( 'Expense Categories', 'wpaesm' ),
		'all_items'                  => __( 'All Expense Categories', 'wpaesm' ),
		'parent_item'                => __( 'Parent Expense Category', 'wpaesm' ),
		'parent_item_colon'          => __( 'Parent Expense Category:', 'wpaesm' ),
		'new_item_name'              => __( 'New Expense Category', 'wpaesm' ),
		'add_new_item'               => __( 'Add Expense Category', 'wpaesm' ),
		'edit_item'                  => __( 'Edit Expense Category', 'wpaesm' ),
		'update_item'                => __( 'Update Expense Category', 'wpaesm' ),
		'separate_items_with_commas' => __( 'Separate Expense Categories with commas', 'wpaesm' ),
		'search_items'               => __( 'Search Expense Categories', 'wpaesm' ),
		'add_or_remove_items'        => __( 'Add or remove Expense Categories', 'wpaesm' ),
		'choose_from_most_used'      => __( 'Choose from the most used Expense Categories', 'wpaesm' ),
		'not_found'                  => __( 'Not Found', 'wpaesm' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'expense_category', array( 'expense' ), $args );

	wp_insert_term(
		'Mileage', // the term 
		'expense_category', // the taxonomy
		array(
			'description'=> 'Mileage to be reimbursed',
			'slug' => 'mileage',
		)
	);

	wp_insert_term(
		'Receipt', // the term 
		'expense_category', // the taxonomy
		array(
			'description'=> 'Receipts to be reimbursed',
			'slug' => 'receipt',
		)
	);

}
add_action( 'init', 'wpaesm_register_tax_expense_category', 0 );

// Register Expense Status
function wpaesm_register_tax_expense_status() {

	$labels = array(
		'name'                       => _x( 'Expense Statuses', 'Taxonomy General Name', 'wpaesm' ),
		'singular_name'              => _x( 'Expense Status', 'Taxonomy Singular Name', 'wpaesm' ),
		'menu_name'                  => __( 'Expense Statuses', 'wpaesm' ),
		'all_items'                  => __( 'All Expense Statuses', 'wpaesm' ),
		'parent_item'                => __( 'Parent Expense Status', 'wpaesm' ),
		'parent_item_colon'          => __( 'Parent Expense Status:', 'wpaesm' ),
		'new_item_name'              => __( 'New Expense Status', 'wpaesm' ),
		'add_new_item'               => __( 'Add Expense Status', 'wpaesm' ),
		'edit_item'                  => __( 'Edit Expense Status', 'wpaesm' ),
		'update_item'                => __( 'Update Expense Status', 'wpaesm' ),
		'separate_items_with_commas' => __( 'Separate Expense Statuses with commas', 'wpaesm' ),
		'search_items'               => __( 'Search Expense Statuses', 'wpaesm' ),
		'add_or_remove_items'        => __( 'Add or remove Expense Statuses', 'wpaesm' ),
		'choose_from_most_used'      => __( 'Choose from the most used Expense Statuses', 'wpaesm' ),
		'not_found'                  => __( 'Not Found', 'wpaesm' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'expense_status', array( 'expense' ), $args );

	wp_insert_term(
		'Reimbursed', // the term 
		'expense_status', // the taxonomy
		array(
			'description'=> 'Expenses for which employee has been reimbursed',
			'slug' => 'reimbursed',
		)
	);

}
add_action( 'init', 'wpaesm_register_tax_expense_status', 0 );


// Expenses
function wpaesm_register_cpt_expense() {

	$labels = array(
		'name'                => _x( 'Expenses', 'Post Type General Name', 'wpaesm' ),
		'singular_name'       => _x( 'Expense', 'Post Type Singular Name', 'wpaesm' ),
		'menu_name'           => __( 'Expenses', 'wpaesm' ),
		'parent_item_colon'   => __( 'Parent Expense:', 'wpaesm' ),
		'all_items'           => __( 'All Expenses', 'wpaesm' ),
		'view_item'           => __( 'View Expense', 'wpaesm' ),
		'add_new_item'        => __( 'Add New Expense', 'wpaesm' ),
		'add_new'             => __( 'Add New', 'wpaesm' ),
		'edit_item'           => __( 'Edit Expense', 'wpaesm' ),
		'update_item'         => __( 'Update Expense', 'wpaesm' ),
		'search_items'        => __( 'Search Expenses', 'wpaesm' ),
		'not_found'           => __( 'Not found', 'wpaesm' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'wpaesm' ),
	);
	$args = array(
		'label'               => __( 'expense', 'wpaesm' ),
		'description'         => __( 'Expenses submitted by employees', 'wpaesm' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'expense_category' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 70,
		'menu_icon'           => 'dashicons-chart-area',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'expense', $args );

}
add_action( 'init', 'wpaesm_register_cpt_expense', 0 );


add_filter('post_type_link', 'wpaesm_filter_client_link', 1, 3);
function wpaesm_filter_client_link($post_link, $post = 0) {

	if($post->post_type === 'client') {
		return home_url('client/' . $post->ID . '/');
	}
	else{
		return $post_link;
	}

//
//		if ( strpos('%client_id%', $post_link) === 'FALSE' ) {
//		return $post_link;
//	}
//	$post = get_post($id);
//	if ( is_wp_error($post) || $post->post_type !== 'client' ) {
//		return $post_link;
//	}
//	return str_replace('%client_id%', $post->ID, $post_link);
}

function wpaesm_rewrites_init(){
	add_rewrite_rule('client/([0-9]+)?$', 'index.php?post_type=client&p=$matches[1]', 'top');
}
add_action('init', 'wpaesm_rewrites_init');

// ------------------------------------------------------------------------
// CREATE EMPLOYEE USER ROLE
// ------------------------------------------------------------------------

function wpaesm_create_employee_user_role() {
    add_role( 'employee', 'Employee', array( 'read' => true, 'edit_posts' => false, 'publish_posts' => false ) );
	add_role( 'archived', 'Former Employee', array( 'read' => false, 'edit_posts' => false, 'publish_posts' => false ) );
}
register_activation_hook( __FILE__, 'wpaesm_create_employee_user_role' );

// create a function to check for the role of the current user
// thanks to http://docs.appthemes.com/tutorials/wordpress-check-user-role-function/
function wpaesm_check_user_role( $role, $user_id = null ) {
 
    if ( is_numeric( $user_id ) )
	$user = get_userdata( $user_id );
    else
        $user = wp_get_current_user();
 
    if ( empty( $user ) )
	return false;
 
    return in_array( $role, (array) $user->roles );
}

// ------------------------------------------------------------------------
// CREATE EMPLOYEE USER FIELDS
// ------------------------------------------------------------------------

add_action( 'show_user_profile', 'wpaesm_employee_profile_fields' );
add_action( 'edit_user_profile', 'wpaesm_employee_profile_fields' );

function wpaesm_employee_profile_fields( $user ) { ?>
	<h3><?php _e("Employee information", "blank"); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="address"><?php _e("Street Address"); ?></label></th>
			<td>
				<input type="text" name="address" id="address" value="<?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="city"><?php _e("City"); ?></label></th>
			<td>
				<input type="text" name="city" id="city" value="<?php echo esc_attr( get_the_author_meta( 'city', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="state"><?php _e("State"); ?></label></th>
			<td>
				<input type="text" name="state" id="state" value="<?php echo esc_attr( get_the_author_meta( 'state', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="zip"><?php _e("Zip"); ?></label></th>
			<td>
				<input type="text" name="zip" id="zip" value="<?php echo esc_attr( get_the_author_meta( 'zip', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="phone"><?php _e("Phone Number"); ?></label></th>
			<td>
				<input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<?php if( is_admin() ) { ?>
			<tr>
				<th><label for="wage"><?php _e("Pay Rate"); ?></label></th>
				<td>
					<input type="text" name="wage" id="wage" value="<?php echo esc_attr( get_the_author_meta( 'wage', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"><?php _e("Enter the hourly pay rate, with no currency sign.  Example: 15"); ?></span>
				</td>
			</tr>
			<tr>
				<th><label for="deduction"><?php _e("Deductions"); ?></label></th>
				<td>
					<span class="description"><?php _e("For each deduction, enter the amount to be deducted from each paycheck, with no currency sign.  Example: 15"); ?></span><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Health Insurance for Employee: ', 'wpaesm' ); ?></label><input type="text" name="deductions_health_self" id="deductions_health_self" value="<?php echo esc_attr( get_the_author_meta( 'deductions_health_self', $user->ID ) ); ?>" class="small-text" /><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Health Insurance for Family: ', 'wpaesm' ); ?></label><input type="text" name="deductions_health_family" id="deductions_health_family" value="<?php echo esc_attr( get_the_author_meta( 'deductions_health_family', $user->ID ) ); ?>" class="small-text" /><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Dental Insurance for Employee: ', 'wpaesm' ); ?></label><input type="text" name="deductions_dental_self" id="deductions_dental_self" value="<?php echo esc_attr( get_the_author_meta( 'deductions_dental_self', $user->ID ) ); ?>" class="small-text" /><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Dental Insurance for Family: ', 'wpaesm' ); ?></label><input type="text" name="deductions_dental_family" id="deductions_dental_family" value="<?php echo esc_attr( get_the_author_meta( 'deductions_dental_family', $user->ID ) ); ?>" class="small-text" /><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Wage Garnishment: ', 'wpaesm' ); ?></label><input type="text" name="deductions_garnish" id="deductions_garnish" value="<?php echo esc_attr( get_the_author_meta( 'deductions_garnish', $user->ID ) ); ?>" class="small-text" /><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Extra Withholding: ', 'wpaesm' ); ?></label><input type="text" name="deductions_withhold" id="deductions_withhold" value="<?php echo esc_attr( get_the_author_meta( 'deductions_withhold', $user->ID ) ); ?>" class="small-text" /><br />
					<label style="display:inline-block;width:16em"><?php _e( 'Other: ', 'wpaesm' ); ?></label><input type="text" name="deductions_other" id="deductions_other" value="<?php echo esc_attr( get_the_author_meta( 'deductions_other', $user->ID ) ); ?>" class="small-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="hours"><?php _e("Hours Per Week", "wpaesm"); ?></label></th>
				<td>
					<input type="text" name="hours" id="hours" value="<?php echo esc_attr( get_the_author_meta( 'hours', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"><?php _e("Enter the number of regular hours the employee works per week.  If the employee works more hours, it will be considered overtime.  You can enter default regular hours on the Employee Schedule Manager settings page.  Example: 40"); ?></span>
				</td>
			</tr>
			<tr>
				<th><label for="unavailable"><?php _e("Unavailable", "wpaesm"); ?></label></th>
				<script type="text/template" id="date-time-template">
					<div class="repeating">
						<p class="third" style="display: inline-block;">
							<label><?php _e("Day", "wpaesm"); ?></label><br />
							<select name="unavailable[0][day]" id="unavailable[0][day]">
								<option value=""></option>
								<option value="sunday"><?php _e( 'Sunday', 'wpaesm' ); ?></option>
								<option value="monday"><?php _e( 'Monday', 'wpaesm' ); ?></option>
								<option value="tuesday"><?php _e( 'Tuesday', 'wpaesm' ); ?></option>
								<option value="wednesday"><?php _e( 'Wednesday', 'wpaesm' ); ?></option>
								<option value="thursday"><?php _e( 'Thursday', 'wpaesm' ); ?></option>
								<option value="friday"><?php _e( 'Friday', 'wpaesm' ); ?></option>
								<option value="saturday"><?php _e( 'Saturday', 'wpaesm' ); ?></option>
							</select>
						</p>
						<p class="third" style="display: inline-block;">
							<label><?php _e( "From", "wpaesm"); ?></label><br />
							<input type="text" name="unavailable[0][start]" id="unavailable[0][start]" class="starttime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable_start[0][start]', $user->ID ) ); ?>" class="short-text" />
						</p>
						<p class="third" style="display: inline-block;">
							<label><?php _e( "To", "wpaesm" ); ?></label><br />
							<input type="text" name="unavailable[0][end]" id="unavailable[0][end]" class="endtime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable[0][end]', $user->ID ) ); ?>" class="short-text" />
						</p>
						<a href="#" class="remove"><?php _e( 'Remove', 'wpaesm'); ?></a>
					</div>
				</script>
				<td>
					<?php $unavailable = get_the_author_meta( 'unavailable', $user->ID );
					if( is_array( $unavailable ) ) {
						$i = 0;
						foreach( $unavailable as $unavailability ) { ?>
							<div class="repeating">
								<p class="third" style="display: inline-block;">
									<label><?php _e("Day", "wpaesm"); ?></label><br />
									<select name="unavailable[<?php echo $i; ?>][day]" id="unavailable[<?php echo $i; ?>][day]">
										<option value=""></option>
										<option value="sunday" <?php selected( $unavailability['day'], 'sunday' ); ?>><?php _e( 'Sunday', 'wpaesm' ); ?></option>
										<option value="monday" <?php selected( $unavailability['day'], 'monday' ); ?>><?php _e( 'Monday', 'wpaesm' ); ?></option>
										<option value="tuesday" <?php selected( $unavailability['day'], 'tuesday' ); ?>><?php _e( 'Tuesday', 'wpaesm' ); ?></option>
										<option value="wednesday" <?php selected( $unavailability['day'], 'wednesday' ); ?>><?php _e( 'Wednesday', 'wpaesm' ); ?></option>
										<option value="thursday" <?php selected( $unavailability['day'], 'thursday' ); ?>><?php _e( 'Thursday', 'wpaesm' ); ?></option>
										<option value="friday" <?php selected( $unavailability['day'], 'friday' ); ?>><?php _e( 'Friday', 'wpaesm' ); ?></option>
										<option value="saturday" <?php selected( $unavailability['day'], 'saturday' ); ?>><?php _e( 'Saturday', 'wpaesm' ); ?></option>
									</select>
								</p>
								<p class="third" style="display: inline-block;">
									<label><?php _e( "From", "wpaesm"); ?></label><br />
									<input type="text" name="unavailable[<?php echo $i; ?>][start]" id="unavailable[<?php echo $i; ?>][start]" class="starttime" value="<?php echo $unavailability['start']; ?>" class="short-text" />
								</p>
								<p class="third" style="display: inline-block;">
									<label><?php _e( "To", "wpaesm" ); ?></label><br />
									<input type="text" name="unavailable[<?php echo $i; ?>][end]" id="unavailable[<?php echo $i; ?>][end]" class="endtime" value="<?php echo $unavailability['end']; ?>" class="short-text" />
								</p>
								<a href="#" class="remove"><?php _e( 'Remove', 'wpaesm'); ?></a>
							</div>
						<?php $i++;
						}
					}  else { ?>
						<div class="repeating">
							<p class="third" style="display: inline-block;">
								<label><?php _e("Day", "wpaesm"); ?></label><br />
								<select name="unavailable[0][day]" id="unavailable[0][day]">
									<option value=""></option>
									<option value="sunday"><?php _e( 'Sunday', 'wpaesm' ); ?></option>
									<option value="monday"><?php _e( 'Monday', 'wpaesm' ); ?></option>
									<option value="tuesday"><?php _e( 'Tuesday', 'wpaesm' ); ?></option>
									<option value="wednesday"><?php _e( 'Wednesday', 'wpaesm' ); ?></option>
									<option value="thursday"><?php _e( 'Thursday', 'wpaesm' ); ?></option>
									<option value="friday"><?php _e( 'Friday', 'wpaesm' ); ?></option>
									<option value="saturday"><?php _e( 'Saturday', 'wpaesm' ); ?></option>
								</select>
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "From", "wpaesm"); ?></label><br />
								<input type="text" name="unavailable[0][start]" id="unavailable[0][start]" class="starttime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable_start[0][start]', $user->ID ) ); ?>" class="short-text" />
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "To", "wpaesm" ); ?></label><br />
								<input type="text" name="unavailable[0][end]" id="unavailable[0][end]" class="endtime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable[0][end]', $user->ID ) ); ?>" class="short-text" />
							</p>
							<a href="#" class="remove"><?php _e( 'Remove', 'wpaesm'); ?></a>
						</div>
					<?php } ?>
					<p><a href="#" class="repeat"><?php _e('Add Another', 'wpaesm'); ?></a></p>
					<span class="description" style="display:block;clear:both"><?php _e("Enter the days and times when employee is regularly unavailable."); ?></span>
				</td>
			</tr>
		<?php } ?>
	</table>
	<script type="text/javascript">
        var repeatingTemplate = jQuery('#date-time-template').html();
         
		// Add a new repeating section         
		jQuery('.repeat').click(function(e){
	        e.preventDefault();
            var repeating = jQuery(repeatingTemplate);
	        var lastRepeatingGroup = jQuery('.repeating').last();
		    var idx = lastRepeatingGroup.index();
		    var attrs = ['for', 'id', 'name'];
		    var tags = repeating.find('input, label, select');
		    tags.each(function() {
		      var section = jQuery(this);
		      jQuery.each(attrs, function(i, attr) {
		        var attr_val = section.attr(attr);
		        if (attr_val) {
		            section.attr(attr, attr_val.replace(/unavailable\[\d+\]\[/, 'unavailable\['+(idx + 1)+'\]\['))
		        }
		      })
		    })


	        lastRepeatingGroup.after(repeating);
            repeating.find('.starttime').datetimepicker({
              datepicker:false,
              format:'H:i',
              step: 15,
            });
            repeating.find('.endtime').datetimepicker({
              datepicker:false,
              format:'H:i',
              step: 15,
            });
	    });

	</script>
<?php }

add_action( 'personal_options_update', 'wpaesm_save_employee_profile_fields' );
add_action( 'edit_user_profile_update', 'wpaesm_save_employee_profile_fields' );

function wpaesm_save_employee_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

	update_user_meta( $user_id, 'address', $_POST['address'] );
	update_user_meta( $user_id, 'city', $_POST['city'] );
	update_user_meta( $user_id, 'state', $_POST['state'] );
	update_user_meta( $user_id, 'zip', $_POST['zip'] );
	update_user_meta( $user_id, 'phone', $_POST['phone'] );
	update_user_meta( $user_id, 'wage', $_POST['wage'] );
	update_user_meta( $user_id, 'deductions_health_self', $_POST['deductions_health_self'] );
	update_user_meta( $user_id, 'deductions_health_family', $_POST['deductions_health_family'] );
	update_user_meta( $user_id, 'deductions_dental_self', $_POST['deductions_dental_self'] );
	update_user_meta( $user_id, 'deductions_dental_family', $_POST['deductions_dental_family'] );
	update_user_meta( $user_id, 'deductions_garnish', $_POST['deductions_garnish'] );
	update_user_meta( $user_id, 'deductions_withhold', $_POST['deductions_withhold'] );
	update_user_meta( $user_id, 'deductions_other', $_POST['deductions_other'] );
	update_user_meta( $user_id, 'hours', $_POST['hours'] );
	// delete unavailability details before we save them so that removed values get removed
	delete_user_meta( $user_id, 'unavailable' );
	update_user_meta( $user_id, 'unavailable', $_POST['unavailable'] );
}


// ------------------------------------------------------------------------
// CREATE CUSTOM METABOXES
// ------------------------------------------------------------------------

include_once 'wpalchemy/MetaBox.php';
include_once 'wpalchemy/MediaAccess.php';
define( 'MYPLUGINNAME_PATH', plugin_dir_path(__FILE__) );

$wpalchemy_media_access = new WPAlchemy_MediaAccess();

// Add stylesheets and scripts
function wpaesm_add_admin_styles_and_scripts($hook) {
	wp_enqueue_style( 'wpalchemy-metabox', plugins_url() . '/employee-schedule-manager/css/meta.css' );
	global $post;  

	if( $hook == 'post-new.php' || $hook == 'post.php' || $hook == 'edit.php' ) {
		if( is_object( $post ) ) {
			if ( 'shift' === $post->post_type ) { 
				$options = get_option('wpaesm_options');
			    wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
			    wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
			    wp_enqueue_script( 'shift-actions', plugin_dir_url(__FILE__) . 'js/shift.js', array( 'jquery' ) );
            	wp_localize_script( 'shift-actions', 'shiftajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'avoid_conflicts' => $options['avoid_conflicts'] ) ); 
			}
			if ( 'expense' === $post->post_type ) { 
			    wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
			    wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
			}
		}
	}
	if ( 'employee-schedule-manager_page_payroll-report' == $hook ||
	     'shift_page_add-repeating-shifts' == $hook ||
	     'shift_page_filter-shifts' == $hook ||
	     'expense_page_filter-expenses' == $hook ||
	     'employee-schedule-manager_page_scheduled-worked' == $hook ||
	     'user-edit.php' == $hook ||
	     'shift_page_view-schedules' == $hook ||
	     'document_page_export-documents' == $hook
	) {
		wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
		wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
	}
	if( 'shift_page_filter-shifts' == $hook || 'employee-schedule-manager_page_scheduled-worked' == $hook || 'document_page_unapproved-documents' == $hook || 'document_page_undocumented-shifts' == $hook ) {
		wp_enqueue_script( 'stupid-table', plugins_url() . '/employee-schedule-manager/js/stupidtable.min.js', array( 'jquery' ) );
	}
	if( 'document_page_unapproved-documents' == $hook || 'document_page_undocumented-shifts' == $hook ) {
		wp_enqueue_script( 'wpaesm_docajax', plugins_url() . '/employee-schedule-manager/js/docajax.js', 'jQuery' );
		wp_localize_script( 'wpaesm_docajax', 'wpaesmajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) ); 
	}
}
add_action( 'admin_enqueue_scripts', 'wpaesm_add_admin_styles_and_scripts' );

// Create metabox for shifts
$shift_metabox = new WPAlchemy_MetaBox(array
(
    'id' => 'shift_meta',
    'title' => 'Shift Details',
    'types' => array('shift'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/shiftinfo.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_'
));

// Create metabox for publishing shifts
$shift_publish_metabox = new WPAlchemy_MetaBox(array
(
    'id' => 'shift_publish_meta',
    'title' => 'Related Shifts',
    'types' => array('shift'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/shiftpublish.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_',
    'context' => 'side',
    'priority' => 'high'
));

// Create metabox for expenses
$expense_metabox = new WPAlchemy_MetaBox(array
(
    'id' => 'expense_meta',
    'title' => 'Expense Details',
    'types' => array('expense'),
    'template' => MYPLUGINNAME_PATH . '/wpalchemy/expenseinfo.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_wpaesm_'
));


// ------------------------------------------------------------------------
// ADD FIELDS TO TAXONOMIES
// http://en.bainternet.info/wordpress-taxonomies-extra-fields-the-easy-way/
// ------------------------------------------------------------------------

//include the main class file
require_once("Tax-meta-class/Tax-meta-class.php");

/*
* configure taxonomy custom fields
*/
$config = array(
   'id' => 'status_meta_box',
   'title' => 'Shift Status Details',
   'pages' => array('shift_status'),
   'context' => 'normal',
   'fields' => array(),
   'local_images' => false,
   'use_with_theme' => false
);

$status_meta = new Tax_Meta_Class($config);

$status_meta->addColor('status_color',array('name'=> 'Shift Status Color '));

$status_meta->Finish();


// ------------------------------------------------------------------------
// ADD COLUMNS TO SHIFTS OVERVIEW PAGE
// ------------------------------------------------------------------------

function wpaesm_shift_overview_columns_headers($defaults) {
    $defaults['shiftdate'] = 'Shift Date';
    $defaults['shifttime'] = 'Scheduled Time';
    return $defaults;
}
function wpaesm_shift_overview_columns($column_name, $post_ID) {
	global $shift_metabox;
	$meta = $shift_metabox->the_meta();
    if ($column_name == 'shiftdate' && isset($meta['date'])) {
        echo $meta['date'];
    }    
    if ($column_name == 'shifttime' && isset($meta['starttime']) && isset($meta['endtime'])) {
        echo $meta['starttime'] . "-" . $meta['endtime'];
    }    
}

add_filter('manage_shift_posts_columns', 'wpaesm_shift_overview_columns_headers', 10);
add_action('manage_shift_posts_custom_column', 'wpaesm_shift_overview_columns', 10, 2);


// ------------------------------------------------------------------------
// ADD COLUMNS TO USERS OVERVIEW PAGE
// ------------------------------------------------------------------------

add_filter( 'manage_users_columns', 'wpaesm_add_timesheet_column_to_users', 10, 2 );
function wpaesm_add_timesheet_column_to_users( $column ) {
	
	$column['timesheet'] = 'Last Timesheet Submitted';
    return $column;
	
}

add_filter( 'manage_users_sortable_columns', 'wpaesm_make_timesheet_column_sortable' );
function wpaesm_make_timesheet_column_sortable( $sortable_columns ) {

	$sortable_columns[ 'timesheet' ] = 'timesheet';
	return $sortable_columns;
	
}

add_action( 'manage_users_custom_column', 'wpaesm_populate_timesheet_column', 10, 3 );
function wpaesm_populate_timesheet_column( $value, $column_name, $user_id ) {

	switch( $column_name ) {
	
		case 'timesheet':
			$timestamp = get_user_meta( $user_id, 'last_timesheet', true );
			if( isset( $timestamp ) && '' !== $timestamp ) {
				$date = date( 'Y-m-d', $timestamp );
			} else {
				$date = '';
			}
			return '<div id="timesheet_history-' . $user_id . '">' . $date . '</div>';
			break;
			
	}
	
}

add_action( 'pre_user_query','wpaesm_sort_timesheet_column' );
function wpaesm_sort_timesheet_column( $user_search ) {
    global $wpdb,$current_screen;

    if( !is_admin() ) {
    	return;
    }

	if( !is_object( $current_screen) )
		return;

    if ( 'users' != $current_screen->id ) 
        return;

    $vars = $user_search->query_vars;

    if( 'timesheet' == $vars['orderby'] ) {
        $user_search->query_from .= " INNER JOIN {$wpdb->usermeta} m1 ON {$wpdb->users}.ID=m1.user_id AND (m1.meta_key='last_timesheet')"; 
        $user_search->query_orderby = ' ORDER BY UPPER(m1.meta_value) '. $vars['order'];
    } 

}

// ------------------------------------------------------------------------
// CONNECT SHIFTS TO CLIENTS AND EMPLOYEES
// https://github.com/scribu/wp-posts-to-posts/blob/master/posts-to-posts.php
// ------------------------------------------------------------------------

function wpaesm_p2p_check() {
	if ( !is_plugin_active( 'posts-to-posts/posts-to-posts.php' ) ) {
		require_once dirname( __FILE__ ) . '/wpp2p/autoload.php';
		define( 'P2P_PLUGIN_VERSION', '1.6.3' );
		define( 'P2P_TEXTDOMAIN', 'cdcrm' );
	}
}
add_action( 'admin_init', 'wpaesm_p2p_check' );

function wpaesm_p2p_load() {
	//load_plugin_textdomain( P2P_TEXTDOMAIN, '', basename( dirname( __FILE__ ) ) . '/languages' );
	if ( !function_exists( 'p2p_register_connection_type' ) ) {
		require_once dirname( __FILE__ ) . '/wpp2p/autoload.php';
	}
	P2P_Storage::init();
	P2P_Query_Post::init();
	P2P_Query_User::init();
	P2P_URL_Query::init();
	P2P_Widget::init();
	P2P_Shortcodes::init();
	register_uninstall_hook( __FILE__, array( 'P2P_Storage', 'uninstall' ) );
	if ( is_admin() )
		wpaesm_load_admin();
}

function wpaesm_load_admin() {
	P2P_Autoload::register( 'P2P_', dirname( __FILE__ ) . '/wpp2p/admin' );

	new P2P_Box_Factory;
	new P2P_Column_Factory;
	new P2P_Dropdown_Factory;

	new P2P_Tools_Page;
}

function wpaesm_p2p_init() {
	// Safe hook for calling p2p_register_connection_type()
	do_action( 'p2p_init' );
}

require dirname( __FILE__ ) . '/wpp2p/scb/load.php';
scb_init( 'wpaesm_p2p_load' );
add_action( 'wp_loaded', 'wpaesm_p2p_init' );

function wpaesm_create_connections() {
    // create the connection between shifts and employees (users)
    p2p_register_connection_type( array(
        'name' => 'shifts_to_employees',
        'from' => 'shift',
        'to' => 'user',
        'cardinality' => 'many-to-one',
//        'admin_column' => 'from',
//        'to_query_vars' => array( 'role' => 'employee' ),   nice idea, but breaks timesheets and other queries
        'to_labels' => array(
			'singular_name' => __( 'Employee', 'wpaesm' ),
			'search_items' => __( 'Search employees', 'wpaesm' ),
			'not_found' => __( 'No employees found.', 'wpaesm' ),
			'create' => __( 'Add Employee', 'wpaesm' ),
		),
    ) );
    // create the connection between expenses and employees (users)
    p2p_register_connection_type( array(
        'name' => 'expenses_to_employees',
        'from' => 'expense',
        'to' => 'user',
        'cardinality' => 'many-to-one',
        'admin_column' => 'from',
        'to_labels' => array(
			'singular_name' => __( 'Employee', 'wpaesm' ),
			'search_items' => __( 'Search employees', 'wpaesm' ),
			'not_found' => __( 'No employees found.', 'wpaesm' ),
			'create' => __( 'Add Employee', 'wpaesm' ),
		),
    ) );
    // create the connection between shifts and clients
    p2p_register_connection_type( array(
        'name' => 'shifts_to_clients',
        'from' => 'shift',
        'to' => 'client',
        'cardinality' => 'many-to-one',
//        'admin_column' => 'from',
        'to_labels' => array(
            'singular_name' => __( 'Client', 'wpaesm' ),
            'search_items' => __( 'Search clients', 'wpaesm' ),
            'not_found' => __( 'No clients found.', 'wpaesm' ),
            'create' => __( 'Add Client', 'wpaesm' ),
        ),
    ) );

    // create the connection between expenses and clients
    p2p_register_connection_type( array(
        'name' => 'expenses_to_clients',
        'from' => 'expense',
        'to' => 'client',
        'cardinality' => 'many-to-one',
        'admin_column' => 'from',
        'to_labels' => array(
            'singular_name' => __( 'Client', 'wpaesm' ),
            'search_items' => __( 'Search clients', 'wpaesm' ),
            'not_found' => __( 'No clients found.', 'wpaesm' ),
            'create' => __( 'Add Client', 'wpaesm' ),
        ),
    ) );
}
add_action( 'p2p_init', 'wpaesm_create_connections' );


// ------------------------------------------------------------------------
// SEND NOTIFICATION WHEN SHIFT IS CREATED - http://codex.wordpress.org/Plugin_API/Action_Reference/save_post
// ------------------------------------------------------------------------

function wpaesm_get_latest_priority( $filter ) // figure out what priority the notify employee function needs, thanks to http://wordpress.stackexchange.com/questions/116221/how-to-force-function-to-run-as-the-last-one-when-saving-the-post
{
    if ( empty ( $GLOBALS['wp_filter'][ $filter ] ) )
        return PHP_INT_MAX;

	if( is_object( $GLOBALS['wp_filter'][ $filter ] ) ) {
		$last = key( array_slice( $GLOBALS['wp_filter'][ $filter ]->callbacks, -1, 1, TRUE ) );
	} else {
		$priorities = array_keys( $GLOBALS['wp_filter'][ $filter ] );
		$last       = end( $priorities );
	}

    if ( is_numeric( $last ) )
        return PHP_INT_MAX;

    return "$last-z";
}
add_action( 'save_post', 'wpaesm_run_that_action_last', 0 ); 

function wpaesm_notify_employee( $post_id ) {
	if( is_admin() && 'trash' !== get_post_status( $post_id ) ) { // we only need to run this function if we're in the dashboard
		$options = get_option('wpaesm_options'); // get options
		global $shift_metabox; // get metabox data
		$meta = $shift_metabox->the_meta( $post_id ); 
		if( isset( $meta['notify'] ) && "1" == $meta['notify'] ) {  // only send the email if the "notify employee" option is checked 
			// get the employee id
			$users = get_users( array(
				'connected_type' => 'shifts_to_employees',
				'connected_items' => $post_id,
			) );
			foreach($users as $user) {
				$employeeid = $user->ID;
			}

			// send the email
			if( !isset( $employeeid ) ) {
				$error = __( 'We could not send a notification, because you did not select an employee.  Click your back button and select an employee for this shift, or uncheck the employee notification option.', 'wpaesm' );
				wp_die( $error );
			}

            wpaesm_send_notification_email( $employeeid );
		}
	}
}

function wpaesm_run_that_action_last() {  // add the notification action now, with lowest priority so it runs after meta data has been saved
    add_action( 
        'save_post', 
        'wpaesm_notify_employee',
        wpaesm_get_latest_priority( current_filter() ),
        2 
    ); 

}

function wpaesm_send_notification_email( $employeeid ) {
	$employeeinfo = get_user_by( 'id', $employeeid );
	$employeeemail = $employeeinfo->user_email;	
	$options = get_option('wpaesm_options');

	$to = $employeeemail;

	$subject = $options['notification_subject'];

	$message = '<p>' . __( 'Your shift has changed.  Please check your schedule for the latest updates: ', 'wpaesm' ) . '<a href="https://seattlecommunitycare.com/your-profile/">https://seattlecommunitycare.com/your-profile/</a></p>';

	$from = $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";

	wpaesm_send_email( $from, $to, '', $subject, $message, '' );
}

// ------------------------------------------------------------------------
// DELETE ALL SHIFTS IN A SERIES 
// largely borrowed from https://wordpress.org/plugins/completely-delete/
// ------------------------------------------------------------------------

add_action( 'plugins_loaded', 'wpaesm_completely_delete' );
add_filter( 'page_row_actions', 'wpaesm_row_actions', 10, 2 );


function wpaesm_get_action_url( $post_id ) {
	return wp_nonce_url( add_query_arg( array( 'action' => 'wpaesm_completely_delete', 'post' => $post_id ), admin_url( 'admin.php' ) ), 'wpaesm_delete_series', 'wpaesm_delete_nonce' );
}

function wpaesm_row_actions( $actions, $post ) {
	if ( 'trash' != $post->post_status && $post->post_type == 'shift' ) {
		$message = __( 'Are you sure you want to delete all of these shifts?', 'wpaesm' );
		$url = wpaesm_get_action_url( $post->ID );
		$actions['wpaesm'] = '<a class="submitcd delete" href="#" onclick="if(confirm(\'' . $message . '\')) { window.location=\'' . $url . '\'; }">' . __( 'Delete Subsequent Shifts In Series', 'wpaesm' ) . '</a>';
	}

	return $actions;
}

function wpaesm_completely_delete() {
	if (!isset($_GET['wpaesm_delete_nonce']) || !wp_verify_nonce($_GET['wpaesm_delete_nonce'], 'wpaesm_delete_series')) {
		// do nothing
	} else {
		if(isset($_REQUEST['post'])) {
			$post_id = $_REQUEST['post'];
			$poststodelete = array();
			$date_after = get_post_meta( $post_id, '_wpaesm_date', true );

			if( '' == $date_after ) {
				$date_after = '2013-01-01'; // set to an early date so that it will get all shifts
			}
			$i= 0;
			// get the children of this post, if there are any
			$args = array(
					'post_type' => 'shift',
					'posts_per_page' => -1,
					'post_parent' => $post_id,
					'meta_key' => '_wpaesm_date',
					'meta_value' => $date_after,
					'meta_compare' => '>',
			);
			$children = get_posts( $args );
			foreach($children as $child) {
				$poststodelete[] = $child->ID;
				$i++;
			}

			// get the parent of this post, if there is one
			$parent = wp_get_post_parent_id( $post_id );
			if(isset($parent) && $parent != '') {
				$poststodelete[] = $parent;
				$i++;
				// get the other children of this post's parent
				$args = array(
						'post_type' => 'shift',
						'posts_per_page' => -1,
						'post_parent' => $parent,
						'meta_key' => '_wpaesm_date',
						'meta_value' => $date_after,
						'meta_compare' => '>',
				);
				$siblings = get_posts( $args );
				foreach($siblings as $sibling) {
					$poststodelete[] = $sibling->ID;
					$i++;
				}
			}

			// delete all of the children and parents
			foreach($poststodelete as $postid) {
				wp_trash_post($postid);
			}
			wp_trash_post( $post_id );  // delete the original post
			wp_redirect( admin_url( '/edit.php?post_type=shift' ) );
		}

	}

}

// ------------------------------------------------------------------------
// APPLY CHANGES TO SUBSEQUENT SHIFTS IN SERIES 
// ------------------------------------------------------------------------

function wpaesm_apply_changes_to_series() {
	if( !isset( $_POST['shift'] ) ) {
		$results = __( 'Error.  No shifts were updated.', 'wpaesm' );
		die( $results );
	}

	$post_id = $_POST['shift'];

	global $shift_publish_metabox; // get metabox data
	$meta = $shift_publish_metabox->the_meta($post_id); 

	// gather all of this shift's information
	$title = get_the_title( $post_id );
	$content = get_post_field( 'post_content', $post_id );
	global $shift_metabox;
	$postmeta = $shift_metabox->the_meta( $post_id );
	$type = wp_get_post_terms( $post_id, 'shift_type', array("fields" => "ids") );
	// $status = wp_get_post_terms( $post_id, 'shift_status', array("fields" => "ids") );
	$employees = get_users( array(
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $post_id
	) );
	foreach( $employees as $employee ) {
		$employeeid = $employee->ID;
	}
	$clients = get_posts( array(
	  'connected_type' => 'shifts_to_clients',
	  'connected_items' => $post_id,
	  'nopaging' => true,
	  'suppress_filters' => false
	) );
	foreach( $clients as $client ) {
		$clientid = $client->ID;
	}

	// find all of the subsequent shifts
	$parent = wp_get_post_parent_id( $post_id );
	if( 0 === $parent ) {
		// this doesn't have a parent, so we need to get the children
		$args = array(
			'post_type' => 'shift',
			'posts_per_page' => '-1',
			'post_parent' => $post_id,
			'meta_key' => '_wpaesm_date',
			'meta_value' => $postmeta['date'],
			'meta_compare' => '>',
		);
	} else {
		// this is a child post, so we need to get the siblings
		$args = array(
			'post_type' => 'shift',
			'posts_per_page' => '-1',
			'post_parent' => $parent,
			'meta_key' => '_wpaesm_date',
			'meta_value' => $postmeta['date'],
			'meta_compare' => '>',
		);
	}
	
	$subsequent_shifts = new WP_Query( $args );

	$i = 0;
	$idlist = '';
	
	// update all subsequent shifts
	if ( $subsequent_shifts->have_posts() ) :
		while ( $subsequent_shifts->have_posts() ) : $subsequent_shifts->the_post();
			$id = get_the_id(); 
			// update basic post information
			$update = array(
				'ID' => $id,
				'post_title' => $title,
				'post_content' => $content,
				);
			wp_update_post( $update );
			// update shift meta: start time, end time
		    update_post_meta( $id, '_wpaesm_starttime', $postmeta['starttime'] );
		    update_post_meta( $id, '_wpaesm_endtime', $postmeta['endtime'] );

			// update shift type and status
			wp_set_post_terms( $id, $type, 'shift_type' );
			// wp_set_post_terms( $id, $status, 'shift_status' );

			// update connected users
			$oldemployees = get_users( array(
				'connected_type' => 'shifts_to_employees',
				'connected_items' => $id
			) );
			foreach( $oldemployees as $oldemployee ) {
				$oldemployeeid = $oldemployee->ID;
			}
			p2p_type( 'shifts_to_employees' )->disconnect( $id, $oldemployeeid );
			p2p_type( 'shifts_to_employees' )->connect( $id, $employeeid, array(
			    'date' => current_time('mysql')
			) );

			// update connected clients
			$oldclients = get_posts( array(
			  'connected_type' => 'shifts_to_clients',
			  'connected_items' => $id,
			  'nopaging' => true,
			  'suppress_filters' => false
			) );
			foreach( $oldclients as $oldclient ) {
				$oldclientid = $oldclient->ID;
			}
			p2p_type( 'shifts_to_clients' )->disconnect( $id, $oldclientid );
			p2p_type( 'shifts_to_clients' )->connect( $id, $clientid, array(
			    'date' => current_time('mysql')
			) );

			$i++;
		endwhile;
	endif;
	
	// Reset Post Data
	wp_reset_postdata();

	$results = $i;
	$results .= __( ' shifts were updated.', 'wpaesm' );

	die( $results );		
}
add_action( 'wp_ajax_wpaesm_apply_changes_to_series', 'wpaesm_apply_changes_to_series' );


// ------------------------------------------------------------------------
// CHECK FOR SCHEDULING CONFLICTS
// ------------------------------------------------------------------------

// ajax function to check for schedule conflicts when you publish a post
function wpaesm_check_for_schedule_conflicts_before_publish() {

	if( !isset( $_POST['employee'] ) || '' == $_POST['employee'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected an employee.  Save this shift anyway?', 'wpaesm' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['day'] ) || '' == $_POST['day'] || '____-__-__' == $_POST['day'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected a date.  Save this shift anyway?', 'wpaesm' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['start'] ) || '' == $_POST['start'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected a start time.  Save this shift anyway?', 'wpaesm' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['end'] ) || '' == $_POST['end'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected an end time.  Save this shift anyway?', 'wpaesm' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['client'] ) || 'false' == $_POST['client'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected a client.  Save this shift anyway?', 'wpaesm' );
		wp_send_json($results);    
		die();
	}

	$availability = wpaesm_check_employee_availability( $_POST['employee'], $_POST['day'], $_POST['start'], $_POST['end'] );
	if( true !== $availability ) {
		$employee = get_user_by( 'id', $_POST['employee'] );
		$results['action'] = 'stop';
		$results['message'] = sprintf( __( '%s is not available to work on %s between %s and %s.  Click "OK" to create the shift anyway, or "Cancel" to continue editing.', 'wpaesm' ), $employee->display_name, ucfirst( $availability['unavailable']['day'] ), $availability['unavailable']['start'], $availability['unavailable']['end'] );
		wp_send_json($results);    
		die();
	}

	$conflicts = wpaesm_check_employee_schedule_conflicts( $_POST['employee'], $_POST['day'], $_POST['start'], $_POST['end'], $_POST['post'] );
	if( true !== $conflicts ) {
		$employee = get_user_by( 'id', $_POST['employee'] );
		$results['action'] = 'stop';
		$results['message'] = sprintf( __( '%s is already scheduled for a shift at this time.  Click "OK" to create the shift anyway, or "Cancel" to continue editing.', 'wpaesm' ), $employee->display_name );
		wp_send_json($results);    
		die();
	}

	$results['action'] = 'go';
	$results['message'] = 'no conflicts!';
	wp_send_json($results);    
	die();
}
add_action( 'wp_ajax_wpaesm_check_for_schedule_conflicts_before_publish', 'wpaesm_check_for_schedule_conflicts_before_publish' );


// check that this shift does not fall within employee's regular unavailability time, as saved in user meta
function wpaesm_check_employee_availability( $employee_id, $day, $start_time, $end_time ) {
	// get the employee meta
	$unavailability = get_the_author_meta( 'unavailable', $employee_id );
	// if the employee does not have any unavailability saved, return true
	if( !is_array( $unavailability ) || empty( $unavailability ) ) {
		return true;
	} else { // if the employee does have unavailability
		// figure out what day of the week the shift date is
		$day_of_week = strtolower( date( 'l', strtotime( $day ) ) );
		foreach( $unavailability as $unavail ) {
			if( $unavail['day'] == $day_of_week ) {// if the day of the week is a day with unavailability
				// find the timeframe and see if they overlap
				$shift_start = str_replace( ':', '', $start_time );
				$shift_end = str_replace( ':', '', $end_time );
				$unavailibility_start = str_replace( ':', '', $unavail['start'] );
				$unavailability_end = str_replace( ':', '', $unavail['end'] );
				if( empty( $unavailibility_start ) || empty( $unavailability_end ) ) {
					$unavailable_time['unavailable'] = $unavail;
					$unavailable_time['employee'] = $employee_id;
					return $unavailable_time;
				}
				if( ( $shift_start >= $unavailibility_start && $shift_start <= $unavailability_end ) || 
					( $shift_end >= $unavailibility_start && $shift_end <= $unavailability_end ) || 
					( $shift_start <= $unavailibility_start && $shift_end >= $unavailability_end ) ) 
				{
					$unavailable_time['unavailable'] = $unavail;
					$unavailable_time['employee'] = $employee_id;
					return $unavailable_time;
				} 
			} 
		}

	}

	// we've gotten this far, so the employee must be available
	return true;

}

// check that the employee isn't scheduled for another shift at the same time
function wpaesm_check_employee_schedule_conflicts( $employee_id, $day, $start_time, $end_time, $post_id ) {
	// query to see if any shifts are assigned to the employee on this day
	$args = array( 
	    'post_type' => 'shift',
	    'post_status' => 'any',
	    'posts_per_page' => -1,
	    'meta_key' => '_wpaesm_date',
	    'meta_value' => $day, 
	    'connected_type' => 'shifts_to_employees',
	    'connected_items' => $employee_id
	);
	
	$conflicting_shifts = new WP_Query( $args );
	
	// The Loop
	if ( $conflicting_shifts->have_posts() ) {
		while ( $conflicting_shifts->have_posts() ) : $conflicting_shifts->the_post();
			$conflicting_shift = get_the_id();
			if( intval( $conflicting_shift ) !== intval( $post_id ) ) { // make sure conflicting shift is not the shift we are trying to save
				// check to see if the times overlap
				$shift_start = str_replace( ':', '', $start_time );
				$shift_end = str_replace( ':', '', $end_time );
				global $shift_metabox;
				$meta = $shift_metabox->the_meta();
				$conflict_start = str_replace( ':', '', $meta['starttime'] );
				$conflict_end = str_replace( ':', '', $meta['endtime'] );
				if( ( $shift_start >= $conflict_start && $shift_start <= $conflict_end ) || ( $shift_end >= $conflict_start && $shift_end <= $conflict_end ) || ( $shift_start <= $conflict_start && $shift_end >= $conflict_end ) ) {
					return $conflicting_shift;
				} 
			}
		endwhile;
		wp_reset_postdata();
		// we got this far, so there must not be any conflicts
		return true;
	} else {
		return true;
	}
}

// calculate duration between two times
function wpaesm_calculate_duration( $start, $end ) {
	$a = new DateTime( $start );
	$b = new DateTime( $end );
	$sched_duration = $a->diff( $b );
	return $sched_duration->format( "%H:%I" );
}

// ------------------------------------------------------------------------
// EMAIL PAYROLL REPORT TO EMPLOYEES
// SCC ONLY
// ------------------------------------------------------------------------

if( isset( $options['email_payroll'] ) && "1" == $options['email_payroll'] ) {
	if ( ! wp_next_scheduled( 'wpaesm_email_payroll' ) ) {
		wp_schedule_event( time(), 'daily', 'wpaesm_email_payroll' );
	}

	add_action( 'wpaesm_email_payroll', 'wpaesm_send_payroll_email' );

	function wpaesm_send_payroll_email() {
		$options = get_option( 'wpaesm_options' ); 
		$today = date( "d", current_time( 'timestamp' ) );
		if( $today == "01" || $today == "16" ) {
			if( $today == "01" ) {
				$start = date( 'Y-m-01', strtotime( '-1 month', current_time( 'timestamp' ) ) );
				$end = date( 'Y-m-15', strtotime( '-1 month', current_time( 'timestamp' ) ) );
			} elseif( $today == "16") {
				$start = date( 'Y-m-01', current_time( "timestamp" ) );
				$end = date( 'Y-m-15', current_time( "timestamp" ) );
			}
			
			$employees = get_users( 'role=employee' );
			foreach( $employees as $employee ) {
				$to = $employee->user_email;
				$subject = "Timesheet for " . $start . " to " . $end;
				$message = '<p>Your timesheet information is below.  Please go to <a href="' . home_url() . '/your-profile/?tab=timesheet">Your Profile page</a> to review and approve your timesheet by 5pm today.</p>';
				$message .= "<p><strong>Overtime hours:</strong> " . wpaesm_overtime($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Regular hours:</strong> " . wpaesm_reghours($employee->ID, $start, $end)  . "</p>";
				$message .= "<p><strong>Subtotal hours:</strong> " . wpaesm_workedhours($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Paid Time Off hours:</strong> " . wpaesm_ptohours($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Total hours:</strong> " . wpaesm_totalhours($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Mileage:</strong> " . wpaesm_mileage($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Receipts:</strong> " . wpaesm_receipts($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Deduction:</strong> " . wpaesm_deduction($employee->ID, $start, $end) . "</p>";
				$message .= "<p><strong>Gross Payment:</strong> " . wpaesm_payment($employee->ID, $start, $end) . "</p>";
				$from = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
				$headers[] = "Cc: " . $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";
				wpaesm_send_email( $from, $to, '', $subject, $message, '' );
			}
		}
	}
}

// email again if they haven't approved it by 5pm
if ( ! wp_next_scheduled( 'wpaesm_email_payroll_reminder' ) ) {
	wp_schedule_event( time(), 'daily', 'wpaesm_email_payroll_reminder' );
}

add_action( 'wpaesm_email_payroll_reminder', 'wpaesm_send_payroll_email_reminder' );

function wpaesm_send_payroll_email_reminder() {
	$options = get_option( 'wpaesm_options' ); 
	$today = date( "d", current_time( 'timestamp' ) );
	$hour = date( 'H', current_time( 'timestamp' ) );
	if( $today == "01" || $today == "16" ) {
		if( '16' == $hour ) {
			
			$employees = get_users( 'role=employee' );
			foreach( $employees as $employee ) {
				$timestamp = get_user_meta( $employee->ID, 'last_timesheet', true );
				if( isset( $timestamp ) && '' !== $timestamp ) {
					// if the last time the employee approved a timesheet was today, then stop
					$date = date( 'Y-m-d', $timestamp );
					if( $date == date( 'Y-m-d', current_time( 'timestamp' ) ) ) {
						return;
					}
				} 

				if( $today == "01" ) {
					$start = date( 'Y-m-01', strtotime( '-1 month', current_time( 'timestamp' ) ) );
					$end = date( 'Y-m-15', strtotime( '-1 month', current_time( 'timestamp' ) ) );
				} elseif( $today == "16") {
					$start = date( 'Y-m-01', current_time( "timestamp" ) );
					$end = date( 'Y-m-15', current_time( "timestamp" ) );
				}

				$to = $employee->user_email;
				$subject = 'Reminder: please review your timesheet';
				$message = 
					'<p>Your timesheet is due at 5pm today.  Please go to <a href="' . home_url() . '/your-profile/?tab=timesheet">Your Profile page</a> to review and approve your timesheet.</p>';

				$from = $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";
				$headers[] = "Cc: " . $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";
				wpaesm_send_email( $from, $to, '', $subject, $message, '' );
			}
		}
	}
}


function wpaesm_create_shift_serialized_array( $shift_id ) {
	$data = array( 
		'_wpaesm_date',
		'_wpaesm_starttime',
		'_wpaesm_endtime',
		'_wpaesm_clockin',
		'_wpaesm_clockout',
		'_wpaesm_location_in',
		'_wpaesm_location_out',
		'_wpaesm_notify',
		'_wpaesm_break',
		'_wpaesm_doc_approved',
		'_wpaesm_doc_reminder',
		'_wpaesm_shiftnotes',
		'_wpaesm_lastnote',
		'_wpaesm_employeenote',
	);
	
	$str = $data;
	update_post_meta( $shift_id, 'shift_meta_fields', $str );
}

// Require Client Manager - SCC only
require_once( plugin_dir_path( __FILE__ ) . 'scc-client-manager.php' );


?>