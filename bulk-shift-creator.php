<?php
function wpaesm_add_bulk_creator_page() {
	global $bulk_page;
	$bulk_page = add_submenu_page( 'edit.php?post_type=shift', 'Add Repeating Shifts', 'Add Repeating Shifts', 'manage_options', 'add-repeating-shifts', 'wpaesm_bulk_shift_form' );
}

function wpaesm_bulk_shift_form() { 
	// get a list of all of the repeating days
	$daysofweek = array();
		if( isset( $_POST['repeatmon'] ) && "Monday" == $_POST['repeatmon'] ) {
			array_push($daysofweek, $_POST['repeatmon']);
		}
		if( isset( $_POST['repeattues'] ) && "Tuesday" == $_POST['repeattues'] ) {
			array_push($daysofweek, $_POST['repeattues']);
		}
		if( isset( $_POST['repeatweds'] ) && "Wednesday" == $_POST['repeatweds'] ) {
			array_push($daysofweek, $_POST['repeatweds']);
		}
		if( isset( $_POST['repeatthurs'] ) && "Thursday" == $_POST['repeatthurs'] ) {
			array_push($daysofweek, $_POST['repeatthurs']);
		}
		if( isset( $_POST['repeatfri'] ) && "Friday" == $_POST['repeatfri'] ) {
			array_push($daysofweek, $_POST['repeatfri']);
		}
		if( isset( $_POST['repeatsat'] ) && "Saturday" == $_POST['repeatsat'] ) {
			array_push($daysofweek, $_POST['repeatsat']);
		}
		if( isset( $_POST['repeatsun'] ) && "Sunday" == $_POST['repeatsun'] ) {
			array_push($daysofweek, $_POST['repeatsun']);
		}
	// get a list of dates on which the shift repeats
	if( isset( $_POST['thisdate'] ) && isset( $_POST['repeatuntil'] ) ) {
		$begindate = $_POST['thisdate'];
		$enddate = $_POST['repeatuntil'];
	}
	if( isset( $begindate ) && isset( $enddate) ) {
		$shiftdates = wpaesm_get_repeating_dates( $begindate, $enddate, $daysofweek );
	}

	if( isset( $_POST['client'] ) && '' !== $_POST['client'] ) {
		$client = get_post($_POST['client']);
		$clientname = $client->post_title;
	}

	$total = 0;
	$schedule_conflicts = '';
	$conflicting_shifts = '';
	if( isset( $_POST['employees'] ) && is_array( $_POST['employees'] ) ) {
		foreach( $_POST['employees'] as $employee ) {
			// insert posts with all of the same meta data
			$i = 0;
			if( isset( $shiftdates ) ) {
				foreach($shiftdates as $shiftdate) {
					$post_name = wpaesm_generate_post_name( $_POST['shift-name'] );

					if( 0 == $i ) {
						$newshift = array(
							'post_type'     => 'shift',
							'post_title'    => sanitize_text_field( $_POST['shift-name'] ),
							'post_name'     => $post_name,
							'post_status'   => 'publish',
						);
					} else {
						$newshift = array(
							'post_type'     => 'shift',
							'post_title'    => sanitize_text_field( $_POST['shift-name'] ),
							'post_name'     => $post_name,
							'post_status'   => 'publish',
							'post_parent'	=> $first,
						);
					}
					$createdshift = wp_insert_post($newshift);
					if( 0 == $i ) {
						$first = $createdshift;
					} 		
					wp_set_object_terms( $createdshift, $_POST['type'], 'shift_type' );
					wp_set_object_terms( $createdshift, $_POST['status'], 'shift_status' );
					// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
					wpaesm_create_shift_serialized_array( $createdshift );

					add_post_meta( $createdshift, '_wpaesm_date', $shiftdate );
					add_post_meta( $createdshift, '_wpaesm_starttime', $_POST['starttime'] );
					add_post_meta( $createdshift, '_wpaesm_endtime', $_POST['endtime'] );

					// Create connections
					if( isset( $_POST['client'] ) ) {
						p2p_type( 'shifts_to_clients' )->connect( $createdshift, $_POST['client'], array(
						    'date' => current_time('mysql')
						) );
					}
					p2p_type( 'shifts_to_employees' )->connect( $createdshift, $employee, array(
					    'date' => current_time('mysql')
					) );

					// check for conflicts
					$availability = wpaesm_check_employee_availability( $employee, $shiftdate, $_POST['starttime'], $_POST['endtime'] );
					if( true !== $availability ) {
						$employee_object = get_user_by( 'id', $employee );
						$schedule_conflicts .= '<p>' . sprintf( __( '%s is not available to work on %s between %s and %s.', 'wpaesm' ), $employee_object->display_name, ucfirst( $availability['unavailable']['day'] ), $availability['unavailable']['start'], $availability['unavailable']['end'] ) . '<br />';
						$schedule_conflicts .= '<a href="' . get_edit_post_link( $createdshift ) . '" target="_blank">' . __( 'Edit new shift', 'wpaesm' ) . '</a> | ';
						$schedule_conflicts .= '<a href="' . get_delete_post_link( $createdshift ) . '" target="_blank">' . __( 'Delete new shift', 'wpaesm' ) . '</a></p>';
					}
					$conflicting = wpaesm_check_employee_schedule_conflicts( $employee, $shiftdate, $_POST['starttime'], $_POST['endtime'], $createdshift );
					if( true !== $conflicting ) {
						$schedule_conflicts .= '<p>' . sprintf( __( '%s is already scheduled for a shift at this time on %s.', 'wpaesm' ), $employee_object->display_name, $shiftdate ) . '<br />';
						$schedule_conflicts .= '<a href="' . get_edit_post_link( $createdshift ) . '" target="_blank">' . __( 'Edit new shift', 'wpaesm' ) . '</a> | ';
						$schedule_conflicts .= '<a href="' . get_delete_post_link( $createdshift ) . '" target="_blank">' . __( 'Delete new shift', 'wpaesm' ) . '</a> | ';
						$schedule_conflicts .= '<a href="' . get_edit_post_link( $conflicting ) . '" target="_blank">' . __( 'Edit existing shift', 'wpaesm' ) . '</a> | ';
						$schedule_conflicts .= '<a href="' . get_delete_post_link( $conflicting ) . '" target="_blank">' . __( 'Delete existing shift', 'wpaesm' ) . '</a></p>';
		
					}
					$i++;
				}

				if(isset($_POST['notify']) && $_POST['notify'] == "1") {
					$date = $begindate;
					$repeatdays = $daysofweek;
					$repeatuntil = $enddate;
					wpaesm_send_notification_email($employee, $clientname, $date, $_POST['starttime'], $_POST['endtime'], $repeatdays, $repeatuntil);
				}
			} 
			$total += $i;
		}
	} else {
		// do all the same stuff, but without the employee
		$i = 0;
		if( isset( $shiftdates ) ) {
			foreach($shiftdates as $shiftdate) {
				$post_name = wpaesm_generate_post_name( $_POST['shift-name'] );

				if( 0 == $i ) {
					$newshift = array(
						'post_type'     => 'shift',
						'post_title'    => sanitize_text_field( $_POST['shift-name'] ),
						'post_name'     => $post_name,
						'post_status'   => 'publish',
					);
				} else {
					$newshift = array(
						'post_type'     => 'shift',
						'post_title'    => sanitize_text_field( $_POST['shift-name'] ),
						'post_name'     => $post_name,
						'post_status'   => 'publish',
						'post_parent'	=> $first,
					);
				}
				$createdshift = wp_insert_post($newshift);
				if( 0 == $i ) {
					$first = $createdshift;
				} 		
				wp_set_object_terms( $createdshift, $_POST['type'], 'shift_type' );
				wp_set_object_terms( $createdshift, $_POST['status'], 'shift_status' );
				// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
				wpaesm_create_shift_serialized_array( $createdshift );

				add_post_meta( $createdshift, '_wpaesm_date', $shiftdate );
				add_post_meta( $createdshift, '_wpaesm_starttime', $_POST['starttime'] );
				add_post_meta( $createdshift, '_wpaesm_endtime', $_POST['endtime'] );

				// Create connections
				if( isset( $_POST['client'] ) ) {
					p2p_type( 'shifts_to_clients' )->connect( $createdshift, $_POST['client'], array(
					    'date' => current_time('mysql')
					) );
				}

				$i++;
			}
		} 
		$total += $i;
	}

// RENDER FORM	?>
	<div class="wrap">
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<?php if($total > 0) { ?>
			<div id="message" class="updated" style="padding: 15px;">
				<?php _e('Created ' . $total . ' new shifts.  <a href="' . get_bloginfo('wpurl') . '/wp-admin/edit.php?post_type=shift">View all shifts', 'wpaesm'); ?></a>.
			</div>
			<?php if( !empty( $schedule_conflicts ) || !empty( $conflicting_shifts ) ) { ?>
				<div id="conflicts" class="error" style="padding: 15px">
					<?php if ( !empty( $schedule_conflicts ) ) { ?>
						<p><strong><?php _e( 'Some of the new shifts occur when employees are unavailable:', 'wpaesm' ); ?></strong></p>
						<?php echo $schedule_conflicts;
					} ?>
					<?php if ( !empty( $conflicting_shifts ) ) { ?>
						<p><strong><?php _e( 'Some of the new shifts occur when employees are scheduled for other shifts:', 'wpaesm' ); ?></strong></p>
						<?php echo $conflicting_shifts;
					} ?>
				</div>
			<?php }
		} ?>

		<h1><?php _e('Add Repeating Shifts', 'wpaesm'); ?></h1>
		<p><?php _e('If you have a shift that repeats on a regular basis with the same employee, client, and time, you can bulk add it here instead of having to create each shift individually.  If you choose several employees, a separate series of shifts will be created for each employee.', 'wpaesm'); ?></p>

		<form id="bulk-shifts" action="<?php bloginfo('wpurl') ?>/wp-admin/edit.php?post_type=shift&page=add-repeating-shifts" method="post">
			<table class="form-table">
				<tr>
					<th scope="row"><?php _e('Shift Name', 'wpaesm'); ?></th>
					<td>
						<input type="text" size="60" name="shift-name" value="" required />
					</td>
				</tr>	
				<tr>
					<th scope="row"><?php _e('Client', 'wpaesm'); ?></th>
					<td>
						<?php $args = array( 
						    'post_type' => 'client',  
						    'posts_per_page' => -1,  
						    'orderby' => 'name',
						    'order' => 'asc',
						);

						$clientquery = new WP_Query( $args );

						if ( $clientquery->have_posts() ) : ?>
							<select name="client" required>
								<option value=""></option>
								<?php while ( $clientquery->have_posts() ) : $clientquery->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endwhile; ?>
							</select>
						<?php endif;
						wp_reset_postdata(); ?>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Employee(s)', 'wpaesm' ) ?></th>
					<td>
						<!-- <select name="employee">
							<option value=""></option> -->
						<ul id="employees">
							<?php $employees = array_merge( get_users( 'role=employee' ), get_users( 'role=administrator' ) );
							usort( $employees, 'wpaesm_alphabetize' );
							foreach ( $employees as $employee ) { ?>
								<li><input type="checkbox" name="employees[]" value="<?php echo $employee->ID; ?>" ><?php echo $employee->display_name; ?></li>
							<?php } ?>
						</ul>
						<!-- </select> -->


					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e('This shift repeats every:', 'wpaesm') ?></th>
					<td>
						<ul>
							<li>
								<input type="checkbox" name="repeatmon" value="Monday"/> <?php _e('Monday', 'wpaesm'); ?>
							</li>
							<li>
								<input type="checkbox" name="repeattues" value="Tuesday"/> <?php _e('Tuesday', 'wpaesm'); ?>
							</li>
							<li>
								<input type="checkbox" name="repeatweds" value="Wednesday"/> <?php _e('Wednesday', 'wpaesm'); ?>
							</li>
							<li>
								<input type="checkbox" name="repeatthurs" value="Thursday"/> <?php _e('Thursday', 'wpaesm'); ?>
							</li>
							<li>
								<input type="checkbox" name="repeatfri" value="Friday"/> <?php _e('Friday', 'wpaesm'); ?>
							</li>
							<li>
								<input type="checkbox" name="repeatsat" value="Saturday"/> <?php _e('Saturday', 'wpaesm'); ?>
							</li>
							<li>
								<input type="checkbox" name="repeatsun" value="Sunday"/> <?php _e('Sunday', 'wpaesm'); ?>
							</li>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e('Dates', 'wpaesm'); ?></th>
					<td>
						From <input type="text" size="10" name="thisdate" id="thisdate" value="" required /> to <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" required />
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Time', 'wpaesm'); ?></th>
					<td>
						From <input type="text" size="10" name="starttime" id="starttime" value="" required /> to <input type="text" size="10" name="endtime" id="endtime" value="" required />
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e('Shift Type', 'wpaesm'); ?></th>
					<td>
						<select name="type">
							<option value=""></option>
							<?php $shifttypes = get_terms( 'shift_type', 'hide_empty=0' );
							foreach ( $shifttypes as $type ) { ?>
								<option value="<?php echo $type->slug; ?>" ><?php echo $type->name; ?></option>
							<?php } ?>
						</select>

					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Shift Status', 'wpaesm'); ?></th>
					<td>
						<select name="status">
							<option value=""></option>
							<?php $shift_statuses = get_terms( 'shift_status', 'hide_empty=0' );
							foreach ( $shift_statuses as $status ) { ?>
								<option value="<?php echo $status->slug; ?>" ><?php echo $status->name; ?></option>
							<?php } ?>
						</select>

					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Notify Employee', 'wpaesm'); ?></th>
					<td>
						<span><?php _e('Employee will only recieve one email', 'wpaesm'); ?><br /></span>
						<input type="checkbox" name="notify" value="1"/> <?php _e('Notify Employee', 'wpaesm'); ?>
					</td>					
				</tr>

			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Create Shifts') ?>" />
			</p>
		</form>

	</div>
<?php }

// ------------------------------------------------------------------------
// GET REPEATING DATES
// ------------------------------------------------------------------------

function wpaesm_get_repeating_dates( $begindate, $enddate, $daysofweek ) {
    $repeaton = array();

    $min_date = strtotime($begindate);
    $max_date = min(strtotime('+6 months'), strtotime($enddate));
    $dow = array_values($daysofweek);

    while ($min_date <= $max_date) {
    	$date = date('Y-m-d', $min_date);
        if (in_array(date('l', $min_date), $daysofweek)) {
            array_push($repeaton, $date);
        }
        $min_date = strtotime('+1 day', $min_date);
    }

    return $repeaton;
}

function wpaesm_alphabetize( $a, $b ) {
   return strcmp( $a->display_name, $b->display_name );
}

function wpaesm_alphabetize_first( $a, $b ) {
   return strcmp( $a->first_name, $b->first_name );
}

// generate unique post slug
function wpaesm_generate_post_name( $title ) {
	$stripped_title = preg_replace( "/[^a-z0-9.]+/i", "", $title );
	$random_string = substr( str_shuffle( MD5( microtime() ) ), 0, 8 );  // http://derek.io/blog/2009/php-a-better-random-string-function/
	$unique_title = $stripped_title . '-' . $random_string;

	return $unique_title;
}

?>