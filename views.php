<?php // SCC

if ( ! defined( 'ABSPATH' ) ) exit;

// ------------------------------------------------------------------------
// SINGLE SHIFT VIEW
// ------------------------------------------------------------------------

function wpaesm_single_shift_title( $title ) {
	global $post;
    if( is_singular( 'shift' ) && $title == $post->post_title && is_main_query() ) {
        $title = "Shift Details: " . $title;
    }
    return $title;
}
add_filter( 'the_title', 'wpaesm_single_shift_title', 10, 2 );

function wpaesm_single_shift_scripts() {

	if( is_singular( 'shift' ) ) {
		wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
		wp_localize_script( 'wpaesm_scripts', 'shiftajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	}
}
add_action( 'wp_enqueue_scripts', 'wpaesm_single_shift_scripts' );

function wpaesm_single_shift_view( $content ) {
	if( is_singular( 'shift' ) && is_main_query() ) {
		if( is_user_logged_in() && ( wpaesm_check_user_role( 'employee' ) || wpaesm_check_user_role( 'administrator' ) ) ) { // only show this if current user is admin or employee
			global $post;

			// get employee associated with this shift
			$users = get_users( array(
				'connected_type' => 'shifts_to_employees',
				'connected_items' => $post->ID
			) );
			foreach( $users as $user ) {
				$employee = $user->display_name;
				$employeeid = $user->ID;
			}

			// Process forms, if we need to
			// status change
			if( isset( $_POST['form_name'] ) && "status" == ( $_POST['form_name'] ) ) {
				wpaesm_change_shift_status( $post, $employee );
			}

			// if employee left a note
			if( isset( $_POST['form_name'] ) && "employee_note" == ( $_POST['form_name'] ) ) {
				wpaesm_save_employee_note( $post, $employee );
			}

			// If employee just pushed the clock in button
			if( isset( $_POST['form_name'] ) && "clockin" == ( $_POST['form_name'] ) ) {
				wpaesm_clock_in( $post );
			}

			// If employee just pushed the clock out button
			if( isset( $_POST['form_name'] ) && "clockout" == ( $_POST['form_name'] ) ) {
				wpaesm_clock_out( $post );
			}

			// gather all of the variables we need
			$current_user = wp_get_current_user(); // get current user - we'll need it later
			global $shift_metabox; // get metabox data
			$meta = $shift_metabox->the_meta(); 
			
			// get client associated with this shift
			$clients = get_posts( array(
				'connected_type' => 'shifts_to_clients',
				'connected_items' => $post->ID,
				'nopaging' => true,
				'suppress_filters' => false
			) );
			foreach($clients as $client) {
				$clientname = $client->post_title;
			}
			$options = get_option('wpaesm_options'); // get options
			$starttime = date("g:i a", strtotime($meta['starttime']));
			$endtime = date("g:i a", strtotime($meta['endtime']));
			if(isset($meta['clockin'])) {
				$clockin = date("g:i a", strtotime($meta['clockin']));
			}
			if(isset($meta['clockout'])) {
				$clockout = date("g:i a", strtotime($meta['clockout']));
			}
			$date = date("D M j", strtotime($meta['date']));
			$typelist =  wp_get_post_terms( $post->ID, 'shift_type' );
			$types = '';
			foreach($typelist as $type) {
				$types .= $type->name;
			}
			$statuslist =  wp_get_post_terms( $post->ID, 'shift_status' );
			$statuses = '';
			foreach($statuslist as $status) {
				$statuses .= $status->name;
			}



			// BEGIN SHIFT VIEW
			global $shiftcontent;
			$shiftcontent = '';
			// global $shiftcontent;  // why did I put this here?  I must have had a reason, but it was causing the shift content to appear twice, so I removed it


			// if the employee is viewing the shift, and if it is today, show clock in/out buttons
			$today = current_time( "Y-m-d" );

			if( isset( $employeeid ) && $employeeid == $current_user->ID && $today == $meta['date'] && empty($meta['clockout'] ) ) {
				$need_geolocation = true; // this tells us to load geolocation script later
				if( isset( $meta['clockin'] ) && '' !== $meta['clockin'] ) { // employee has already clocked in, so show the clock out button
					$shiftcontent .= "<form method='post' action='" . get_the_permalink() . "' id='clock' class='clock-form'>";
					$shiftcontent .= "<div class='extra-clock-fields'></div>";
					$shiftcontent .= "<input type='hidden' name='form_name' id='form_name' value='clockout'>";
					$shiftcontent .= "<input type='hidden' name='clock-out' value='clock-out'>";
					$shiftcontent .= "<input type='hidden' name='scheduled' id='scheduled' value='" . strtotime( $meta['endtime'] . ' ' . $meta['date']) . "'>";
					if( isset( $options['geolocation'] ) && $options['geolocation'] == 1 ) { // geolocation field, if we're using it
						$shiftcontent .= "<input type='hidden' id='latitude' name='latitude' value=''>";
						$shiftcontent .= "<input type='hidden' id='longitude' name='longitude' value=''>";
					}
					$shiftcontent .= '<input type="checkbox" id="break" name="break" value="1"><label> I took a 30-minute break during this shift.</label>';
					$shiftcontent .= '<br /><label>CIC Miles:</label>&nbsp;<input type="number" step="0.1" min="0" name="mileage" id="mileage">';
					$shiftcontent .= '<p class="where" style="display: none;"><label>Where did you go?</label>&nbsp;<input type="text" id="where" name="where"></p>';
					$shiftcontent .= "<p>" . __( 'You clocked in at', 'wpaesm') . "&nbsp;" . $meta['clockin'] . "</p>";
					$shiftcontent .= "<input name='wpaesm_clockout_nonce' id='wpaesm_clockout_nonce' type='hidden' value='" . wp_create_nonce( 'wpaesm_clockout_nonce' ) . "'>";
					$shiftcontent .= "<input type='submit' class='clock-button' value='" . __('Clock Out', 'wpaesm') . "' id='clock-out'>";
					$shiftcontent .= "</form>";
				} else { // employee has not clocked in, so show the clock in button
					$shiftcontent .= "<form method='post' action='" . get_the_permalink() . "' id='clock' class='clock-form'>";
					$shiftcontent .= "<div class='extra-clock-fields'></div>";
					$shiftcontent .= "<input type='hidden' name='form_name' id='form_name' value='clockin'>";
					$shiftcontent .= "<input type='hidden' name='clock-in' value='clock-in'>";
					$shiftcontent .= "<input type='hidden' name='scheduled' id='scheduled' value='" . strtotime( $meta['starttime'] . ' ' . $meta['date']) . "'>";
					if( isset( $options['geolocation'] ) && $options['geolocation'] == 1 ) { // geolocation field, if we're using it
						$shiftcontent .= "<input type='hidden' id='latitude' name='latitude' value=''>";
						$shiftcontent .= "<input type='hidden' id='longitude' name='longitude' value=''>";
					}
					$shiftcontent .= "<input name='wpaesm_clockin_nonce' id='wpaesm_clockin_nonce' type='hidden' value='" . wp_create_nonce( 'wpaesm_clockin_nonce' ) . "'>";
					$shiftcontent .= "<input type='submit' class='clock-button' value='" . __('Clock In', 'wpaesm') .  "' id='clock-in'>";
					$shiftcontent .= "</form>";
				}
				
			}


			if( isset( $employee ) ) {
				$shiftcontent .= "<p><strong>" . __('Employee:', 'wpaesm') . "</strong> " . $employee . "</p>";
			}
			if( isset( $clientname ) ) {
				$shiftcontent .= "<p><strong>" . __('Client:', 'wpaesm') . "</strong> " . $clientname . "</p>";
			}
			$shiftcontent .= "<p><strong>" . __('When: ', 'wpaesm') . "</strong> " . $date;
			if( 'Extra' !== $types ) {
				$shiftcontent .= ", " . __('from ', 'wpaesm') . $starttime . __(' to ', 'wpaesm') . $endtime ;
			} 
			$shiftcontent .= "</p>";
			if( isset( $clockin ) ) {
				$shiftcontent .= "<p><strong>Hours Worked: </strong>" . $clockin . " to " . $clockout . "</p>";
			}
			if( $types != '' ) {
				$shiftcontent .= "<p><strong>" . __('Shift Type: ', 'wpaesm') . "</strong> " . $types . "</p>";
			}

			// display employee notes, if any exist
			if( isset( $meta['employeenote'] ) && is_array( $meta['employeenote'] ) ) {
				$employeenotes = $meta['employeenote'];
				$shiftcontent .= "<strong>" . __( 'Notes', 'wpaesm' ) . "</strong>";
				foreach( $employeenotes as $note ) {
					if( isset( $note['notedate'] ) && isset( $note['notetext'] ) ) {
						$shiftcontent .= "<p><strong>" . $note['notedate'] . ":</strong> " . $note['notetext'] . "</p>";
					}
				}
			}

			// display the form for employee to add notes (only visible to employee assigned to shift)
			if( isset( $employeeid ) && $employeeid == $current_user->ID ) {
				$shiftcontent .= "<form method='post' action='" . get_the_permalink() . "' id='shift-note'>";
				$shiftcontent .= "<input type='hidden' name='form_name' value='employee_note'>";
				$shiftcontent .= "<label>" . __('Add a note about this shift, such as corrections to your clock-in and clock-out times.', 'wpaesm') . "</label>";
				if(isset($options['admin_notify_note']) && $options['admin_notify_note'] == 1) {
					$shiftcontent .= "<p>" . __('The site admin will receive an email with your note', 'wpaesm') . "</p>";
				}
				$shiftcontent .= "<textarea name='note'></textarea>";
				$shiftcontent .= "<input name='wpaesm_employee_note_nonce' id='wpaesm_employee_note_nonce' type='hidden' value='" . wp_create_nonce( 'wpaesm_employee_note_nonce' ) . "'>";
				$shiftcontent .= "<input type='submit' value='" . __('Add Note', 'wpaesm') . "'>";
				$shiftcontent .= "</form>";
			}

			// Find connected documents
			$docs = new WP_Query( array(
			  'connected_type' => 'documents_to_shifts',
			  'connected_items' => get_the_id(),
			  'posts_per_page' => -1,   
			) );

			if ( $docs->have_posts() ) :
				$shiftcontent .= '<br /><br /><h3>Documents</h3>';
				$shiftcontent .= '<div id="docs">';
					while ( $docs->have_posts() ) : $docs->the_post();
						$shiftcontent .= '<div class="document">';
					    	$docid = get_the_id();
					    	global $document_metabox;
						    $docmeta = $document_metabox->the_meta();
						    if( isset( $docmeta['ratings'] ) ) {
						    	$ratings = $docmeta['ratings'];
						    }
						    $shiftcontent .= '<div><strong>Notes:</strong><br />' . get_the_content() . '</div>';
						    if( !empty( $ratings ) ) {
						    	$shiftcontent .= '<p><strong>Behavior Goals: </strong><ul>';
				    			foreach( $ratings as $rating ) { 
				    				$shiftcontent .= '<li>' . $rating['goal'] . ': ' . $rating['score'] . '</li>';
				    			}
						    	$shiftcontent .= '</ul></p>';
						    }
						$shiftcontent .= '</div>';
					endwhile;
					wp_reset_postdata();
				$shiftcontent .= '</div>';
			endif; 

			// Show edit shift link to admins
			if ( current_user_can( 'edit_post', $post->ID ) ) {
				$shiftcontent .= "<p class='edit'><a href='" . get_edit_post_link() . "'>" . __('Edit this shift', 'wpaesm') . "</a></p>";
			}

			// geolocation JS, if needed
			$options = get_option('wpaesm_options');
			if( isset( $need_geolocation ) && isset( $options['geolocation'] ) && 1 == $options['geolocation'] ) {
				$shiftcontent .= "
					<script type='text/javascript'>
					window.onload = getLocationConstant;

					function getLocationConstant()
					{
					    if(navigator.geolocation)
					    {
					        navigator.geolocation.getCurrentPosition(onGeoSuccess,onGeoError);  
					    } else {
					        alert('Your browser or device does not support Geolocation');
					    }
					}

					function onGeoSuccess(event)
					{
					    document.getElementById('latitude').value =  event.coords.latitude; 
					    document.getElementById('longitude').value = event.coords.longitude;

					}

					function onGeoError(event)
					{
						document.getElementById('latitude').value =  event.message; 
					    document.getElementById('longitude').value = event.message;
					}
					</script>";
			}

			$content .= $shiftcontent;
		} else {
			$shiftcontent = "<p>" . __('You must be logged in to view this page.', 'wpaesm') . "</p>";
			$args = array(
		        'echo' => false,
			); 
			$shiftcontent .= wp_login_form($args);

			$content .= $shiftcontent;
		}
	} 

	return $content;
}
add_filter( 'the_content', 'wpaesm_single_shift_view' );


// ------------------------------------------------------------------------
// PROCESS FORMS IN SINGLE SHIFT VIEW
// ------------------------------------------------------------------------

add_action( 'wp_ajax_nopriv_wpaesm_early_or_late_form', 'wpaesm_early_or_late_form' ); // only need this on front end
add_action( 'wp_ajax_wpaesm_early_or_late_form', 'wpaesm_early_or_late_form' );
function wpaesm_early_or_late_form() {

	if( !isset( $_POST['form'] ) || '' == $_POST['form'] ) {
		return;
	}

	if( !isset( $_POST['offset'] ) || '' == $_POST['offset'] ) {
		return;
	}

	if( 'clockin' == $_POST['form'] && 'early' == $_POST['offset'] ) {

		$form = '<div class="early failure">
					<label>Why are you clocking in early? *</label>
					<select class="reason" name="reason">
						<option value=""></option>
						<option value="Early arrival: waited to work">Early arrival: waited to work</option>
						<option value="Early arrival: started working">Early arrival: started working</option>
						<option value="Other">Other</option>
					</select>
					<div class="clock-other" style="display:none;">
						<label>Other reason:</label>
						<textarea name="other"></textarea>
					</div>
					<label>Additional notes:</label>
					<textarea name="additional-note"></textarea>
					<label>Approved by manager</label>
					<input type="radio" name="approved" value="Approved by manager">Yes <br />
					<input type="radio" name="approved" value="Not approved by manager">No <br />
					<input type="radio" name="approved" value="Not applicable">Not applicable
				</div>
				';
	}

	if( 'clockin' == $_POST['form'] && 'late' == $_POST['offset'] ) {
		$form = '<div class="late failure">
					<label>Why are you clocking in late? *</label>
					<select class="reason" name="reason">
						<option value=""></option>
						<option value="Arrived late">Arrived late</option>
						<option value="Forgot to clock in">Forgot to clock in</option>
						<option value="Other">Other</option>
					</select>
					<div class="clock-other" style="display:none;">
						<label>Other reason: </label>
						<textarea name="other"></textarea>
					</div>
					<label>Additional notes:</label>
					<textarea name="note"></textarea>
					<label>Approved by manager</label>
					<input type="radio" name="approved" value="Approved by manager">Yes <br />
					<input type="radio" name="approved" value="Not approved by manager">No <br />
					<input type="radio" name="approved" value="Not applicable">Not applicable
				</div>';
	}

	if( 'clockout' == $_POST['form'] && 'early' == $_POST['offset'] ) {
		$form = '<div class="early failure">
					<label>Why are you clocking out early? *</label>
					<select class="reason" name="reason">
						<option value=""></option>
						<option value="Client picked up or early bus">Client picked up / early bus</option>
						<option value="School early dismissal">School early dismissal</option>
						<option value="Parent/teacher/guardian early dismissal">Parent/teacher/guardian early dismissal: explain in notes</option>
						<option value="Other">Other</option>
					</select>
					<div class="clock-other" style="display:none;">
						<label>Other reason: </label>
						<textarea name="other"></textarea>
					</div>
					<label>Additional notes:</label>
					<textarea name="note"></textarea>
					<label>Approved by manager</label>
					<input type="radio" name="approved" value="Approved by manager">Yes <br />
					<input type="radio" name="approved" value="Not approved by manager">No <br />
					<input type="radio" name="approved" value="Not applicable">Not applicable
				</div>';
	}

	if( 'clockout' == $_POST['form'] && 'late' == $_POST['offset'] ) {
		$form = '<div class="late failure">
					<label>Why are you clocking out late? *</label>
					<select class="reason" name="reason">
						<option value="">Select a reason...</option>
						<option value="Forgot, left on time">Forgot, left on time</option>
						<option value="Talked with teacher/parent/guardian">Talked with teacher/parent/guardian</option>
						<option value="Late client pick-up">Late client pick-up</option>
						<option value="Other">Other</option>
					</select>
					<div class="clock-other" style="display:none;">
						<label>Other reason: </label>
						<textarea name="other"></textarea>
					</div>
					<label>Additional notes:</label>
					<textarea name="note"></textarea>
					<label>Approved by manager</label>
					<input type="radio" name="approved" value="Yes">Yes <br />
					<input type="radio" name="approved" value="No">No <br />
					<input type="radio" name="approved" value="Not applicable">Not applicable
				</div>';
	}

	die( $form );

}

function wpaesm_save_employee_note( $post, $employee ) {
	if ( !wp_verify_nonce( $_POST['wpaesm_employee_note_nonce'], "wpaesm_employee_note_nonce")) {
        exit( "Permission error." );
    }

	wpaesm_create_shift_serialized_array( $post->ID );
	// enter the date so we know when the last note was left
	$now = time();
	update_post_meta( $post->ID, '_wpaesm_lastnote', $now );
	
	// Put note text in the array format wpalchemy expects
	$notes2 = get_post_meta($post->ID, '_wpaesm_employeenote', true);
	delete_post_meta( $post->ID, '_wpaesm_employeenote' );

	if(!isset($notes2) || !is_array($notes2)) {
		$notes2 = array();
	}
	$now = current_time( 'Y-m-d');

 	$tempnotes['notedate'] = $now;
    $tempnotes['notetext'] = sanitize_text_field($_POST['note']);
    array_push($notes2, $tempnotes);
	add_post_meta( $post->ID, '_wpaesm_employeenote', $notes2 );
	// if admin wants email notifications, send an email
	if(isset($options['admin_notify_note']) && $options['admin_notify_note'] == 1) {
		if(isset($options['admin_notification_email'])) {
			$to = $options['admin_notification_email'];
		} else {
			$to = get_bloginfo('admin_email');
		}
		$subject = $employee . " left a note on their shift on " . $meta['date'];
		$message = '<p>' . $employee . " left the following note on their shift that is scheduled for " . $meta['date'] . ':</p>';
		$message .= $_POST['note'];
		$from = $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";
		wpaesm_send_email( $from, $to, '', $subject, $message, '' );
	}

	unset($_POST);
}

function wpaesm_clock_in( $post ) {

	if ( !wp_verify_nonce( $_POST['wpaesm_clockin_nonce'], "wpaesm_clockin_nonce")) {
        exit( "Permission error." );
    }
	
	wpaesm_create_shift_serialized_array( $post->ID );
	// save clock in time
	$clockin = current_time("H:i");
	update_post_meta( $post->ID, '_wpaesm_clockin', $clockin );

	$testing_meta = get_post_meta( $post->ID, '_wpaesm_clockin', true );
	if( !isset( $testing_meta ) || '' == $testing_meta ) {
		wp_die( __( 'Something has gone wrong.  Please use the back button to try to clock in again.  If you continue to receive this error, contact the site administrator.', 'wpaesm' ) );
	}

	// save address
	if(isset($_POST['latitude']) && isset($_POST['longitude'])) {
		$lat = $_POST['latitude'];
		$long = $_POST['longitude'];

		$response = wp_remote_get( 'http://maps.google.com/maps/api/geocode/json?latlng=' . $lat . ',' . $long );

		if ( is_wp_error( $response ) ) {
			$address = __( 'Unable to retrieve location data', 'employee-scheduler' );
		} else {
			$body = wp_remote_retrieve_body( $response );
			$json = json_decode( $body );
			if ( isset( $json->status ) && 'OK' == $json->status ) {
				$address = $json->results[0]->formatted_address;
			} else {
				$address = __( 'Unable to retrieve location data', 'employee-scheduler' );
			}
		}

		update_post_meta( $post->ID, '_wpaesm_location_in', $address );

	}

	if( isset( $_POST['reason'] ) && '' !== $_POST['reason'] ) {
		wpaesm_save_required_note( $post );
	}

	unset($_POST);
}

function wpaesm_clock_out( $post ) {
	if ( !wp_verify_nonce( $_POST['wpaesm_clockout_nonce'], "wpaesm_clockout_nonce")) {
        exit( "Permission error." );
    }
	
	wpaesm_create_shift_serialized_array( $post->ID );
	// save clock out time
	$clockout = current_time("H:i");
	update_post_meta( $post->ID, '_wpaesm_clockout', $clockout );

	if( isset( $_POST['break'] ) && "1" == $_POST['break'] ) {
		update_post_meta( $post->ID, '_wpaesm_break', $_POST['break'] );
	}

	$testing_meta = get_post_meta( $post->ID, '_wpaesm_clockout', true );
	if( !isset( $testing_meta ) || '' == $testing_meta ) {
		wp_die( __( 'Something has gone wrong.  Please use the back button to try to clock out again.  If you continue to receive this error, contact the site administrator.', 'wpaesm' ) );
	}

	// save address
	if(isset($_POST['latitude']) && isset($_POST['longitude'])) {
		$lat = $_POST['latitude'];
		$long = $_POST['longitude'];
		$response = wp_remote_get( 'http://maps.google.com/maps/api/geocode/json?latlng=' . $lat . ',' . $long );

		if ( is_wp_error( $response ) ) {
			$address = __( 'Unable to retrieve location data', 'employee-scheduler' );
		} else {
			$body = wp_remote_retrieve_body( $response );
			$json = json_decode( $body );
			if ( isset( $json->status ) && 'OK' == $json->status ) {
				$address = $json->results[0]->formatted_address;
			} else {
				$address = __( 'Unable to retrieve location data', 'employee-scheduler' );
			}
		}

		update_post_meta( $post->ID, '_wpaesm_location_in', $address );

	}		
	$worked = wp_set_object_terms( $post->ID, 'worked', 'shift_status', false );

	if( isset( $_POST['reason'] ) && '' !== $_POST['reason'] ) {
		wpaesm_save_required_note( $post );
	}

	if( isset( $_POST['mileage'] ) && '' !== $_POST['mileage'] ) {
		wpaesm_create_shift_serialized_array( $post->ID );
		// enter the date so we know when the last note was left
		$now = time();
		update_post_meta( $post->ID, '_wpaesm_lastnote', $now );

		// Put note text in the array format wpalchemy expects
		$notes2 = get_post_meta($post->ID, '_wpaesm_employeenote', true);
		delete_post_meta( $post->ID, '_wpaesm_employeenote' );

		if(!isset($notes2) || !is_array($notes2)) {
			$notes2 = array();
		}
		$now = current_time( 'Y-m-d');

		$tempnotes['notedate'] = $now;
		$tempnotes['notetext'] = 'CIC Mileage: ' . sanitize_text_field( $_POST['mileage'] );
		if( isset( $_POST['where'] ) && '' !== $_POST['where'] ) {
			$tempnotes['notetext'] .= '
		| Where did you go?: ' . sanitize_text_field( $_POST['where'] );
		}
		array_push($notes2, $tempnotes);
		add_post_meta( $post->ID, '_wpaesm_employeenote', $notes2 );
	}

	unset($_POST);
}

function wpaesm_save_required_note( $post ) {
	wpaesm_create_shift_serialized_array( $post->ID );
	// enter the date so we know when the last note was left
	$now = time();
	update_post_meta( $post->ID, '_wpaesm_lastnote', $now );

	// Put note text in the array format wpalchemy expects
	$notes2 = get_post_meta($post->ID, '_wpaesm_employeenote', true);
	delete_post_meta( $post->ID, '_wpaesm_employeenote' );

	if(!isset($notes2) || !is_array($notes2)) {
		$notes2 = array();
	}
	$now = current_time( 'Y-m-d');

	$tempnotes['notedate'] = $now;
	$tempnotes['notetext'] = 'Reason for time discrepancy: ' . sanitize_text_field( $_POST['reason'] );
	if( isset( $_POST['other'] ) && ''!== $_POST['other'] ) {
		$tempnotes['notetext'] .= '
		| Other: ' . sanitize_text_field( $_POST['other'] );
	}
	if( isset( $_POST['note'] ) && ''!== $_POST['note'] ) {
		$tempnotes['notetext'] .= '
		| Additional note: ' . sanitize_text_field( $_POST['note'] );
	}
	if( isset( $_POST['approved'] ) && ''!== $_POST['approved'] ) {
		$tempnotes['notetext'] .= '
		| Approved by manager: ' . sanitize_text_field( $_POST['approved'] );
	}
	array_push($notes2, $tempnotes);
	add_post_meta( $post->ID, '_wpaesm_employeenote', $notes2 );
	// if admin wants email notifications, send an email
	if(isset($options['admin_notify_note']) && $options['admin_notify_note'] == 1) {
		$users = get_users( array(
			'connected_type' => 'shifts_to_employees',
			'connected_items' => $post->ID
		) );
		foreach( $users as $user ) {
			$employee = $user->display_name;
		}

		if(isset($options['admin_notification_email'])) {
			$to = $options['admin_notification_email'];
		} else {
			$to = get_bloginfo('admin_email');
		}
		$subject = $employee . " left a note on their shift";
		$message = '<p>' . $employee . 'left the following note on their shift:</p>';
		$message .= $tempnotes['notetext'];
		$from = $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";
		wpaesm_send_email( $from, $to, '', $subject, $message, '' );
	}
}

// ------------------------------------------------------------------------
// MASTER SCHEDULE SHORTCODE
// ------------------------------------------------------------------------

function wpaesm_master_schedule_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'begin' => '',
			'end' => '',
		), $atts )
	);

	// Enqueue script to make table sortable
	wp_enqueue_script( 'stupid-table', plugin_dir_url(__FILE__) . 'js/stupidtable.min.js', array( 'jquery' ) );

	// must be logged in as administrator or employee to view this
	if( is_user_logged_in() && ( wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator') ) ) {
		
		// see if we have shortcode attributes
		if( '' !== $begin && '' !== $end ) {
			$nav = 'off';
			$schedulebegin = $begin;
			$scheduleend = $end;
			$i = 0;
			$thisday = strtotime( $begin );
			$lastday = strtotime( $end );
			$week[date( "Y-m-d", $thisday )] = array();
			while( $thisday < $lastday ) {
				$thisday = strtotime( '+ 1 day', $thisday );
				$week[date( "Y-m-d", $thisday )] = array();
				$i++;
			}

		} else {
			$nav = 'on';
			// we don't have shortcode attributes, so we'll use default dates
			// get the appropriate date
			if( isset( $_GET['week'] ) && '' !== $_GET['week'] ) {
				$thisweek = $_GET['week'];
				$nextweek = strtotime("+1 week", $thisweek);
				$lastweek = strtotime("-1 week", $thisweek);
			} else {
				$thisweek = current_time("timestamp");
				$nextweek = strtotime("+1 week");
				$lastweek = strtotime("-1 week");
			}
			
			$options = get_option('wpaesm_options');

			// get the range of dates for this week

			// find out what day of the week today is
			$today = date("l", $thisweek);

			if($today == $options['week_starts_on']) { // today is first day of the week
				$weekstart = $thisweek;
			} else { // find the most recent first day of the week
				$sunday = 'last ' . $options['week_starts_on'];
				$weekstart = strtotime($sunday, $thisweek);
			}

			// from the first day of the week, add one day 7 times to get all the days of the week
			$i = 0;
			while($i < 7) {
				$week[date("Y-m-d", strtotime('+ ' . $i . 'days', $weekstart))] = array();
				if($i == 0) {
					$schedulebegin = date('F j, Y', strtotime('+ ' . $i . 'days', $weekstart));
				} elseif ($i == 6) {
					$scheduleend = date('F j, Y', strtotime('+ ' . $i . 'days', $weekstart));
				}
				$i++;
			}
		}

		$mschedule = "<h3>" . sprintf( __( 'Schedule for %s through %s', 'wpaesm' ), $schedulebegin, $scheduleend ) . "</h3>";
		if( 'on' == $nav ) {

			$mschedule .= wpaesm_get_schedule_nav();

		}

		// collect all the shifts
		foreach( $week as $day => $shifts ) {
			$args = array( 
				'post_type' => 'shift',
				'meta_query' => array(
					array(
						'key'     => '_wpaesm_date',
						'value'   => $day,
					),
				),
				// removed on 9/2 on Brenna's request
				// 'tax_query' => array(
				// 	array(
				// 		'taxonomy' => 'shift_type',
				// 		'field'    => 'slug',
				// 		'terms'    => array( 'extra', 'pto' ),
				// 		'operator' => 'NOT IN',
				// 	),
				// ),
				'posts_per_page' => -1,
				'meta_key' => '_wpaesm_starttime',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
			);
			
			$msquery = new WP_Query( $args );
			$i = 0;
			if ( $msquery->have_posts() ) :
				while ( $msquery->have_posts() ) : $msquery->the_post();
					global $shift_metabox;
					$meta = $shift_metabox->the_meta();
					$id = get_the_id();
					$week[$day][$i]['id'] = $id;
					$week[$day][$i]['permalink'] = get_the_permalink();
					$week[$day][$i]['starttime'] = $meta['starttime'];
					$week[$day][$i]['endtime'] = $meta['endtime'];
					$week[$day][$i]['date'] = $meta['date'];
					$statuses = get_the_terms($id, 'shift_status');
					if(is_array($statuses)) {
						foreach($statuses as $status) {
							$week[$day][$i]['status'] = $status->slug;
							$color = get_tax_meta($status->term_id, 'status_color');
							$week[$day][$i]['color'] = $color;
						}
					} 
					$users = get_users( array(
						'connected_type' => 'shifts_to_employees',
						'connected_items' => $id,
						'orderby'      => 'display_name',
					) );
					if( empty( $users ) ) {
						$week[$day][$i]['employee'] = __( 'Unassigned', 'wpaesm' );
					} else {
						foreach($users as $user) {
							$week[$day][$i]['employee'] = $user->id;
						}
					}
					$clients = get_posts( array(
					  'connected_type' => 'shifts_to_clients',
					  'connected_items' => $id,
					  'nopaging' => true,
					  'suppress_filters' => false
					) );
					if( empty( $clients ) ) {
						$week[$day][$i]['client'] = 'No client';
						$week[$day][$i]['clientlink'] = '#';
					} else {
						foreach($clients as $client) {
							$yourclients[] = $client->ID; // SCC only
							$week[$day][$i]['client'] = $client->post_title;
							$week[$day][$i]['clientlink'] = site_url() . "/client/" . $client->post_name;
						}
					}
					$i++;
				endwhile;
			endif;
			wp_reset_postdata();

		}

		// go through the shifts and collect all the employees
		$employeearray = array();
		foreach( $week as $day => $shifts ) {
			foreach( $shifts as $shift ) {
				if( isset( $shift['employee'] ) ) {
					$employeearray[] = $shift['employee'];
				}
			}
		}

		// take out all the duplicate employees
		$employeearray = array_unique( $employeearray );

		// display table
		if( 'off' == $nav ) {
			$class = 'class="wp-list-table widefat fixed posts striped"';
		} else {
			$class = '';
		}
		$mschedule .= "<table id='master-schedule'" . $class . "><thead class='sticky'><tr>";
		$mschedule .= "<th data-sort='string'><span>Employee</span></th>";
		foreach( $week as $day => $shifts ) {
			$mschedule .= "<th data-sort='string'><span>" . date( "D M j", strtotime( $day ) ) . "</span></th>";
		}
		$mschedule .= '<th data-sort="string">Total Hours</th>';
		$mschedule .= "</tr></thead><tbody>";
		foreach( $employeearray as $employee ) {
			if( 'Unassigned' == $employee ) {
				$employee_cell = $employee;
			} else {
				$employeeinfo = get_user_by( 'id', $employee );
				$employee_cell = $employeeinfo->display_name;
				if( isset( $employeeinfo->user_email ) ) {
					$employee_cell .= "<br /><a href='mailto:" . $employeeinfo->user_email . "'>" . $employeeinfo->user_email . "</a>";
				}
				$phone = get_user_meta( $employee, 'phone', true );
				if( isset( $phone ) ) {
					$employee_cell .= "<br /><a href='tel:" . $phone . "'>" . $phone . "</a>";
				}
			}
			$mschedule .= "<tr><th scope='row'>" . $employee_cell . "</th>";
			$sched_duration = 0;
			$worked_duration = 0;
			foreach( $week as $day => $shifts ) {
				$mschedule .= "<td>";
				$shift_text='';
				foreach( $shifts as $shift ) {
					if( isset( $shift['employee'] ) && $employee == $shift['employee'] ) {
						$shift_text .= "<div";
						if( isset( $shift['status'] ) ) {
							$shift_text .= " class='" . $shift['status'] . "'";
						}
						if( isset( $shift['color'] ) ) {
							$shift_text .= " style='background: " . $shift['color'] . "'";
						}
						$shift_text .= ">";
						if( '' !== get_the_title( $shift['id'] ) ) {
							$shift_text .= get_the_title( $shift['id'] );
						} elseif( isset( $shift['client'] ) ) {
							$shift_text .= "<span class='client'><a href='" . $shift['clientlink'] . "'>" . $shift['client'] . "</a></span>";
						} 
						// on 8/5/15, I removed this and replaced it with above at SCC's request
						// if( isset( $shift['client'] ) ) {
						// 	$shift_text .= "<span class='client'><a href='" . $shift['clientlink'] . "'>" . $shift['client'] . "</a></span>"; 
						// } else {
						// 	$shift_text .= get_the_title( $shift['id'] );
						// }
						$shift_text .= "<br /><span class='time'>" . date("g:i a", strtotime($shift['starttime'])) . " - " . date("g:i a", strtotime($shift['endtime'])) . "</span>";
						$shift_text .= "<br /><a class='details' href='" . $shift['permalink'] . "'>" . __( 'View Shift Details', 'wpaesm' ) . "</a>";
						$shift_text .= "</div>";
						// get shift scheduled time
						$to_time = strtotime( $shift['starttime'] );
						$from_time = strtotime( $shift['endtime'] );
						$minutes = round(abs($to_time - $from_time) / 60,2);
						$quarters = round($minutes/15) * 15;
						$workedmins[] = $quarters;
						$sched_duration += $quarters/60;

						// get shift worked time, if it exists
						$to_time = strtotime( get_post_meta( $shift['id'], '_wpaesm_clockin', true ) );
						$clockout = get_post_meta( $shift['id'], '_wpaesm_clockout', true );
						if( isset( $clockout ) && '' !== $clockout ) {
							$from_time = strtotime( get_post_meta( $shift['id'], '_wpaesm_clockout', true ) );
							$minutes      = round( abs( $to_time - $from_time ) / 60, 2 );
							$quarters     = round( $minutes / 15 ) * 15;
							$workedmins[] = $quarters;
							$worked_duration += $quarters / 60;
						}
					}
				}
				if( empty($shift_text) ){
					$mschedule .= "<span class='noshift'>" . __( 'No shifts', 'wpaesm' ) . "</span>";
				}
				$mschedule .= $shift_text;
				$mschedule .= "</td>";
			}
			$mschedule .= '<td>Hours Scheduled: ' . $sched_duration;
			if( 0 !== $worked_duration ) {
				$mschedule .= '<br />Hours Worked: ' . $worked_duration;
			}
			$mschedule .= '</td>';
			$mschedule .= "</tr>";
		}

		$mschedule .= "</tbody></table>";
		if( 'on' == $nav ) {
			$mschedule .= wpaesm_get_schedule_nav();
		}
		
	} else {
		$mschedule = "<p>" . __( 'You must be logged in to view this page.', 'wpaesm' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$mschedule .= wp_login_form($args);
	}
	return $mschedule;
}
add_shortcode( 'master_schedule', 'wpaesm_master_schedule_shortcode' );

function wpaesm_get_schedule_nav() {

	$options = get_option('wpaesm_options');

	$thisweek = current_time("timestamp");

	// find out what day of the week today is
	$today = date("l", $thisweek);

	if($today == $options['week_starts_on']) { // today is first day of the week
		$weekstart = $thisweek;
	} else { // find the most recent first day of the week
		$sunday = 'last ' . $options['week_starts_on'];
		$weekstart = strtotime($sunday, $thisweek);
	}

	$weeks = array();

	for( $i = 1; $i <= 8; $i++ ) {
		$nextweek = $weekstart - ( 604800 * $i );
		$key = date( 'Y-m-d', $nextweek );
		$weeks[$key] = $nextweek;
	}

	asort( $weeks );

	$weeks['This Week'] = '';

	for( $i = 1; $i <= 8; $i++ ) {
		$nextweek = $weekstart + ( 604800 * $i );
		$key = date( 'Y-m-d', $nextweek );
		$weeks[$key] = $nextweek;
	}


	$nav = "<nav class='schedule'><form method='get' action='" . get_the_permalink() . "'>
				<select name='week' id='week'>";

					if( isset( $_GET['week'] ) ) {
						$current = $_GET['week'];
					} else {
						$current = '';
					}

					foreach( $weeks as $key => $value ) {
						$nav .= '<option value="' . $value . '" ' . selected( $value, $current, false ) .'>' . $key . '</option>';
					}

				$nav .= "</select>
				<input type='submit' value='go to week'>
			</form></nav>";

	return $nav;

}



// ------------------------------------------------------------------------
// YOUR SCHEDULE SHORTCODE
// ------------------------------------------------------------------------

function wpaesm_your_schedule_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'employee' => '',
			'begin' => '',
			'end' => '',
		), $atts )
	);

	// must be logged in to view this
	if( is_user_logged_in() && ( wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator') ) ) {
		
		// see if we have shortcode attributes
		if( '' !== $employee ) {
			$nav = 'off';
			$schedulebegin = $begin;
			$scheduleend = $end;
			$thisday = strtotime( $begin );
			$lastday = strtotime( $end );
			$week[date( "Y-m-d", $thisday )] = array();
			while( $thisday < $lastday ) {
				$thisday = strtotime( '+ 1 day', $thisday );
				$week[date( "Y-m-d", $thisday )] = array();
				$i++;
			}

		} else {
			$nav = 'on';
			$employee = get_current_user_id();

			// get the appropriate date
			if(isset($_GET['week'])) {
				$thisweek = $_GET['week'];
				$nextweek = strtotime("+1 week", $thisweek);
				$lastweek = strtotime("-1 week", $thisweek);
			} else {
				$thisweek = current_time("timestamp");
				$nextweek = strtotime("+1 week");
				$lastweek = strtotime("-1 week");
			}
			
			$options = get_option('wpaesm_options');

			// get the range of dates for this week

			// find out what day of the week today is
			$today = date("l", $thisweek);

			if($today == $options['week_starts_on']) { // today is first day of the week
				$weekstart = $thisweek;
			} else { // find the most recent first day of the week
				$sunday = 'last ' . $options['week_starts_on'];
				$weekstart = strtotime($sunday, $thisweek);
			}

			// from the first day of the week, add one day 7 times to get all the days of the week
			$i = 0;
			while($i < 7) {
				$week[date("Y-m-d", strtotime('+ ' . $i . 'days', $weekstart))] = array();
				if($i == 0) {
					$schedulebegin = date('F j, Y', strtotime('+ ' . $i . 'days', $weekstart));
				} elseif ($i == 6) {
					$scheduleend = date('F j, Y', strtotime('+ ' . $i . 'days', $weekstart));
				}
				$i++;
			}
		}

		$mschedule = "<h3>" . sprintf( __( 'Schedule for %s through %s', 'wpaesm' ), $schedulebegin, $scheduleend ) . "</h3>";
		if( 'on' == $nav ) {
			$mschedule .= wpaesm_get_schedule_nav();
		}

		// collect all the shifts
		foreach($week as $day => $shifts) {
			$args = array( 
				'post_type' => 'shift',
				'meta_query' => array(
					array(
						'key'     => '_wpaesm_date',
						'value'   => $day,
					),
				),
				'connected_type' => 'shifts_to_employees',
				'connected_items' => $employee,
				'posts_per_page' => -1,
				'meta_key' => '_wpaesm_starttime',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
			);
			
			$msquery = new WP_Query( $args );
			$i = 0;
			global $yourclients; // SCC only
			$yourclients = array(); // SCC only
			if ( $msquery->have_posts() ) :
				while ( $msquery->have_posts() ) : $msquery->the_post();
					global $shift_metabox;
					$meta = $shift_metabox->the_meta();
					$id = get_the_id();
					$week[$day][$i]['id'] = $id;
					$week[$day][$i]['permalink'] = get_the_permalink();
					$week[$day][$i]['starttime'] = $meta['starttime'];
					$week[$day][$i]['endtime'] = $meta['endtime'];
					$week[$day][$i]['date'] = $meta['date'];
					$clients = get_posts( array(
					  'connected_type' => 'shifts_to_clients',
					  'connected_items' => $id,
					  'nopaging' => true,
					  'suppress_filters' => false
					) );
					if( empty( $clients ) ) {
						$week[$day][$i]['client'] = 'No client';
						$week[$day][$i]['clientlink'] = '#';
					} else {
						foreach($clients as $client) {
							$yourclients[] = $client->ID; // SCC only
							$week[$day][$i]['client'] = $client->post_title;
							$week[$day][$i]['clientlink'] = site_url() . "/client/" . $client->post_name;
						}
					}
					$i++;
				endwhile;
			endif;
			wp_reset_postdata();

		}

		// collect all the clients
		global $clientarray;
		$clientarray = array();
		foreach($week as $day => $shifts) {
			foreach($shifts as $shift) {
				if(isset($shift['client'])) {
					$clientarray[] = $shift['client'];
				}
			}
		}
		// take out all the duplicates
		$clientarray = array_unique($clientarray);

		// display table
		if( 'off' == $nav ) {
			$class = 'class="wp-list-table widefat fixed posts striped"';
		} else {
			$class = '';
		}
		$mschedule .= "<table id='master-schedule'" . $class . "><thead><tr>";
		$mschedule .= "<th>" . __( 'Client', 'wpaesm' ) . "</th>";
		foreach($week as $day => $shifts) {
			$mschedule .= "<th>" . date("D M j", strtotime($day)) . "</th>";
		}
		$mschedule .= "</tr>";
		foreach($clientarray as $client) {
			$mschedule .= "<tr><th scope='row'>" . $client . "</th>";
			foreach($week as $day => $shifts) {
				$mschedule .= "<td>";
				$shift_text='';
				foreach($shifts as $shift) {
					if(isset($shift['client']) && $client == $shift['client']) {
						if( '' !== get_the_title( $shift['id'] ) ) {
							$shift_text .= get_the_title( $shift['id'] ) . '<br />';
						} elseif( isset( $shift['client'] ) ) {
							$shift_text .= "<span class='client'><a href='" . $shift['clientlink'] . "'>" . $shift['client'] . "</a></span><br />";
						} 
						$shift_text .= "<span class='employee'>" . date("g:i a", strtotime($shift['starttime'])) . " - " . date("g:i a", strtotime($shift['endtime'])) . "</span>";
						$shift_text .= "<a class='details' href='" . $shift['permalink'] . "'>" . __( 'View Shift Details', 'wpaesm' ) . "</a>";
					}
				}
				if( empty($shift_text) ){
					$mschedule .= __( 'No shifts', 'wpaesm' );
				}
				$mschedule .= $shift_text;
				$mschedule .= "</td>";
			}
			$mschedule .= "</tr>";
		}

		$mschedule .= "</table>";
		if( 'on' == $nav ) {
			$mschedule .= wpaesm_get_schedule_nav();
		}
		
	} else {
		$mschedule = "<p>" . __( 'You must be logged in to view this page.', 'wpaesm' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$mschedule .= wp_login_form($args);
	}
	return $mschedule;
}
add_shortcode( 'your_schedule', 'wpaesm_your_schedule_shortcode' );


// ------------------------------------------------------------------------
// EMPLOYEE PROFILE SHORTCODE
// ------------------------------------------------------------------------

function wpaesm_employee_profile_shortcode() {
	// must be logged in to view this
	if( is_user_logged_in() && ( wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator') ) ) {
		$profile = '';
		
		// thanks to http://wordpress.stackexchange.com/questions/9775/how-to-edit-a-user-profile-on-the-front-end
		global $current_user, $wp_roles;
		get_currentuserinfo();

		/* Load the registration file. */
		require_once( ABSPATH . WPINC . '/registration.php' );
		$error = array();    
		/* If profile was saved, update profile. */
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

		    /* Update user password. */
		    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
		        if ( $_POST['pass1'] == $_POST['pass2'] )
		            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
		        else
		            $error[] = __( 'The passwords you entered do not match.  Your password was not updated.', 'profile' );
		    }

		    /* Update user information. */
		    if ( !empty( $_POST['url'] ) )
		        update_user_meta( $current_user->ID, 'user_url', esc_url( $_POST['url'] ) );
		    if ( !empty( $_POST['email'] ) ){
		        if (!is_email(esc_attr( $_POST['email'] )))
		            $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
		        elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
		            $error[] = __('This email is already used by another user.  try a different one.', 'profile');
		        else{
		            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
		        }
		    }

		    if ( !empty( $_POST['first-name'] ) )
		        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
		    if ( !empty( $_POST['last-name'] ) )
		        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
		    if ( !empty( $_POST['description'] ) )
		        update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );

		    /* Redirect so the page will show updated info.*/
		  /*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
		    if ( count($error) == 0 ) {
		        //action hook for plugins and extra fields saving
		        do_action('edit_user_profile_update', $current_user->ID);
		        wp_redirect( get_permalink() );
		        exit;
		    }
		}

		// if ( count($error) > 0 ) {
			$profile .= "<p class='error'>" . implode('<br />', $error) . "</p>"; 
			$profile .= "<form method='post' id='adduser' action='" . the_permalink(); "'>";
	        $profile .= "<p class='form-username'>";
	        $profile .= "<label for='first-name'>" . _e('First Name', 'wpaesm') . "</label>";
	        $profile .= "<input class='text-input' name='first-name' type='text' id='first-name' value='" . the_author_meta( 'first_name', $current_user->ID ) . "'>";
	        $profile .= "</p><!-- .form-username -->";
	        $profile .= "<p class='form-username'>";
	        $profile .= "<label for='last-name'>" . _e('Last Name', 'wpaesm') . "</label>";
	        $profile .= "<input class='text-input' name='last-name' type='text' id='last-name' value='" . the_author_meta( 'last_name', $current_user->ID ) . "'>";
	        $profile .= "</p><!-- .form-username -->";
	        $profile .= "<p class='form-email'>";
	        $profile .= "<label for='email'>" . _e('E-mail *', 'wpaesm') . "</label>";
	        $profile .= "<input class='text-input' name='email' type='text' id='email' value='" . the_author_meta( 'user_email', $current_user->ID ) . "'>";
	        $profile .= "</p><!-- .form-email -->";
	        $profile .= "<p class='form-password'>";
	        $profile .= "<label for='pass1'>" . _e('Password *', 'wpaesm') . "</label>";
	        $profile .= "<input class='text-input' name='pass1' type='password' id='pass1' />";
	        $profile .= "</p><!-- .form-password -->";
	        $profile .= "<p class='form-password'>";
	        $profile .= "<label for='pass2'>" . _e('Repeat Password *', 'wpaesm') . "</label>";
	        $profile .= "<input class='text-input' name='pass2' type='password' id='pass2' />";
	        $profile .= "</p><!-- .form-password -->";
	        $profile .= "<p class='form-textarea'>";
	    // }

        //action hook for plugin and extra fields
        do_action('edit_user_profile',$current_user); 
        $profile .= "<p class='form-submit'>";
        // $profile .= $referer;
        $profile .= "<input name='updateuser' type='submit' id='updateuser' class='submit button' value='" . _e('Update', 'wpaesm') . "' />";
        $profile .=  wp_nonce_field( 'update-user' );
        $profile .= "<input name='action' type='hidden' id='action' value='update-user' />";
        $profile .= "</p><!-- .form-submit -->";
        $profile .= "</form><!-- #adduser -->";
        
        // endif; 

	} else {
		$profile = "<p>" . __( 'You must be logged in to view this page.', 'wpaesm' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$profile .= wp_login_form($args);
	}

	return $profile;
}
add_shortcode( 'employee_profile', 'wpaesm_employee_profile_shortcode' );


// ------------------------------------------------------------------------
// TODAY SHORTCODE
// ------------------------------------------------------------------------

function wpaesm_today_shortcode() {
	// must be logged in to view this
	if( is_user_logged_in() && ( wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator') ) ) {
		$now = date("Y-m-d", current_time("timestamp"));
		$viewer = wp_get_current_user();
		$today = '';
		
		$args = array( 
		    'post_type' => 'shift',
		    'posts_per_page' => -1,
		    'order' => 'ASC', 
		    'meta_key' => '_wpaesm_starttime',        
		    'orderby' => 'meta-value', 
		    'meta_query' => array(
				array(
					'key'     => '_wpaesm_date',
					'value'   => $now,
				),
			),
		    'connected_type' => 'shifts_to_employees',
		    'connected_items' => $viewer->ID,

		);
		
		$todayquery = new WP_Query( $args );
		
		// The Loop
		if ( $todayquery->have_posts() ) {
			$clients = get_posts( array(  // TO DO - this doesn't find clients  Why?
				'connected_type' => 'shifts_to_clients',
				'connected_items' => get_the_id(),
				'nopaging' => true,
				'suppress_filters' => false
			) );
			foreach($clients as $client) {
				$clientname = $client->post_title;
			}
			$today .= "<p>You have " . $todayquery->found_posts . " shift(s) scheduled today.  Which would you like to view?</p>";
			$today .= "<ul>";
				while ( $todayquery->have_posts() ) : $todayquery->the_post();
					global $shift_metabox;
					$meta = $shift_metabox->the_meta();
					$today .= "<li><a href='" . get_the_permalink() . "'>" . date("g:i", strtotime($meta['starttime'])) . " - " . date("g:i", strtotime($meta['endtime']));
					if(isset($clientname)) {
						$today .=  " with " . $clientname . "</a></li>";
					}
					$today .= "</a></li>";
				endwhile;
			$today .= "</ul>";
		} else {
			$today .= "<p>You do not have any shifts scheduled today.</p>";
		}
		
		// Reset Post Data
		wp_reset_postdata();
		
	} else {
		$today = "<p>" . __( 'You must be logged in to view this page.', 'wpaesm' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$today .= wp_login_form($args);
	}

	return $today;

}
add_shortcode( 'today', 'wpaesm_today_shortcode' );

// ------------------------------------------------------------------------
// EXTRA-SHIFT WORK SHORTCODE
// ------------------------------------------------------------------------

function wpaesm_extra_work_shortcode() {

	// enqueue scripts to make date and time pickers work
    wp_enqueue_style( 'wpalchemy-metabox', plugins_url() . '/employee-schedule-manager/css/meta.css' );
    wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
    wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );

	// must be logged in to view this
	if( is_user_logged_in() && ( wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator') ) ) {
		$morework = '';
		// Process the form if we need to
	    // get current user's name
	    $viewer = wp_get_current_user();
	    $viewername = $viewer->display_name;
	    if( isset( $_POST['form_name'] ) && "extra_work" == ( $_POST['form_name'] ) ) {
	    	$morework = wpaesm_add_extra_work_shift( $viewer );
	    }

	    // display the form
		$morework .= "<p>" . __('Use this form to record the time you spend writing documentation.', 'wpaesm'). "</p>";
		$morework .= "<form method='post' action='" . get_the_permalink() . "?tab=extra-shift' id='extra-work'>";
		$morework .= "<p><label>" . __('Date', 'wpaesm') . "</label>";
		$morework .= "<input type='text' name='thisdate' id='thisdate' class='required' required></p>";
		$morework .= "<p><label>" . __('Start Time', 'wpaesm') . "</label>";
		$morework .= "<input type='text' name='starttime' id='starttime' class='required' required></p>";
		$morework .= "<p><label>" . __('End Time', 'wpaesm') . "</label>";
		$morework .= "<input type='text' name='endtime' id='endtime' class='required' required></p>";
		// get the extra term
//		$extratype = get_term_by( 'slug', 'extra', 'shift_type' );
//		// if extra has children, show dropdown of children
//		$extra_children = get_term_children( $extratype->term_id, 'shift_type' );
//		if( !empty( $extra_children ) ) {
//			$morework .= "<p><label>" . __('Type of Work', 'wpaesm') . "</label>";
//			$morework .= "<select name='shifttype' id='extra-work-shift-type'><option value=''> </option>";
//			foreach( $extra_children as $child ) {
//				$childterm = get_term_by( 'id', $child, 'shift_type' );
//				$morework .= "<option value='" . $childterm->slug . "'>" . $childterm->name . "</option>";
//			}
//			$morework .= "</select>";
//		}
		$morework .= '<input type="hidden" name="shifttype" id="extra-work-shift-type" value="documentation">';
		$morework .= "<p id='wpaesm-client'><label>" . __('Client', 'wpaesm') . "</label>";
		$morework .= "<select name='thisclient' id='thisclient'><option value=''> </option>";
		$args = array('post_type' => 'client', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title' );
		$clients = get_posts( $args );
		foreach ($clients as $client) {
			$morework .= "<option value='" . $client->ID . "'>" . get_the_title($client->ID) . "</option>";
		}
		$morework .= "</select>";
		$morework .= "<p><label>" . __('Description', 'wpaesm') . "</label>";
		$morework .= "<textarea name='description' id='description'></textarea></p>";
		$morework .= "<input type='hidden' name='form_name' value='extra_work'>";
		$morework .= "<input name='wpaesm_extra_work_nonce' id='wpaesm_extra_work_nonce' type='hidden' value='" . wp_create_nonce( 'wpaesm_extra_work_nonce' ) . "'>";
		$morework .= "<input type='submit' value='" . __('Record work', 'wpaesm') . "'>";
		$morework .= "</form>";

	} else {
		$morework = "<p>" . __( 'You must be logged in to view this page.', 'wpaesm' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$morework .= wp_login_form($args);
	}

	return $morework;
}
add_shortcode( 'extra_work', 'wpaesm_extra_work_shortcode' );


// ------------------------------------------------------------------------
// EXTRA-SHIFT WORK FUNCTIONS
// ------------------------------------------------------------------------

function wpaesm_add_extra_work_shift( $viewer ) {
	if ( !wp_verify_nonce( $_POST['wpaesm_extra_work_nonce'], "wpaesm_extra_work_nonce")) {
        exit( "Permission error." );
    }

    $viewername = $viewer->display_name;
	$extrawork = array(
			'post_type'     => 'shift',
			'post_title'    => 'Extra shift by ' . $viewername,
			'post_status'   => 'publish',
			'post_content'	=> sanitize_text_field( $_POST['description'] ),
		);
	$extrashift = wp_insert_post( $extrawork );

	wp_set_object_terms( $extrashift, 'extra', 'shift_type' );
	// also add subcategory, if they selected one from the drop-down
	if( isset( $_POST['shifttype'] ) ) {
		wp_set_object_terms( $extrashift, $_POST['shifttype'], 'shift_type' );
	}
	
	$options = get_option( 'wpaesm_options' );
	if( '1' == $options['extra_shift_approval'] ) {
		// mark the shift as pending approval
		wp_set_object_terms( $extrashift, 'pending-approval', 'shift_status' );
		// email notification to admin
		$from = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
		$to = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
		$subject = sprintf( __( 'Extra shift by %s is pending your approval', 'wpaesm' ), $viewername );
		$message = '
			<p>' . __( 'There is a new extra shift awaiting your approval', 'wpaesm' ) . '</p>
			<p><strong>' . __( 'Shift details' ) . '</strong>
				<ul>
					<li><strong>' . __( 'Employee:', 'wpaesm' ) . '</strong> ' . $viewername . '</li>
					<li><strong>' . __( 'Date:', 'wpaesm' ) . '</strong> ' . $_POST['thisdate'] . '</li>
					<li><strong>' . __( 'Time:', 'wpaesm' ) . '</strong> ' . $_POST['starttime'] . '&nbsp;-&nbsp' . $_POST['endtime'] . '</li>
					<li><strong>' . __( 'Duration:', 'wpaesm' ) . '</strong> ' . wpaesm_calculate_duration( $_POST['starttime'], $_POST['endtime'] ) . '</li>';
					if( isset( $_POST['description'] ) && '' !== $_POST['description'] ) {
						$message .= '
						<li><strong>' . __( 'Description:', 'wpaesm' ) . '</strong> ' . sanitize_text_field( $_POST['description'] ) . '</li>
						';
					}
				$message .=
				'</ul>
			</p>
			<p><a href="' . get_the_permalink( $extrashift ) . '">' . __( 'View this shift', 'wpaesm' ) . '</a></p>
			<p><a href="' . get_edit_post_link( $extrashift ) . '">' . __( 'Edit this shift', 'wpaesm' ) . '</a></p>
			<p>' . __( 'To approve this shift, edit it and change the shift status to "worked."  If you do not approve this shift, edit it and change the shift status to "not approved."') . '</p>
			<p><a href="' . admin_url( 'edit.php?shift_status=pending-approval&post_type=shift' ) . '">' . __( 'View all extra shifts awaiting approval', 'wpaesm' ) . '</a></p>';
		// SCC only - do not send notificaton email
		//wpaesm_send_email( $from, $to, '', $subject, $message, '' );
	} else {
		// we don't need admin approval, so mark the shift as worked
		wp_set_object_terms( $extrashift, 'worked', 'shift_status' );
	}
	

	// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
	wpaesm_create_shift_serialized_array( $extrashift );

	add_post_meta( $extrashift, '_wpaesm_date', $_POST['thisdate'] );
	add_post_meta( $extrashift, '_wpaesm_clockin', $_POST['starttime'] );
	add_post_meta( $extrashift, '_wpaesm_clockout', $_POST['endtime'] );
	add_post_meta( $extrashift, '_wpaesm_starttime', $_POST['starttime'] );
	add_post_meta( $extrashift, '_wpaesm_endtime', $_POST['endtime'] );
	// connect shift to employee
	p2p_type( 'shifts_to_employees' )->connect( $extrashift, $viewer->ID, array(
	    'date' => current_time('mysql')
	) );
	// connect shift to client
	if(isset($_POST['thisclient']) && $_POST['thisclient'] !== ' ') {
		p2p_type( 'shifts_to_clients' )->connect( $extrashift, $_POST['thisclient'], array(
		    'date' => current_time('mysql')
		) );
	}

	if($extrashift) {
		$message = "<p class='success'>" . __('Your extra work has been recorded.  ', 'wpaesm') . "<a href='" . get_the_permalink( $extrashift ) . "'>" . __('View extra work shift', 'wpaesm') . "</a></p>";
	} else {
		$message = "<p class='failure'>" . __('Sorry, there was an error recording your work.', 'wpaesm') . "</p>";
	}
	return $message;
}

// ------------------------------------------------------------------------
// EXPENSE REPORT SHORTCODE
// ------------------------------------------------------------------------

function wpaesm_record_expense_shortcode() {

	// enqueue scripts to make date and time pickers work
    wp_enqueue_style( 'wpalchemy-metabox', plugins_url() . '/employee-schedule-manager/css/meta.css' );
    wp_enqueue_script( 'validate', plugins_url() . '/employee-schedule-manager/js/jquery.validate.min.js', 'jquery' );
    wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
    wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );

	// must be logged in to view this
	if( is_user_logged_in() && ( wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator') ) ) {
		$expense = '';
		// Process the form if we need to
	    // get current user's name
	    $viewer = wp_get_current_user();
	    $viewername = $viewer->display_name;
	    if( isset( $_POST['form_name'] ) && "expense" == ( $_POST['form_name'] ) ) {
	    	$expense .= wpaesm_add_expense( $viewer );
	    }

	    // display the form
		$expense .= "<p>" . __('Use this form to record your expenses.', 'wpaesm'). "</p>";
		$expense .= "<form method='post' action='" . get_the_permalink() . "?tab=expense' id='expense'>";
		$expense .= "<input type='hidden' name='form_name' value='expense'>";
		$expense .= "<p><label>" . __('Date', 'wpaesm') . "</label>";
		$expense .= "<input type='text' name='thisdate' id='thisdate2' class='required' required></p>";
		// $expense .= "<p><label>" . __('Expense Type', 'wpaesm') . "</label>";
		// $expense .= "<select name='type' id='create_expense_type' class='required' required><option value=''> </option>";
		// $expense .= wpaesm_expense_category_dropdown();
		// $expense .= "</select>";
		$expense .= "<p><label>" . __('Amount (dollars)', 'wpaesm') . "</label>";
		$expense .= "<input type='text' name='amount' id='amount' class='required' required></p>";
		$expense .= "<p id='clientfield'><label>" . __('Client', 'wpaesm') . "</label>";
		$expense .= "<select name='thisclient' id='thisclient'><option value=''> </option>";
		$args = array('post_type' => 'client', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title' );
		$clients = get_posts( $args );
		foreach ($clients as $client) {
			$expense .= "<option value='" . $client->ID . "'>" . get_the_title($client->ID) . "</option>";
		}
		$expense .= "</select>";
		$expense .= "<p><label>" . __('Description', 'wpaesm') . "</label>";
		$expense .= "<textarea name='description' id='description'></textarea></p>";
		$expense .= "<input type='submit' value='" . __('Record Expense', 'wpaesm') . "'>";
		$expense .= "</form>";

	} else {
		$expense = "<p>" . __( 'You must be logged in to view this page.', 'wpaesm' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$expense .= wp_login_form($args);
	}

	return $expense;
}
add_shortcode( 'record_expense', 'wpaesm_record_expense_shortcode' );

// functions to make the expense type dropdown display hierarchically
// not using this any more - see support ticket on 10/7
function wpaesm_expense_category_dropdown() {
	$dropdown = '';

	$disabled_array = array( // SCC ONLY
		'mileage'
		);

    // Get all taxonomy terms 
    $terms = get_terms('expense_category', array(
            "hide_empty" => false,
            "parent" => 0, 
            "exclude" => array( '33', '28' ),
        )
    );

    if( isset( $terms ) ) {
    	foreach( $terms as $term ) {
    		$dropdown .= '<option value="' . $term->slug . '"';
    		if( in_array( $term->slug, $disabled_array ) ) {  // SCC ONLY
	    		$dropdown .= ' disabled ';
	    	}
    		$dropdown .= '>' . $term->name . '</option>';
    		$dropdown .= wpaesm_get_term_children( $term->term_id, 1 );
    	}
    }

   	return $dropdown;
}

function wpaesm_get_term_children( $termid, $depth ) {
	$disabled_array = array( // SCC ONLY
		'client',
		'second-shift',
		);
	$children = '';
	$childterms = get_terms('expense_category', array(
            "hide_empty" => false,
            "parent" => $termid,
            "exclude" => array( '33', '28' ),
        )
    );

	if( isset( $childterms ) ) {
		$depth++;
	    foreach( $childterms as $childterm ) {
	    	

	    	$children .= '<option value="' . $childterm->slug . '"';
	    	if( in_array( $childterm->slug, $disabled_array ) ) {  // SCC ONLY
	    		$children .= ' disabled ';
	    	}
	    	$children .= '> ';
	    	for ($i=0; $i < $depth; $i++) { 
	    		$children .= '--';
	    	}
	    	$children .= ' ' . $childterm->name . '</option>';
	    	$children .= wpaesm_get_term_children( $childterm->term_id, $depth );
	    }
	}

	return $children;
}


function wpaesm_add_expense( $viewer ) {
	$viewername = $viewer->display_name;
	$thisexpense = array(
			'post_type'     => 'expense',
			'post_title'    => 'Expense reported by ' . $viewername,
			'post_status'   => 'publish',
			'post_content'	=> sanitize_text_field( $_POST['description'] ),
		);
	$newexpense = wp_insert_post($thisexpense);

	// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
	$data = array('_wpaesm_date','_wpaesm_amount','_wpaesm_mileage');
	$str = $data;
	update_post_meta( $newexpense, 'expense_meta_fields', $str );

	add_post_meta( $newexpense, '_wpaesm_date', $_POST['thisdate'] );
	
	wp_set_object_terms( $newexpense, 'receipt', 'expense_category' );

	// connect shift to employee
	p2p_type( 'expenses_to_employees' )->connect( $newexpense, $viewer->ID, array(
	    'date' => current_time('mysql')
	) );
	// connect shift to client
	if(isset($_POST['thisclient']) && $_POST['thisclient'] !== ' ') {
		p2p_type( 'expenses_to_clients' )->connect( $newexpense, $_POST['thisclient'], array(
		    'date' => current_time('mysql')
		) );
	}

	if($newexpense) {
		$message = "<p class='success'>" . __('Your expense has been recorded.', 'wpaesm') . "</p>";
	} else {
		$message = "<p class='failure'>" . __('Sorry, there was an error recording your expense.', 'wpaesm') . "</p>";
	}

	return $message;
}


// ------------------------------------------------------------------------
// YOUR UNAVAILABILITY SHORTCODE
// ------------------------------------------------------------------------
function wpaesm_your_unavailability_shortcode() {
	$unavailability = '';

	if( is_user_logged_in() && ( wpaesm_check_user_role( 'employee' ) || wpaesm_check_user_role( 'administrator' ) ) ) {
		$current_user = wp_get_current_user();
		$unavailable = get_user_meta( $current_user->ID, 'unavailable', false );
		if( isset( $unavailable) && !empty( $unavailable ) ) {
			foreach( $unavailable[0] as $off_limits ) {
				$unavailability .= '<li>' . ucfirst( $off_limits['day'] ) . ', ' . date( "g:i", strtotime( $off_limits['start'] ) ) . ' - ' . date( "g:i", strtotime( $off_limits['end'] ) ) . '</li>';
			}
		} else {
			$unavailability .= 'You have not submitted any unavailability.';
		}

	} else {
		$unavailability .= "<p>" . __('You must be logged in to view this content.', 'wpaesm') . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$unavailability .= wp_login_form($args);
	}

	return $unavailability;
}
add_shortcode( 'your_unavailability', 'wpaesm_your_unavailability_shortcode' );


?>