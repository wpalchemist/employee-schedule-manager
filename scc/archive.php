<?php 
/**
 * Archive
 *
 * Once a month, go through the site and remove old shifts, employees, and clients
 *
 * @link URL
 *
 * @package WordPress
 * @subpackage Component
 * @since x.x.x (when the file was introduced)
 */

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Create monthly recurrence.
 *
 */
function wpaesm_archive_old_data_custom_recurrence( $schedules ) {
	$schedules['monthly'] = array(
		'display' => __( 'Once a Month', 'textdomain' ),
		'interval' => 2635200,
	);
	return $schedules;
}
add_filter( 'cron_schedules', 'wpaesm_archive_old_data_custom_recurrence' );

/**
 * Schedule cron job.
 *
 * Schedule the archive event to happen monthly.
 *
 * @since 2.2.0
 */
function wpaesm_archive_old_data() {
	if ( ! wp_next_scheduled( 'wpaesm_export_and_delete' ) ) {
		wp_schedule_event( current_time( 'timestamp' ), 'monthly', 'wpaesm_export_and_delete' );
	}
}
add_action( 'init', 'wpaesm_archive_old_data' );

/**
 * Export and delete.
 *
 * Find all information on the site that is more than a year old, export it to CSV, and delete it.
 *
 * @since 2.2.0
 *
 */
add_action( 'wpaesm_export_and_delete', 'wpaesm_do_archiving' );
function wpaesm_do_archiving() {

  	$before = date( 'Y-m-d', strtotime( '-6 months', time() ) );
	$csv_list = array();
	$deleted = array();

	$oldshifts = wpaesm_find_old_shifts( $before );
	if( false !== $oldshifts ) {
		$csv_list['shifts'] = wpaesm_export_old_shifts( $oldshifts );
		$deleted['shifts'] = wpaesm_delete_old_posts( $oldshifts );
	}

	$olddocs = wpaesm_find_old_documents( $before );
	if( false !== $olddocs ) {
		$csv_list['documents'] = wpaesm_export_old_docs( $olddocs );
		$deleted['documents'] = wpaesm_delete_old_posts( $olddocs );
	}

	$oldexpenses = wpaesm_find_old_expenses( $before );
	if( false !== $oldexpenses ) {
		$csv_list['expenses'] = wpaesm_export_old_expenses( $oldexpenses );
		$deleted['expenses'] = wpaesm_delete_old_posts( $oldexpenses );
	}

//	$oldclients = wpaesm_find_old_clients( $before );
//	if( !empty( $oldclients ) ) {
//		$csv_list['clients'] = wpaesm_export_old_clients( $oldclients );
//		$deleted['clients'] = wpaesm_delete_old_clients( $oldclients );
//	}

	$oldemployees = wpaesm_find_old_employees( $before );
	if( false !== $oldemployees ) {
		$csv_list['employees'] = wpaesm_export_old_employees( $oldemployees );
		$deleted['employees'] = wpaesm_delete_old_employees( $oldemployees );
	}

	wpaesm_send_archive_summary( $csv_list, $deleted );

}

/**
 * Find old shifts.
 *
 * Find all of the shifts older than the given date.
 *
 * @since 2.2.0
 *
 * @param string  Date (Y-m-d).
 * @return WP_Query Object  Query of old shifts.
 */
function wpaesm_find_old_shifts( $before ) {

	$args = array( 
	    'post_type' => 'shift',
	    'posts_per_page' => -1,
	    'meta_key' => '_wpaesm_date',
	    'meta_value' => $before,
	    'meta_compare' => '<=', 
	);
	
	$oldshifts = new WP_Query( $args );
	
	if ( $oldshifts->have_posts() ) {
		return $oldshifts;
	} else {
		return false;
	}
		
}

/**
 * Find old documents.
 *
 * Find all the documents associated created before the given date.
 *
 * @since 2.2.0
 *
 * @param string  Date (Y-m-d).
 * @param array  List of shifts.
 * @return WP_Query object  Query result with docs.
 */
 function wpaesm_find_old_documents( $before ) {

	 $args = array(
			 'post_type' => 'document',
			 'posts_per_page' => -1,
			 'meta_key' => '_wpaesm_created',
			 'meta_value' => $before,
			 'meta_compare' => '<=',
	 );

	 $documents = new WP_Query( $args );

	// Display connected pages
	if ( $documents->have_posts() ) {
		return $documents;
	} else {
		return false;
	}

 }

 /**
  * Find old expenses.
  *
  * Find all the expenses older than the given date.
  *
  * @since 2.2.0
  *
  * @param string  date (Y-m-d).
  * @return WP_Query object  Query results.
  */
 function wpaesm_find_old_expenses( $before ) {

 	$args = array( 
	    'post_type' => 'expense',
	    'posts_per_page' => -1,
	    'meta_key' => '_wpaesm_date',
	    'meta_value' => $before,
	    'meta_compare' => '<=', 
	);
	
	$oldexpenses = new WP_Query( $args );
	
	if ( $oldexpenses->have_posts() ) {
		return $oldexpenses;
	} else {
		return false;
	}

 }

 /**
  * Find old clients.
  *
  * Find clients with no associated shifts after the given date.
  *
  * @since 2.2.0
  *
  * @param string  Date (Y-m-d).
  * @return array  Array of client ids.
  */
 function wpaesm_find_old_clients( $before ) {
 	$clients_to_archive = array();

 	// get all clients in the "archived" category
 	$args = array( 
 	    'tax_query' => array(
 	      array(
 	        'taxonomy' => 'client_category',
 	        'field' => 'slug',
 	        'terms' => 'archived',
 	      )
 	    ),
 	    'post_type' => 'client',  
 	    'posts_per_page' => -1, 
 	);
 	
 	$archived_clients = new WP_Query( $args );
 	
 	if ( $archived_clients->have_posts() ) :
	 	while ( $archived_clients->have_posts() ) : $archived_clients->the_post();
	 		// for each client, check if there are any associated shifts after the given date
	 		$this_client_id = get_the_id();
	 		$args = array( 
	 		    'post_type' => 'shift', 
	 		    'connected_type' => 'shifts_to_clients',
				'connected_items' => $this_client_id,
				'meta_key' => '_wpaesm_date',
			    'meta_value' => $before,
			    'meta_compare' => '>=', 
	 		);
	 		
	 		$shifts = new WP_Query( $args );

	 		if ( !$shifts->have_posts() ) {
	 			$clients_to_archive[] = $this_client_id;
	 		}

	 		wp_reset_postdata();
	 	endwhile;
 	endif;
 	
 	wp_reset_postdata();
 	
 	return $clients_to_archive;

 }

 /**
  * Find old employees.
  *
  * Find employees with no associated shifts after the given date.
  *
  * @since 2.2.0
  *
  * @param string  Date (Y-m-d).
  * @return Array  Array of employees.
  */
 function wpaesm_find_old_employees( $before ) {

 	$employees_to_archive = array();

 	// find all users who are not employees, admins, or supervisors
 	$args = array(
		'role' => 'archived'
	);

	$user_query = new WP_User_Query( $args );

	if ( ! empty( $user_query->results ) ) {
		foreach ( $user_query->results as $user ) {
			// for each user, check if there are any associated shifts after the given date
	 		$args = array( 
	 		    'post_type' => 'shift', 
	 		    'connected_type' => 'shifts_to_employees',
				'connected_items' => $user->ID,
				'meta_key' => '_wpaesm_date',
			    'meta_value' => $before,
			    'meta_compare' => '>=', 
	 		);
	 		
	 		$shifts = new WP_Query( $args );

	 		if ( !$shifts->have_posts() ) {
	 			$employees_to_archive[] = $user->ID;
	 		}

	 		wp_reset_postdata();
		}
		return $employees_to_archive;
	} else {
		return false;
	}

 }

/**
 * Export old shifts.
 *
 * Create and save a XSLX file of the old shifts.
 *
 * @since 2.2.0
 *
 * @param $oldshifts WP_Query object
 *
 * @return string URL of saved file
 */
function wpaesm_export_old_shifts( $oldshifts ) {

	$uploads = wp_upload_dir();
	$filename = 'archive-' . date( 'Y-m-d', current_time( 'timestamp' ) ) . '-shifts.xlsx';
	if( !is_dir( $uploads['basedir'] . "/wpaesm_archives/" ) ) {
		mkdir( $uploads['basedir'] . "/wpaesm_archives/" );
	}
	$file = $uploads['basedir'] . "/wpaesm_archives/" . $filename;
	$file_link = $uploads['baseurl'] . "/wpaesm_archives/" . $filename;

	// set up spreadsheet
	include_once( plugin_dir_path( __FILE__ ) . 'xlsxwriter.class.php' );
	ini_set('display_errors', 0);
	ini_set('log_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE);

	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');

	$header_row = array(
		'Shift ID' => 'string',
		'Title' => 'string',
		'Description' => 'string',
		'Date' => 'string',
		'Start Time' => 'string',
		'End Time' => 'string',
		'Clock In' => 'string',
		'Clock Out' => 'string',
		'Clock In Location' => 'string',
		'Clock Out Location' => 'string',
		'Break Taken?' => 'string',
		'Shift Type' => 'string',
		'Shift Status' => 'string',
		'Employee' => 'string',
		'Client' => 'string',
		'Document ID' => 'string',
		'Internal Notes' => 'string',
		'Employee Notes' => 'string',
	);

	$data_rows = array();

	while ( $oldshifts->have_posts() ) : $oldshifts->the_post();
		$row = array();

		global $shift_metabox;
		$meta = $shift_metabox->the_meta();

		$row[0] = get_the_id();
		$row[1] = get_the_title();
		$row[2] = get_the_content();
		if( isset( $meta['date'] ) ) {
			$row[3] = $meta['date'];
		} else {
			$row[3] = '';
		}
		if( isset( $meta['starttime'] ) ) {
			$row[4] = $meta['starttime'];
 		} else {
			$row[4] = '';
		}
		if( isset( $meta['endtime'] ) ) {
			$row[5] = $meta['endtime'];
		} else {
			$row[5] = '';
		}
		if( isset( $meta['clockin'] ) ) {
			$row[6] = $meta['clockin'];
		} else {
			$row[6] = '';
		}
		if( isset( $meta['clockout'] ) ) {
			$row[7] = $meta['clockout'];
		} else {
			$row[7] = '';
		}
		if( isset( $meta['location_in'] ) ) {
			$row[8] = $meta['location_in'];
		} else {
			$row[8] = '';
		}
		if( isset( $meta['location_out'] ) ) {
			$row[9] = $meta['location_out'];
		} else {
			$row[9] = '';
		}
		if( isset( $meta['break'] ) && '1' == $meta['break'] ) {
			$row[10] = 'Yes';
		} else {
			$row[10] = 'No';
		}
		$row[11] = '';
		$types = wp_get_post_terms( get_the_id(), 'shift_type', array( "fields" => "all" ) );
		if( !empty( $types ) ) {
			foreach( $types as $type ) {
				$row[11] .= $type->name . ' ';
			}
		}
		$row[12] = '';
		$statuses = wp_get_post_terms( get_the_id(), 'shift_status', array( "fields" => "all" ) );
		if( !empty( $statuses ) ) {
			foreach( $statuses as $status ) {
				$row[12] .= $status->name . ' ';
			}
		}
		$employees = get_users( array(
				'connected_type' => 'shifts_to_employees',
				'connected_items' => get_the_id(),
		) );
		$row[13] = '';
		if( !empty( $employees ) ) {
			foreach( $employees as $employee ) {
				$row[13] .= $employee->display_name . ' ';
			}
		}
		$clients = get_posts( array(
			'connected_type' => 'shifts_to_clients',
			'connected_items' => get_the_id(),
			'nopaging' => true,
			'suppress_filters' => false
		) );
		$row[14] = '';
		if( !empty( $clients ) ) {
			foreach( $clients as $client ) {
				$row[14] .= $client->post_title . ' ';
			}
		}
		$docs = get_posts( array(
				'connected_type' => 'documents_to_shifts',
				'connected_items' => get_the_id(),
				'nopaging' => true,
				'suppress_filters' => false
		) );
		$row[15] = '';
		if( !empty( $docs ) ) {
			foreach( $docs as $doc ) {
				$row[15] .= $doc->post_ID . ' ';
			}
		}
		if( isset( $meta['shiftnotes'] ) ) {
			$row[16] = $meta['shiftnotes'];
		} else {
			$row[16] = '';
		}
		if( isset( $meta['employeenote'] ) && !empty( $meta['employeenote'] ) ) {
			$row[17] = '';
			foreach( $meta['employeenote'] as $note ) {
				$row[17] .= "Date: " . $note['notedate'] . ' Note: ' . $note['notetext'] . ' ';
			}
		} else {
			$row[17] = '';
		}

		$data_rows[] = $row;
	endwhile;

	$writer = new XLSXWriter();
	$writer->writeSheet( $data_rows,'Sheet1', $header_row );
	$writer->writeToFile( $file );

	return $file_link;
}


/**
 * Export documents
 *
 * Create a CSV of the documents
 *
 * @since 2.2.0
 *
 * @param $olddocs
 *
 * @return string URL of CSV file
 */
function wpaesm_export_old_docs( $olddocs ) {

	$uploads = wp_upload_dir();
	$filename = 'archive-' . date( 'Y-m-d', current_time( 'timestamp' ) ) . '-documents.xlsx';
	$file = $uploads['basedir'] . "/wpaesm_archives/" . $filename;
	$file_link = $uploads['baseurl'] . "/wpaesm_archives/" . $filename;

	// set up spreadsheet
	include_once( plugin_dir_path( __FILE__ ) . 'xlsxwriter.class.php' );
	ini_set('display_errors', 0);
	ini_set('log_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE);

	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');

	$header_row = array(
		'Document ID' => 'string',
		'Title' => 'string',
		'Description' => 'string',
		'Date Created' => 'string',
		'Date Approved' => 'string',
		'Admin Feedback' => 'string',
		'Behavior Ratings' => 'string',
		'Employee' => 'string',
		'Client' => 'string',
		'Shift ID' => 'string',
	);

	$data_rows = array();

	while ( $olddocs->have_posts() ) : $olddocs->the_post();
		$row = array();

		$docid = get_the_id();

		global $document_metabox;
		$docmeta = $document_metabox->the_meta();

		global $document_history;
		$dochistory = $document_history->the_meta();

		$row[0] = $docid;
		$row[1] = get_the_title();
		$row[2] = get_the_content();
		if( isset( $dochistory['created'] ) ) {
			$row[3] = $dochistory['created'];
		} else {
			$row[3] = '';
		}
		if( isset( $dochistory['approved'] ) ) {
			$row[4] = $dochistory['approved'];
		} else {
			$row[4] = '';
		}
		if( isset( $dochistory['feedback'] ) ) {
			$row[5] = $dochistory['feedback'];
		} else {
			$row[5] = '';
		}
		$row[6] = '';
		if( !empty( $docmeta['behavior'] ) ) {
			foreach( $docmeta['behavior'] as $behavior ) {
				$row[6] = $behavior['goal'] . ': ' . $behavior['score'];
			}
		}
		$row[7] = '';
		$users = get_users( array(
			'connected_type' => 'documents_to_employees',
			'connected_items' => $docid,
		) );
		foreach( $users as $user ) {
			$row[7] .= $user->display_name;
		}
		$row[8] = '';
		$clients = new WP_Query( array(
			'connected_type' => 'documents_to_clients',
			'connected_items' => $docid,
		) );
		if( $clients->have_posts() ) {
			while ( $clients->have_posts() ) : $clients->the_post();
				$row[8] .= get_the_title();
			endwhile;
		}
		wp_reset_postdata();
		$row[9] = '';
		$shifts = get_posts( array(
			'connected_type' => 'documents_to_shifts',
			'connected_items' => $docid,
		) );
		if( !empty( $shifts ) ) {
			foreach( $shifts as $shift ) {
				$row[9] = $shift->ID;
			}
		}
		wp_reset_postdata();

		$data_rows[] = $row;
	endwhile;

	$writer = new XLSXWriter();
	$writer->writeSheet( $data_rows,'Sheet1', $header_row );
	$writer->writeToFile( $file );

	return $file_link;
}


/**
 * Export old expenses
 *
 * Create a CSV of old expenses and save it in the uploads directory
 *
 * @since 2.2.0
 *
 * @param $oldexpenses
 *
 * @return string
 */
function wpaesm_export_old_expenses( $oldexpenses ) {
	$uploads = wp_upload_dir();
	$filename = 'archive-' . date( 'Y-m-d', current_time( 'timestamp' ) ) . '-expenses.xlsx';
	$file = $uploads['basedir'] . "/wpaesm_archives/" . $filename;
	$file_link = $uploads['baseurl'] . "/wpaesm_archives/" . $filename;

	// set up spreadsheet
	include_once( plugin_dir_path( __FILE__ ) . 'xlsxwriter.class.php' );
	ini_set('display_errors', 0);
	ini_set('log_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE);

	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');

	$header_row = array(
		'Expense ID' => 'string',
		'Title' => 'string',
		'Description' => 'string',
		'Date' => 'string',
		'Amount' => 'string',
		'Category' => 'string',
		'Status' => 'string',
		'Employee' => 'string',
		'Client' => 'string',
	);

	$data_rows = array();

	while ( $oldexpenses->have_posts() ) : $oldexpenses->the_post();

		global $expense_metabox;
		$expense_meta = $expense_metabox->the_meta();

		$row = array();

		$row[0] = get_the_id();
		$row[1] = get_the_title();
		$row[2] = get_the_content();
		if( isset( $expense_meta['date'] ) ) {
			$row[3] = $expense_meta['date'];
		} else {
			$row[3] = '';
		}
		if( isset( $expense_meta['amount'] ) ) {
			$row[4] = $expense_meta['amount'];
		} else {
			$row[4] = '';
		}
		$categories = wp_get_post_terms( get_the_id(), 'expense_category', array( "fields" => "all" ) );
		$row[5] = '';
		if( !empty( $categories ) ) {
			foreach( $categories as $category ) {
				$row[5] .= $category->name . ' ';
			}
		}
		$statuses = wp_get_post_terms( get_the_id(), 'expense_status', array( "fields" => "all" ) );
		$row[6] = '';
		if( !empty( $statuses ) ) {
			foreach( $statuses as $status ) {
				$row[6] .= $status->name . ' ';
			}
		}
		$row[7] = '';
		$users = get_users( array(
				'connected_type' => 'expenses_to_employees',
				'connected_items' => get_the_id(),
		) );
		foreach( $users as $user ) {
			$row[7] .= $user->display_name;
		}
		$row[8] = '';
		$clients = new WP_Query( array(
				'connected_type' => 'expenses_to_clients',
				'connected_items' => get_the_id(),
		) );
		if( $clients->have_posts() ) {
			while ( $clients->have_posts() ) : $clients->the_post();
				$row[8] .= get_the_title();
			endwhile;
		}
		wp_reset_postdata();

		$data_rows[] = $row;
	endwhile;

	$writer = new XLSXWriter();
	$writer->writeSheet( $data_rows,'Sheet1', $header_row );
	$writer->writeToFile( $file );

	return $file_link;
}

/**
 * Export old clients
 *
 * Export old clients to a CSV file and save the file
 *
 * @param $oldclients array of client IDs
 *
 * @return string URL of export file
 */
function wpaesm_export_old_clients( $oldclients ) {
	$uploads = wp_upload_dir();
	$filename = 'archive-' . date( 'Y-m-d', current_time( 'timestamp' ) ) . '-clients.xlsx';
	$file = $uploads['basedir'] . "/wpaesm_archives/" . $filename;
	$file_link = $uploads['baseurl'] . "/wpaesm_archives/" . $filename;

	// set up spreadsheet
	include_once( plugin_dir_path( __FILE__ ) . 'xlsxwriter.class.php' );
	ini_set('display_errors', 0);
	ini_set('log_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE);

	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');

	$header_row = array(
		'Client ID' => 'string',
		'Name' => 'string',
		'Description' => 'string',
		'Intake Form' => 'string',
		'Diagnosis' => 'string',
		'Triggers' => 'string',
		'Activities' => 'string',
		'Restraint' => 'string',
		'Behavior Goals' => 'string',
		'Sub Plan' => 'string',
		'Crisis Plan' => 'string',
		'School' => 'string',
		'Funder Name' => 'string',
		'Funder Email' => 'string',
		'Funder Organization' => 'string',
		'Address(es)' => 'string',
		'Phone Number(s)' => 'string',
		'Email Address(es)' => 'string',
		'Parents/Guardians' => 'string',
		'Category' => 'string',
		'Customer' => 'string',
	);

	$data_rows = array();

	foreach( $oldclients as $client ) {

		$row = array();

		$post = get_post( $client );

		$row[0] = $client;
		$row[1] = $post->post_title;
		$row[2] = $post->post_content;
		$intake = get_post_meta( $client, '_wpaesm_intake', true );
		if( $intake ) {
			$row[3] = $intake;
		} else {
			$row[3] = '';
		}
		$diagnosis = get_post_meta( $client, '_wpaesm_diagnosis', true );
		if( $diagnosis ) {
			$row[4] = $diagnosis;
		} else {
			$row[4] = '';
		}
		$triggers = get_post_meta( $client, '_wpaesm_triggers', true );
		if( isset( $triggers ) ) {
			$row[5] = $triggers;
		} else {
			$row[5] = '';
		}
		$activities = get_post_meta( $client, '_wpaesm_activities', true );
		if( isset( $activities ) ) {
			$row[6] = $activities;
		} else {
			$row[6] = '';
		}
		$restraint = get_post_meta( $client, '_wpaesm_restraint', true );
		if( isset( $restraint ) ) {
			$row[7] = $restraint;
		} else {
			$row[7] = '';
		}
		$row[8] = '';
		$goals = get_post_meta( $client, '_wpaesm_goals', true );
		if( !empty( $goals ) ) {
			foreach( $goals as $goal ) {
				$row[8] .= $goal['goal_name'] . ', ';
			}
		}
		$sub = get_post_meta( $client, '_wpaesm_sub', true );
		if( isset( $sub ) ) {
			$row[9] = $sub;
		} else {
			$row[9] = '';
		}
		$crisis = get_post_meta( $client, '_wpaesm_crisis', true );
		if( isset( $crisis ) ) {
			$row[10] = $crisis;
		} else {
			$row[10] = '';
		}
		$school = get_post_meta( $client, '_wpaesm_school', true );
		if( isset( $school ) ) {
			$row[11] = $school;
		} else {
			$row[11] = '';
		}
		$funder_name = get_post_meta( $client, '_wpaesm_funder_name', true );
		if( isset( $funder_name ) ) {
			$row[12] = $funder_name;
		} else {
			$row[12] = '';
		}
		$funder_shcool = get_post_meta( $client, '_wpaesm_funder_shcool', true );
		if( isset( $funder_shcool ) ) {
			$row[13] = $funder_shcool;
		} else {
			$row[13] = '';
		}
		$funder_organization = get_post_meta( $client, '_wpaesm_funder_organization', true );
		if( isset( $funder_organization ) ) {
			$row[14] = $funder_organization;
		} else {
			$row[14] = '';
		}
		$row[15] = '';
		$addresses = get_post_meta( $client, '_wpaesm_address', true );
		if( !empty( $addresses ) ) {
			foreach( $addresses as $address ) {
				$row[15] .= $address['addressname'] . ': ' . $address['address'] . ' ' . $address['city'] . ' ' . $address['state'] . ', ' . $address['zip'] . ', ';
			}
		}
		$row[16] = '';
		$phones = get_post_meta( $client, '_wpaesm_clientphone', true );
		if( !empty( $phones ) ) {
			foreach ( $phones as $phone ) {
				$row[16] .= $phone['phonenumber'] . ' (' . $phone['phonetype'] . ')';
			}
		}
		$row[17] = '';
		$emails = get_post_meta( $client, '_wpaesm_clientemail', true );
		if( !empty( $emails ) ) {
			foreach ( $emails as $email ) {
				$row[17] .= $email['emailaddress'] . ' (' . $email['emailtype'] . ')';
			}
		}
		$row[18] = '';
		$parents = get_post_meta( $client, '_wpaesm_parents', true );
		if( !empty( $parents ) ) {
			foreach( $parents as $parent ) {
				$row[18] = $parent['name'] . '( ' . $parent['relation'] . ') ';
			}
		}
		$categories = wp_get_post_terms( $client, 'client_category', array( "fields" => "all" ) );
		$row[19] = '';
		if( !empty( $categories ) ) {
			foreach( $categories as $category ) {
				$row[19] .= $category->name . ' ';
			}
		}
		$customers = wp_get_post_terms( $client, 'customer', array( "fields" => "all" ) );
		$row[20] = '';
		if( !empty( $customers ) ) {
			foreach( $customers as $customer ) {
				$row[20] .= $customer->name . ' ';
			}
		}

		$data_rows[] = $row;
	}

	$writer = new XLSXWriter();
	$writer->writeSheet( $data_rows,'Sheet1', $header_row );
	$writer->writeToFile( $file );

	return $file_link;
}


/**
 * Export old employees
 *
 * Create a spreadsheet of old employees
 *
 * @param $oldemployees Array of employee IDs
 *
 * @return string URL of spreadsheet
 */
function wpaesm_export_old_employees( $oldemployees ) {

	$uploads = wp_upload_dir();
	$filename = 'archive-' . date( 'Y-m-d', current_time( 'timestamp' ) ) . '-employees.xlsx';
	$file = $uploads['basedir'] . "/wpaesm_archives/" . $filename;
	$file_link = $uploads['baseurl'] . "/wpaesm_archives/" . $filename;

	// set up spreadsheet
	include_once( plugin_dir_path( __FILE__ ) . 'xlsxwriter.class.php' );
	ini_set('display_errors', 0);
	ini_set('log_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE);

	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');

	$header_row = array(
			'Username' => 'string',
			'First Name' => 'string',
			'Last Name' => 'string',
			'Email' => 'string',
			'Address' => 'string',
			'Phone Number' => 'string',
			'Pay Rate' => 'string',
			'Deductions: Health Insurance' => 'string',
			'Deductions: Health Ins Family' => 'string',
			'Deductions: Dental Insurance' => 'string',
			'Deductions: Dental Ins Family' => 'string',
			'Deductions: Garnishment' => 'string',
			'Deductions: Witholding' => 'string',
			'Deductions: Other' => 'string',
			'Timesheets' => 'string',
	);

	$data_rows = array();

	foreach( $oldemployees as $employee ) {
		$user = get_user_by( 'id', $employee );

		$row = array();

		$row[0] = $user->user_login;
		$row[1] = $user->first_name;
		$row[2] = $user->last_name;
		$row[3] = $user->user_email;
		$row[4] = get_user_meta( $user, 'address' ) . get_user_meta( $user, 'city' ) . get_user_meta( $user, 'state' ) . get_user_meta( $user, 'zip' );
		$row[5] = get_user_meta( $user, 'phone' );
		$row[6] = get_user_meta( $user, 'wage' );
		$row[7] = get_user_meta( $user, 'deductions_health_self' );
		$row[8] = get_user_meta( $user, 'deductions_health_family' );
		$row[9] = get_user_meta( $user, 'deductions_dental_self' );
		$row[10] = get_user_meta( $user, 'deductions_dental_family' );
		$row[11] = get_user_meta( $user, 'deductions_garnish' );
		$row[12] = get_user_meta( $user, 'deductions_withhold' );
		$row[13] = get_user_meta( $user, 'deductions_other' );
		$row[14] = '';
		$timesheets = get_user_meta( $user, 'timesheet_history' );
		if( !empty ( $timesheets ) ) {
			foreach( $timesheets as $time ) {
				$row[14] .= $time['file'] . ' ';
			}
		}
		$data_rows[] = $row;
	}

	$writer = new XLSXWriter();
	$writer->writeSheet( $data_rows,'Sheet1', $header_row );
	$writer->writeToFile( $file );

	return $file_link;
}



/**
 * Delete old posts.
 *
 * Given a WP_Query object, delete all of the posts in the query.
 *
 * @since 2.2.0
 * @param $oldshifts WP_Query object
 *
 * @return int number of deleted posts
 */
function wpaesm_delete_old_posts( $oldposts ) {

	$i = 0;

	while ( $oldposts->have_posts() ) : $oldposts->the_post();
		$delete = wp_delete_post( get_the_id(), true );
		if( $delete ) {
			$i++;
		}
	endwhile;

	return $i;
}

/**
 * Delete old clients
 *
 * @param $oldclients array of client IDs
 *
 * @return int number of deleted clients
 */
function wpaesm_delete_old_clients( $oldclients ) {

	$i = 0;

	foreach( $oldclients as $client ) {
		$delete = wp_delete_post( $client, true );
		if( $delete ) {
			$i++;
		}
	}

	return $i;
}

/**
 * Delete old employees
 *
 * @param $oldemployees array of employee IDs
 *
 * @return int number of deleted employees
 */
function wpaesm_delete_old_employees( $oldemployees ) {

	$i = 0;

	foreach( $oldemployees as $employee ) {
		require_once(ABSPATH.'wp-admin/includes/user.php' );
		$delete = wp_delete_user( $employee );
		if( $delete ) {
			$i++;
		}
	}
	return $i;
}

/**
 * Email summary
 *
 * Email site admin with a summary of what was deleted and links to archive files
 *
 * @param $csv_list array list of archive files
 * @param $deleted array list of how many items were deleted
 */
function wpaesm_send_archive_summary( $csv_list, $deleted ) {

	$options = get_option( 'wpaesm_options' );

	$from = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
	$to = $options['admin_notification_email'];
	$cc = 'morgan@alchemycs.com';
	$subject = 'Results of monthly archive';
	$message_text = '
		<p>Old data from your site has been archived and deleted. Here is a summary of what was deleted:
			<ul>
				<li>
					<strong>Shifts:</strong> ' . $deleted['shifts'] . ' were deleted<br />';
					if( isset( $csv_list['shifts'] ) ) {
						$message_text .= '<a href = "' . $csv_list['shifts'] . '" > Download spreadsheet of deleted shifts </a >';
					}
				$message_text .=
				'</li>
				<li>
					<strong>Documents:</strong> ' . $deleted['documents'] . ' were deleted<br />';
					if( isset( $csv_list['documents'] ) ) {
						$message_text .= '<a href = "' . $csv_list['documents'] . '" > Download spreadsheet of deleted documents </a >';
					}
				$message_text .=
				'</li>
				<li>
					<strong>Expenses:</strong> ' . $deleted['expenses'] . ' were deleted<br />';
					if( isset( $csv_list['expenses'] ) ) {
						$message_text .= '<a href="' . $csv_list['expenses'] . '">Download spreadsheet of deleted expenses</a>';
					}
				$message_text .=
				'</li>
				<li>
					<strong>Clients:</strong> ' . $deleted['clients'] . ' were deleted<br />';
					if( isset( $csv_list['clients'] ) ) {
						$message_text .= '<a href="' . $csv_list['clients'] . '">Download spreadsheet of deleted clients</a>';
					}
				$message_text .=
				'</li>
				<li>
					<strong>Employees:</strong> ' . $deleted['employees'] . ' were deleted<br />';
					if( isset( $csv_list['employees'] ) ) {
						$message_text .= '<a href="' . $csv_list['employees'] . '">Download spreadsheet of deleted employees</a>';
					}
				$message_text .=
				'</li>
			</ul>
		</p>';
	wpaesm_send_email( $from, $to, $cc, $subject, $message_text, '' );
}