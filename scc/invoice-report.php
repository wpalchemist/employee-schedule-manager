<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// Create page for invoice report

function wpa_add_invoice_page() {
	global $invoice_report;
	$super_users = array( 1, 44, 86 );
	if( in_array( get_current_user_id(), $super_users ) ) {
		$invoice_report = add_submenu_page( '/employee-schedule-manager/options.php', 'Invoice Report', 'Invoice Report', 'manage_options', 'invoice', 'wpa_create_invoice_report_form' );
	}
}
add_action('admin_menu', 'wpa_add_invoice_page');

function wpa_enqueue_invoice_script($hook) {
	global $invoice_report;
	if ( $hook == $invoice_report ) {
		wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
		wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
	}
}
add_action( 'admin_enqueue_scripts', 'wpa_enqueue_invoice_script' );

// Create invoice report

function wpa_create_invoice_report_form() {

	$super_users = array( 1, 44, 86 );
	if( !in_array( get_current_user_id(), $super_users ) ) {
		wp_die( 'You do not have sufficient permissions to view this page' );
	} ?>

	<?php if( $_POST ) {
		if( ( $_POST['thisdate'] == '____-__-__' ) || ( $_POST['repeatuntil'] == '____-__-__' ) ) {
			_e( 'You must enter both a start date and an end date to create a report.', 'wpaesm' );
		} elseif($_POST['thisdate'] > $_POST['repeatuntil']) {
			_e( 'The report end date must be after the report begin date.', 'wpaesm' );
		} else {
			$reportstart = $_POST['thisdate'];
			$reportend = $_POST['repeatuntil'];
			wpa_generate_invoice_report();
		}
	} ?>

	<div class="wrap">

		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h2>Generate Invoice Report</h2>

		<form method='post' action='<?php echo admin_url( 'admin.php?page=invoice'); ?>' id='invoice-report'>
			<table class="form-table">
				<tr>
					<th scope="row"><label>Date Range:</label></th>
					<td>
						From <input type="text" size="10" name="thisdate" id="thisdate" value="" /> to <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="client_category">Client Category</label></th>
					<td>
						<select name="client_category" id="client_category">
							<option value=""></option>
							<?php $clicats = get_terms( 'client_category' );
							foreach( $clicats as $cat ) { ?>
								<option value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="customer">Customer</label></th>
					<td>
						<select name="customer" id="customer">
							<option value=""></option>
							<?php $customers = get_terms( 'customer' );
							foreach( $customers as $customer ) { ?>
								<option value="<?php echo $customer->term_id; ?>"><?php echo $customer->name; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
			<p class="submit">
				<input type="hidden" name="wpa_generate_invoice_report" value="wpa_generate_invoice_report">
				<input type="submit" class="button-primary" value="<?php _e( 'Download Report', 'wpaesm' ); ?>" />
			</p>
		</form>

	</div>
	<?php
}

add_action('admin_init','wpa_generate_invoice_report');

function wpa_generate_invoice_report() {
	if ( isset( $_POST['wpa_generate_invoice_report'] ) ) {
		if( ( $_POST['thisdate'] == '____-__-__' ) || ( $_POST['repeatuntil'] == '____-__-__' ) ) {
			$msg = '<div class="error"><p>You must enter both a start date and an end date to create a report.</p></div>';
			wp_die( $msg );
		} elseif( $_POST['thisdate'] > $_POST['repeatuntil'] ) {
			wp_die( 'The report end date must be after the report begin date.' );
		}

		$client_category = '';
		if( isset( $_POST['client_category'] ) ) {
			$client_category = $_POST['client_category'];
		}

		// set up spreadsheet
		include_once( plugin_dir_path( __FILE__ ) . 'xlsxwriter.class.php' );
		ini_set('display_errors', 0);
		ini_set('log_errors', 1);
		error_reporting(E_ALL & ~E_NOTICE);

		$filename = "invoice-report.xlsx";
		header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');

		// find all the days in the date range
		$days = wpa_get_date_range( $_POST['thisdate'], $_POST['repeatuntil'] );

		// make spreadsheet header
		$header_first = array(
			'Client Name' => 'string',
			'Staff Name' => 'string',
		);

		$header_days = array();
		foreach( $days as $day ) {
			$header_days[$day] = 'string';
		}

		$more_header = array(
			'Billable Hours' => 'integer',
			'TOTAL' => 'dollar',
			'Funder' => 'string',
			'CIC Miles' => 'integer',
			'RT Miles' => 'integer',
			'# Shifts' => 'integer',
			'Total Mileage' => 'dollar',
			'Bill' => 'dollar',
			'Admin Fee' => 'dollar',
			'Notes' => 'string'
		);

		$header = $header_first + $header_days + $more_header;

		$row_count = 1;

		$data = array();

		// get all the customers
		if( isset( $_POST['customer'] ) ) {
			$customers = get_terms( 'customer', array( 'include' => $_POST['customer'] ) );
		} else {
			$customers = get_terms( 'customer' );
		}
		// $msg = "<pre>" . print_r($customers, true) . "</pre>";
		// wp_die($msg);

		// for each customer, get all the clients
		$customer_client_list = array();

		foreach( $customers as $customer ) {
			$hourly_rate = get_tax_meta( $customer->term_id,'hourly_rate' );

			$args = array(
				'post_type' => 'client',
				'tax_query' => array(
					array(
						'taxonomy' => 'customer',
						'field'    => 'id',
						'terms'    => $customer->term_id,
					),
				),
				'posts_per_page' => -1
			);

			if( isset( $client_category ) && '' !== $client_category ) {
				$args['tax_query'][] = array(
					'taxonomy' => 'client_category',
					'field'    => 'slug',
					'terms'    => $client_category,
				);
			}

			$clients = new WP_Query( $args );
			if ( $clients->have_posts() ) :
				// make a row in the spreadsheet for the customer
				$row_count++; // this tells us what row we are on
				$data[] = array( $customer->name ); // this row is just the customer name

				while ( $clients->have_posts() ) : $clients->the_post();
					// for each day in the range, find the shifts and put them in an array; also make array of employees
					$client = get_the_title();
					$this_client_id = get_the_id();
					$customer_client_list[$customer->name][$client] = '';

					global $client_metabox;
					$client_meta = $client_metabox->the_meta();
					$customer_client_list[$customer->name][$client]['funder'] = '';
					if( isset( $client_meta['funder_name'] ) || isset( $client_meta['funder_organization'] ) || isset( $client_meta['funder_shcool'] ) ) {
						if( isset( $client_meta['funder_name'] ) ) {
							$customer_client_list[$customer->name][$client]['funder'] .= $client_meta['funder_name'];
						}
						if( isset( $client_meta['funder_organization'] ) ) {
							$customer_client_list[$customer->name][$client]['funder'] .= ' (' . $client_meta['funder_organization'] . ')';
						}
						if( isset( $client_meta['funder_shcool'] ) ) {
							$customer_client_list[$customer->name][$client]['funder'] .= ' ' . $client_meta['funder_shcool'];
						}
					}
					$customer_client_list[$customer->name][$client]['round_trip'] = '';
					if( is_array( $client_meta['address'] ) ) {
						foreach( $client_meta['address'] as $address ) {
							$customer_client_list[$customer->name][$client]['round_trip'] = $address['miles'];
						}
					}

					$shifts_by_day = array();
					$client_employees = array();

					// find all this client's shifts within the date range
					$args = array(
						'post_type' => 'shift',
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'shift_status',
								'field' => 'slug',
								'terms' => 'worked',
								'operator' => 'IN'
							),
							array(
								'taxonomy' => 'shift_type',
								'field' => 'slug',
								'terms' => 'documentation',
								'operator' => 'NOT IN'
							),
						),
						'posts_per_page' => -1,
						'meta_query' => array(
							array(
								'key' => '_wpaesm_date',
								'value' => $_POST['thisdate'],
								'type' => 'CHAR',
								'compare' => '>='
							),
							array(
								'key' => '_wpaesm_date',
								'value' => $_POST['repeatuntil'],
								'type' => 'CHAR',
								'compare' => '<='
							),
						),
						'connected_type' => 'shifts_to_clients',
						'connected_items' => get_the_id(),
					);

					$shift_query = new WP_Query( $args );

					// The Loop
					if ( $shift_query->have_posts() ) :
						while ( $shift_query->have_posts() ) : $shift_query->the_post();
							// get employee associated with this shift
							$users = get_users( array(
								'connected_type' => 'shifts_to_employees',
								'connected_items' => get_the_id(),
							) );
							if( isset( $users ) ) {
								foreach( $users as $user ) {
									$employee = $user->display_name;
									$employee_id = $user->ID;
									$client_employees[] = $user->display_name;
								}
							}

							// calculate duration
							global $shift_metabox;
							$meta = $shift_metabox->the_meta();
							if( isset( $meta['starttime'] ) && isset( $meta['endtime'] ) ) {
								$to_time = strtotime($meta['starttime']);
								$from_time = strtotime($meta['endtime']);
								$minutes = round(abs($to_time - $from_time) / 60,2);
								$quarters = round($minutes/15) * 15;
								$duration = $quarters/60;
							} else {
								$duration = 'N/A';
							}

							$date = $meta['date'];

							if( isset( $shifts_by_day[$date] ) && !empty( $shifts_by_day[$date] ) ) {
								$shifts_by_day[$date]['hours'] += $duration;

							} else {

								$shifts_by_day[$date] = array(
									'shift_id' => get_the_id(),
									'employee' => $employee,
									'hours' => $duration,
								);
							}


						endwhile;
					endif;

					$customer_client_list[$customer->name][$client]['shifts'] = $shifts_by_day;
					$employee_list = array_unique( $client_employees );
					$customer_client_list[$customer->name][$client]['employees'] = $employee_list;

					wp_reset_postdata();

					// remove clients that don't have shifts
					if( empty( $customer_client_list[$customer->name][$client] ) ){
						unset( $customer_client_list[$customer->name][$client] );
					}


					// make a row for each employee
					foreach( $customer_client_list[$customer->name][$client]['employees'] as $employee_row ) {
						$row_count++; // this tells us what row we are on
						$name_cells = array( $client, $employee_row );
						$cell_count = 2;
						$day_cells = array();
						foreach( $days as $day ) {
							if( isset( $customer_client_list[$customer->name][$client]['shifts'][$day] ) && $customer_client_list[$customer->name][$client]['shifts'][$day]['employee'] == $employee_row  ) {
								$day_cells[] = $customer_client_list[$customer->name][$client]['shifts'][$day]['hours'];
								$cell_count++;
							} else {
								$day_cells[] = '';
								$cell_count++;
							}
						}


						$cell_letter = wpaesm_get_col_letter($cell_count);

						$rate = floatval( $hourly_rate );

						// billable hours
						$billable_hours = '=SUM(C'.$row_count.':'.$cell_letter.$row_count.')';
						$cell_count++;

						// billable total
						$total_cell_letter = wpaesm_get_col_letter($cell_count);
						$billable_total = '=SUM('.$total_cell_letter.$row_count.'*'.$rate.')';
						$cell_count++;
						$billable_total_cell = wpaesm_get_col_letter($cell_count);

						// funder information
						$funder = $customer_client_list[$customer->name][$client]['funder'];
						$cell_count++;

						// CIC miles
						$cic_miles = 0;
						$cell_count++;
						$cic_cell = wpaesm_get_col_letter($cell_count);
						// find expenses in the date range in client miles category with this employee and client
						$args = array(
							'post_type' => 'expense',
							'tax_query' => array(
								array(
									'taxonomy' => 'expense_category',
									'field' => 'slug',
									'terms' => 'client',
									'include_children' => true,
									'operator' => 'IN'
								),
							),
							'posts_per_page' => -1,
							'meta_query' => array(
								array(
									'key' => '_wpaesm_date',
									'value' => $_POST['thisdate'],
									'type' => 'CHAR',
									'compare' => '>='
								),
								array(
									'key' => '_wpaesm_date',
									'value' => $_POST['repeatuntil'],
									'type' => 'CHAR',
									'compare' => '<='
								)
							),
							'connected_type' => 'expenses_to_clients',
							'connected_items' => $this_client_id,
						);

						$client_mileage = new WP_Query( $args );

						// The Loop
						if ( $client_mileage->have_posts() ) :
							while ( $client_mileage->have_posts() ) : $client_mileage->the_post();
								// if the employee name doesn't match this row, then move on
								$users = get_users( array(
									'connected_type' => 'expenses_to_employees',
									'connected_items' => get_the_id(),
								) );
								foreach( $users as $user ) {
									$employee_name = $user->display_name;
								}
								if( $employee_row !== $employee_name ) {
									continue;
								}

								global $expense_metabox;
								$expense_meta = $expense_metabox->the_meta();
								$this_mileage = intval( $expense_meta['amount']);
								$cic_miles += $this_mileage;
							endwhile;
						endif;

						// Reset Post Data
						wp_reset_postdata();


						// RT miles
						$rt_miles = $customer_client_list[$customer->name][$client]['round_trip'];
						$cell_count++;
						$rt_cell = wpaesm_get_col_letter($cell_count);


						// # shifts
						$no_shifts = count( $customer_client_list[$customer->name][$client]['shifts'] );
						$cell_count++;
						$shift_count_cell = wpaesm_get_col_letter($cell_count);


						// total mileage
						$cell_count++;
						$options = get_option( 'wpaesm_options' );
						if( isset( $options['mileage'] ) ) {
							$mileage_rate = $options['mileage'];
						} else {
							$mileage_rate = 0.56;
						}
						$total_mileage = '=SUM(('.$rt_cell.$row_count.'*'.$shift_count_cell.$row_count.')+'.$cic_cell.$row_count.')*'.$mileage_rate;
						$total_mileage_cell = wpaesm_get_col_letter($cell_count);

						// bill
						$bill_cell = wpaesm_get_col_letter($cell_count);
						$bill = '=SUM('.$billable_total_cell.$row_count.'+'.$total_mileage_cell.$row_count.')';
						$cell_count++;

						$more_cells = array( $billable_hours, $billable_total, $funder, $cic_miles, $rt_miles, $no_shifts, $total_mileage, $bill. '', '' );

						$data[] = array_merge( $name_cells, $day_cells, $more_cells );

					}

				endwhile;
			endif; // end of client loop

			wp_reset_postdata();

		}
// $msg = '<pre>' . print_r($customer_client_list, true) . '</pre>';
// 					wp_die($msg);
		// output the Excel file
		$writer = new XLSXWriter();
		$writer->setAuthor('Some Author');
		$writer->writeSheet($data,'Sheet1',$header);
		$writer->writeToStdOut();
		exit(0);
	}
}


// get all the days between two dates
// http://stackoverflow.com/questions/4312439/php-return-all-dates-between-two-dates-in-an-array
function wpa_get_date_range($strDateFrom,$strDateTo) {
	// takes two dates formatted as YYYY-MM-DD and creates an
	// inclusive array of the dates between the from and to dates.

	$aryRange=array();

	$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	if ($iDateTo>=$iDateFrom)
	{
		array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
		while ($iDateFrom<$iDateTo)
		{
			$iDateFrom+=86400; // add 24 hours
			array_push($aryRange,date('Y-m-d',$iDateFrom));
		}
	}
	return $aryRange;
}


// convert column number to column letter
// http://www.tiny-threads.com/blog/2013/11/12/php-convert-number-excel-column-letter-2/
function wpaesm_get_col_letter($num){
	$comp=0;
	$pre='';
	$letters=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

	//if the number is greater than 26, calculate to get the next letters
	if($num > 26){
		//divide the number by 26 and get rid of the decimal
		$comp=floor($num/26);

		//add the letter to the end of the result and return it
		if($comp!=0)
			// don't subtract 1 if the comparative variable is greater than 0
			return wpaesm_get_col_letter($comp).$letters[($num-$comp*26)];
		else
			return wpaesm_get_col_letter($comp).$letters[($num-$comp*26)-1];
	}
	else
		//return the letter
		return $letters[($num-1)];
}


?>