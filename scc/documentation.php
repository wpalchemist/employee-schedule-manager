<?php  // Documentation

if ( ! defined( 'ABSPATH' ) ) exit;

// Shortcode to display shifts that need documents
function wpaesm_documents_to_do_shortcode() {
	// enqueue JS
	wp_enqueue_script( 'wpaesm_docajax', plugins_url() . '/employee-schedule-manager/js/docajax.js', 'jQuery' );
	wp_localize_script( 'wpaesm_docajax', 'wpaesmajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	$to_do = '';

	$today = date( 'Y-m-d', current_time("timestamp") );

	// find all shifts assigned to this employee in the past
	$args = array(
		'post_type' => 'shift',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'shift_type',
				'field' => 'slug',
				'terms' => array( 'respite', 'school-shift' ),
				'include_children' => true,
				'operator' => 'IN'
			),
			array(
				'taxonomy' => 'shift_status',
				'field' => 'slug',
				'terms' => array( 'worked' ),
				'include_children' => true,
				'operator' => 'IN'
			),
		),
		'meta_key' => '_wpaesm_date',
		'orderby' => 'meta_value',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => '_wpaesm_doc_approved',
				'compare' => 'NOT EXISTS',
			),
			array(
				'key' => '_wpaesm_date',
				'value' => $today,
				'compare' => '<=',
			),
		),
		'connected_type' => 'shifts_to_employees',
		'connected_items' => get_current_user_id(),
	);

	$no_docs = new WP_Query( $args );

	// The Loop
	if ( $no_docs->have_posts() ) {
		while ( $no_docs->have_posts() ) : $no_docs->the_post();
			$shiftid = get_the_id();
			global $shift_metabox; // get metabox data
			$shiftmeta = $shift_metabox->the_meta();
			// get client associated with this shift
			$clients = new WP_Query( array(
				'connected_type' => 'shifts_to_clients',
				'connected_items' => get_the_id(),
				'nopaging' => true,
			) );
			$clientname = 'No client';
			if ( $clients->have_posts() ) :
				while ( $clients->have_posts() ) : $clients->the_post();
					$clientid = get_the_id();
					$clientname = get_the_title();
					global $client_details;
					$clientmeta = $client_details->the_meta();
					if( isset( $clientmeta['goal'] ) ) {
						$goals = $clientmeta['goal'];
					}
				endwhile;
				wp_reset_postdata();
			endif;

			// The form
			$to_do .= '
				<fieldset id="shift-' . $shiftid . '">
				<legend>Documentation for shift with ' . $clientname . ' on ' . date( "D M j", strtotime( $shiftmeta['date'] ) ) . '</legend>
				<p>' . wpaesm_doc_due_date( $shiftmeta['date'] ) . '</p>
				<form id="shift-' . $shiftid . '" class="save-document-form">
					<input type="hidden" name="clientid" value="' . $clientid . '">
					<input type="hidden" name="clientname" value="' . $clientname . '">
					<input type="hidden" name="employeeid" value="' . get_current_user_id() . '">
					<input type="hidden" name="shiftid" value="' . $shiftid . '">
					<input type="hidden" name="date" value="' . $shiftmeta['date'] . '">
					<input type="hidden" name="wpaesm_create_document_nonce" value="' . wp_create_nonce( 'wpaesm_create_document_nonce' ) . '">
					<label><strong>Notes: RIPE format and narrative</strong></label>
						<textarea name="notes" id="notes"></textarea>
					<label><strong>Behavior Ratings</strong></label>';
			$i = 1;
			if( !empty( $goals ) ) {
				foreach( $goals as $goal ) {
					if( isset( $goal['active'] ) && $goal['active'] == 'active' ) {
						$to_do .= '
								<p>
									<label class="goal">' . $goal['goal_name'] . '</label>
									<input type="hidden" name="goals[' . $i . '][name]" value="' . $goal['goal_name'] . '">
									<input type="radio" name="goals[' . $i . '][rank]" value="0">NA</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="1">1</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="2">2</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="3">3</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="4">4</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="5">5</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="6">6</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="7">7</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="8">8</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="9">9</input>
									<input type="radio" name="goals[' . $i . '][rank]" value="10">10</input>
								</p>';
						$i++;
					}
				}
			} else {
				$to_do .= '<p>' . __( 'No behavior goals have been set for this client.', 'wpaesm' ) . '</p>';
			}
			unset( $goals );
			$to_do .= '
				<input type="submit" class="save-document" id="shift-' . $shiftid . '" value="' . __( 'Save Document', 'wpaesm' ) . '">
				</form>
				<div class="result"></div>
				</fieldset>';
		endwhile;
	} else {
		$to_do .= '<p>' . __( 'You have turned in all of your documentation!', 'wpaesm' ) . '</p>';
	}

	// Reset Post Data
	wp_reset_postdata();

	return $to_do;
}
add_shortcode( 'documents_to_do', 'wpaesm_documents_to_do_shortcode' );


// Shortcode to display unapproved documents (needs work and pending review)
function wpaesm_unapproved_docs_shortcode() {
	wp_enqueue_script( 'wpaesm_docajax', plugins_url() . '/employee-schedule-manager/js/docajax.js', 'jQuery' );
	wp_localize_script( 'wpaesm_docajax', 'wpaesmajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	$to_do = '<div id="documentation">';

	// find all unapproved documents associated with this employee
	$args = array(
		'post_type' => 'document',
		'tax_query' => array(
			array(
				'taxonomy' => 'doc_status',
				'field' => 'slug',
				'terms' => array( 'needs-work', 'pending-review' ),
				'include_children' => true,
				'operator' => 'IN'
			),
		),
		'posts_per_page' => -1,
		'connected_type' => 'documents_to_employees',
		'connected_items' => get_current_user_id(),
		'orderby' => 'title',
		'order' => 'ASC',
	);

	$pending_docs = new WP_Query( $args );

	// The Loop
	if ( $pending_docs->have_posts() ) {
		while ( $pending_docs->have_posts() ) : $pending_docs->the_post();
			$docid = get_the_id();
			global $document_metabox;
			$docmeta = $document_metabox->the_meta();
			if( !empty( $docmeta['ratings'] ) ) {
				$ratings = $docmeta['ratings'];
			}

			// get client associated with this shift
			$clients = new WP_Query( array(
				'connected_type' => 'documents_to_clients',
				'connected_items' => $docid,
				'nopaging' => true,
			) );
			$clientname = 'No client';
			if ( $clients->have_posts() ) :
				while ( $clients->have_posts() ) : $clients->the_post();
					$clientid = get_the_id();
					$clientname = get_the_title();
					global $client_details;
					$clientmeta = $client_details->the_meta();
					if( isset( $clientmeta['goal'] ) ) {
						$goals = $clientmeta['goal'];
					}
				endwhile;
				wp_reset_postdata();
			endif;

			// get the status
			$doc_status = wp_get_post_terms( $docid, 'doc_status', array( "fields" => "all" ) );
			$status_list = '';
			if( !empty( $doc_status ) ) {
				foreach( $doc_status as $status ) {
					$status_list .= '<span class="' . $status->slug . '">' . $status->name . '</span>';
				}
			}

			// The form
			$to_do .= '
				<fieldset id="doc-' . $docid . '">
				<legend>' . get_the_title( $docid ) . '</legend>
				<form id="doc-' . $docid . '" class="update-document-form">
					<p>Status: ' . $status_list . '</p>';
			// show feedback, if any
			$feedback = get_post_meta( $docid, '_wpaesm_feedback', true );
			if( isset( $feedback ) && '' !== $feedback ) {
				$to_do .= '<p class="feedback"><strong>Feedback:</strong><br />' . $feedback . '</p>';
			}
			$to_do .= '
					<input type="hidden" name="docid" value="' . $docid . '">
					<input type="hidden" name="wpaesm_update_document_nonce" value="' . wp_create_nonce( 'wpaesm_update_document_nonce' ) . '">
					<label><strong>Notes: RIPE format and narrative</strong></label>
						<textarea name="notes" id="notes">' . get_post_field( 'post_content', $docid ) . '</textarea>
					<label><strong>Behavior Ratings</strong></label>';
			$i = 1;
			if( !empty( $goals ) ) {
				foreach( $goals as $goal ) {
					$oldscore = array();
					if( isset( $goal['active'] ) && $goal['active'] == 'active' ) {
						// check existing ratings to see if we need to pre-populate the form
						if( !empty( $ratings ) ) {
							foreach( $ratings as $rating ) {
								if( $rating['goal'] == $goal['goal_name'] ) {
									// create an array of the values so we can stick them in where needed
									$oldscore[$rating['score']] = true;
								}
							}
						}
						$to_do .= '
								<p>
									<label class="goal">' . $goal['goal_name'] . '</label>
									<input type="hidden" name="goals[' . $i . '][name]" value="' . $goal['goal_name'] . '">
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[0]) ? ' checked ' : '') . 'value="0">NA</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[1]) ? ' checked ' : '') . 'value="1">1</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[2]) ? ' checked ' : '') . 'value="2">2</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[3]) ? ' checked ' : '') . 'value="3">3</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[4]) ? ' checked ' : '') . 'value="4">4</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[5]) ? ' checked ' : '') . 'value="5">5</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[6]) ? ' checked ' : '') . 'value="6">6</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[7]) ? ' checked ' : '') . 'value="7">7</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[8]) ? ' checked ' : '') . 'value="8">8</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[9]) ? ' checked ' : '') . 'value="9">9</input>
									<input type="radio" name="goals[' . $i . '][rank]"' . (isset($oldscore[10]) ? ' checked ' : '') . 'value="10">10</input>
								</p>';
						$i++;
					}
					unset($oldscore);
				}
			} else {
				$to_do .= '<p>' . __( 'No behavior goals have been set for this client.', 'wpaesm' ) . '</p>';
			}
			unset( $goals );
			$to_do .= '
				<input type="submit" class="update-document" id="doc-' . $docid . '" value="' . __( 'Update Document', 'wpaesm' ) . '">
				</form>
				<div class="result"></div>
				</fieldset>';
		endwhile;
	} else {
		$to_do .= '<p>' . __( 'You don\'t have any unapproved documentation!', 'wpaesm' ) . '</p>';
	}

	// Reset Post Data
	wp_reset_postdata();

	$to_do .= '</div>';

	return $to_do;
}
add_shortcode( 'unapproved_docs', 'wpaesm_unapproved_docs_shortcode' );


// Shortcode to display approved documents (not editable)
function wpaesm_approved_docs_shortcode() {
	$to_do = '';

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;

	// find all approved documents associated with this employee
	$args = array(
		'post_type' => 'document',
		'tax_query' => array(
			array(
				'taxonomy' => 'doc_status',
				'field' => 'slug',
				'terms' => array( 'approved' ),
				'include_children' => true,
				'operator' => 'IN'
			),
		),
		'posts_per_page' => 25,
		'paged' => $paged,
		'connected_type' => 'documents_to_employees',
		'connected_items' => get_current_user_id(),
	);

	$approved_docs = new WP_Query( $args );

	// The Loop
	if ( $approved_docs->have_posts() ) {
		$to_do .= '<div id="docs">';
		while ( $approved_docs->have_posts() ) : $approved_docs->the_post();
			$docid = get_the_id();
			$content = get_the_content();
			global $document_metabox;
			$docmeta = $document_metabox->the_meta();
			if( isset( $docmeta['ratings'] ) ) {
				$ratings = $docmeta['ratings'];
			}

			$to_do .= '
			<div class="document">
				<p><strong>Client: </strong>';
			$clients = new WP_Query( array(
				'connected_type' => 'documents_to_clients',
				'connected_items' => $docid,
			) );
			if( $clients->have_posts() ) {
				while ( $clients->have_posts() ) : $clients->the_post();
					$to_do .= get_the_title();
				endwhile;
			}
			wp_reset_postdata();
			$to_do .= '
				</p>
			    <p><strong>Shift Date: </strong>';
			$shifts = new WP_Query( array(
				'connected_type' => 'documents_to_shifts',
				'connected_items' => $docid,
			) );
			if( $shifts->have_posts() ) {
				while ( $shifts->have_posts() ) : $shifts->the_post();
					$to_do .= get_post_meta( get_the_id(), '_wpaesm_date', true );
				endwhile;
			}
			wp_reset_postdata();
			$to_do .= '
				</p>
			    <div>
			    	<strong>Notes:</strong><br />' . $content . '
			    </div>';
			if( !empty( $ratings ) ) {
				$to_do .= '
			    	<p><strong>Behavior Goals: </strong>
			    		<ul>';
				foreach( $ratings as $rating ) {
					$to_do .= '<li>' . $rating['goal'] . ': ' . $rating['score'] . '</li>';
				}
				$to_do .= '
			    		</ul>
			    	</p>';
			}
			$to_do .= '</div>';
		endwhile;

		$total_pages = $approved_docs->max_num_pages;
		if ($total_pages > 1){
			$current_page = max(1, get_query_var('paged'));
			$to_do .= "<div class='pagination'>";
			$to_do .= paginate_links(array(
				'base' => get_pagenum_link(1) . '%_%',
				'format' => '/page/%#%',
				'current' => $current_page,
				'total' => $total_pages,
			));
			$to_do .= "</div>";
		}
	} else {
		$to_do .= '<p>' . __( 'You don\'t have any approved documentation!', 'wpaesm' ) . '</p>';
	}

	// Reset Post Data
	wp_reset_postdata();

	return $to_do;
}
add_shortcode( 'approved_docs', 'wpaesm_approved_docs_shortcode' );


// calculate document due date
function wpaesm_doc_due_date( $date ) {
	$duedate = strtotime( '+24 hours', strtotime( $date ) );

	if( $duedate > time() ) {
		$output = 'Due: ' . date( 'D M j', $duedate );
	} else {
		$output = '<span class="overdue">Due: ' . date( 'D M j', $duedate ) . '</span>';
	}

	return $output;
}


// create a new document
function wpaesm_create_new_document() {

	$post_data = array();
	parse_str($_POST['field_values'], $post_data);

	if ( !wp_verify_nonce( $post_data['wpaesm_create_document_nonce'], 'wpaesm_create_document_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// get employee info
	$employee = get_user_by( 'id', $post_data['employeeid'] );

	// Create new document
	$docinfo = array(
		'post_type'     => 'document',
		'post_title'    => $employee->user_nicename  . '\'s shift with ' . $post_data['clientname'] . ' on ' . $post_data['date'],
		'post_status'   => 'publish',
		'post_content'	=> sanitize_text_field( $post_data['notes'] ),
	);
	$document = wp_insert_post( $docinfo );

	// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
	$fields = array('_wpaesm_ratings');
	$str = $fields;
	update_post_meta( $document, 'document_meta_fields', $str );

	// Get all the goals and put them in the array format wpalchemy expects
	if( isset( $post_data['goals'] ) ) {
		foreach ( $post_data['goals'] as $goal ) {
			if( isset( $goal['rank'] ) )
				$goalfields[] = array(
					'goal' => sanitize_text_field( $goal['name'] ),
					'score' => sanitize_text_field( $goal['rank'] ),
				);
		}
	}

	// Add all of the post meta data in one fell swoop
	if( !empty( $goalfields ) ) {
		update_post_meta( $document, '_wpaesm_ratings', $goalfields );
	}

	// record the date the document was created
	$fields = array('_wpaesm_created', '_wpaesm_updated', '_wpaesm_approved', '_wpaesm_feedback');
	$str = $fields;
	update_post_meta( $document, 'document_history_fields', $str );
	update_post_meta( $document, '_wpaesm_created', current_time( 'Y-m-d' ) );

	// connect document to employee
	p2p_type( 'documents_to_employees' )->connect( $document, $post_data['employeeid'], array(
		'date' => current_time('mysql')
	) );
	// connect document to client
	p2p_type( 'documents_to_clients' )->connect( $document, $post_data['clientid'], array(
		'date' => current_time('mysql')
	) );
	// connect document to shift
	p2p_type( 'documents_to_shifts' )->connect( $document, $post_data['shiftid'], array(
		'date' => current_time('mysql')
	) );

	// give document "pending review" status
	$pending = get_term_by( 'slug', 'pending-review', 'doc_status' );
	wp_set_post_terms( $document, $pending->term_id, 'doc_status', 0 );

	// in shift metadata, show that there is a document pending review
	wpaesm_create_shift_serialized_array( $post_data['shiftid'] );
	update_post_meta( $post_data['shiftid'], '_wpaesm_doc_approved', 'pending' );

	// send an email, if that option is selected
	$options = get_option( 'wpaesm_options' );
	if( "1" == $options['admin_notify_doc_create'] ) {
		wpaesm_send_doc_notification( 'created', $document, $post_data['shiftid'] );
	}

	// return a result
	if( $document ) {
		$results = '<p class="success">Your document was successfully saved! <a href="' . get_the_permalink( $post_data['shiftid'] ) . '" target="_blank">View Shift ></a></p>';
	} else {
		$results = '<p class="failure">There was an error saving your document.</p>';
	}

	die( $results );

}
add_action( 'wp_ajax_nopriv_wpaesm_create_new_document', 'wpaesm_create_new_document' );
add_action( 'wp_ajax_wpaesm_create_new_document', 'wpaesm_create_new_document' );

// update document
function wpaesm_update_document() {
	$post_data = array();
	parse_str($_POST['field_values'], $post_data);

	if ( !wp_verify_nonce( $post_data['wpaesm_update_document_nonce'], 'wpaesm_update_document_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// update document
	$docinfo = array(
		'ID'     => $post_data['docid'],
		'post_content'	=> sanitize_text_field( $post_data['notes'] ),
	);
	wp_update_post( $docinfo );

	// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
	$fields = array('_wpaesm_ratings');
	$str = $fields;
	update_post_meta( $post_data['docid'], 'document_meta_fields', $str );

	// Get all the goals and put them in the array format wpalchemy expects
	if( isset( $post_data['goals'] ) ) {
		foreach ( $post_data['goals'] as $goal ) {
			if( isset( $goal['rank'] ) )
				$goalfields[] = array(
					'goal' => sanitize_text_field( $goal['name'] ),
					'score' => sanitize_text_field( $goal['rank'] ),
				);
		}
	}

	// Add all of the post meta data in one fell swoop
	if( !empty( $goalfields ) ) {
		update_post_meta( $post_data['docid'], '_wpaesm_ratings', $goalfields );
	}

	// give document "pending review" status
	$pending = get_term_by( 'slug', 'pending-review', 'doc_status' );
	wp_set_post_terms( $post_data['docid'], $pending->term_id, 'doc_status', 0 );

	// in shift metadata, show that there is a document pending review
	$connected = get_posts( array(
		'connected_type' => 'documents_to_shifts',
		'connected_items' => $post_data['docid'],
		'nopaging' => true,
		'suppress_filters' => false
	) );
	foreach( $connected as $shift ) {
		$shiftid = $shift->ID;
	}
	wpaesm_create_shift_serialized_array( $shiftid );
	update_post_meta( $shiftid, '_wpaesm_doc_approved', 'updated-pending' );

	// record the date the document was created
	$fields = array('_wpaesm_created', '_wpaesm_updated', '_wpaesm_approved', '_wpaesm_feedback');
	$str = $fields;
	update_post_meta( $post_data['docid'], 'document_history_fields', $str );
	update_post_meta( $post_data['docid'], '_wpaesm_updated', current_time( 'Y-m-d' ) );

	// send an email, if that option is selected
	$options = get_option( 'wpaesm_options' );
	if( "1" == $options['admin_notify_doc_create'] ) {
		wpaesm_send_doc_notification( 'updated', $post_data['docid'], $shiftid );
	}

	// return a result
	$results = '<p class="success">Your document was successfully saved! <a href="' . get_the_permalink( $shiftid ) . '" target="_blank">View Shift ></a></p>';

	die( $results );
}
add_action( 'wp_ajax_nopriv_wpaesm_update_document', 'wpaesm_update_document' );
add_action( 'wp_ajax_wpaesm_update_document', 'wpaesm_update_document' );

function wpaesm_send_doc_notification( $action, $docid, $shiftid ) {
	$options = get_option( 'wpaesm_options' );

	// get employee
	$users = get_users( array(
		'connected_type' => 'documents_to_employees',
		'connected_items' => $docid
	) );
	foreach( $users as $user ) {
		$employee_name = $user->display_name;
	}

	$from = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
	$to = $options['admin_notification_email'];
	$subject = $employee_name . ' has just ' . $action . ' a document';

	$message_text = '
	<p>' . $employee_name . ' has just ' . $action . ' a document</p>
	<ul>
		<li>
			<a href="' . get_edit_post_link( $docid ) . '">Edit document</a>
		</li>
		<li>
			<a href="' . get_permalink( $shiftid ) . '">View shift</a>
		</li>
		<li>
			<a href="' . admin_url( 'edit.php?post_type=document&page=unapproved-documents' ) . '">See all unapproved documents</a>
		</li>
	</ul>';

	wpaesm_send_email( $from, $to, '', $subject, $message_text, '' );
}

// Create page to show unapproved documents
function wpaesm_unapproved_documents_page() { ?>

	<h1><?php _e( 'Unapproved Documents', 'wpaesm' ); ?></h1>

	<?php
	$args = array(
		'post_type' => 'document',
		'tax_query' => array(
			array(
				'taxonomy' => 'doc_status',
				'field' => 'slug',
				'terms' => 'approved',
				'include_children' => false,
				'operator' => 'NOT IN'
			)
		),
		'posts_per_page' => -1,
		'order' => 'ASC',
	);

	$unapproved = new WP_Query( $args );

	// The Loop
	if ( $unapproved->have_posts() ) : ?>
		<table id="filtered-shifts" class="wp-list-table widefat fixed posts striped">
			<thead>
			<tr>
				<th data-sort='string'><span><?php _e( 'Shift', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Employee', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Client', 'wpaesm'); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Status', 'wpaesm' ); ?></span></th>
				<th data-sort='string' width="50%"><span><?php _e( 'Document', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Actions', 'wpaesm' ); ?></span></th>
			</tr>
			</thead>
			<tbody>
			<?php while ( $unapproved->have_posts() ) : $unapproved->the_post();
				// gather all the information we need to display
				$docid = get_the_id();
				$content = get_the_content();
				global $document_metabox;
				$docmeta = $document_metabox->the_meta();
				if( isset( $docmeta['ratings'] ) ) {
					$ratings = $docmeta['ratings'];
				}
				$edit_doc = get_edit_post_link();

				// get the client
				$client_name = '';
				$clients = new WP_Query( array(
					'connected_type' => 'documents_to_clients',
					'connected_items' => $docid,
				) );
				if( $clients->have_posts() ) {
					while ( $clients->have_posts() ) : $clients->the_post();
						$client_name = get_the_title();
					endwhile;
				}
				wp_reset_postdata();

				// get the shift date
				$shifts = new WP_Query( array(
					'connected_type' => 'documents_to_shifts',
					'connected_items' => $docid,
				) );
				if( $shifts->have_posts() ) {
					while ( $shifts->have_posts() ) : $shifts->the_post();
						$shift_id = get_the_id();
						$shift_date = get_post_meta( get_the_id(), '_wpaesm_date', true );
						$shift_url = get_the_permalink();
						$shift_edit = get_edit_post_link();
					endwhile;
				}
				wp_reset_postdata();

				// get employee
				$users = get_users( array(
					'connected_type' => 'documents_to_employees',
					'connected_items' => $docid
				) );
				foreach( $users as $user ) {
					$employee_name = $user->display_name;
					$employee_email = $user->user_email;
				}

				// get status
				$doc_status = wp_get_post_terms( $docid, 'doc_status', array( "fields" => "all" ) );
				$status_list = '';
				if( !empty( $doc_status ) ) {
					foreach( $doc_status as $status ) {
						$status_list .= '<span class="' . $status->slug . '">' . $status->name . '</span>';
					}
				}

				?>
				<tr id="doc-<?php echo $docid; ?>">
					<td class="shift">
						<strong>Shift date:</strong> <?php echo $shift_date; ?><br />
						<a href="<?php echo $shift_url; ?>" target="_blank">View Shift</a> |
						<a href="<?php echo $shift_edit; ?>" target="_blank">Edit Shift</a>
					</td>
					<td class="employee">
						<?php echo $employee_name; ?>
					</td>
					<td class="client">
						<?php echo $client_name; ?>
					</td>
					<td class="status">
						<?php echo $status_list; ?>
					</td>
					<td class="document">
						<p><strong>Notes:</strong><br />
							<?php echo $content; ?>
						</p>
						<?php if( !empty( $ratings ) ) { ?>
							<p><strong>Behavior Goals: </strong><br />
								<?php foreach( $ratings as $rating ) { ?>
									<?php echo $rating['goal'] . ': ' . $rating['score']; ?><br />
								<?php } ?>
							</p>
						<?php } ?>
						<p>
							<a href="<?php echo $edit_doc; ?>" target="_blank">Edit Document</a>
						</p>
					</td>
					<td class="actions">
						<form id="approve-doc-<?php echo $docid; ?>" class="approve-document-form">
							<input type="hidden" name="docid" value="<?php echo $docid; ?>">
							<input type="hidden" name="shiftid" value="<?php echo $shift_id; ?>">
							<input type="hidden" name="wpaesm_approve_document_nonce" value="<?php echo wp_create_nonce( 'wpaesm_approve_document_nonce' ); ?>">
							<input type="submit" value="Approve" class="button-primary">
						</form>
						<p class="needs-work-button"><a href="#" class="button show-needs-work-form">Needs Work</a></p>
						<form class="needs-work-form" style="display:none;">
							<label>Feedback</label>
							<textarea class="doc-feedback" name="doc-feedback" rows="15"></textarea>
							<input type="hidden" name="docid" value="<?php echo $docid; ?>">
							<input type="hidden" name="shiftid" value="<?php echo $shift_id; ?>">
							<input type="hidden" name="employee-email" value="<?php echo $employee_email; ?>">
							<input type="hidden" name="wpaesm_document_feedback_nonce" value="<?php echo wp_create_nonce( 'wpaesm_document_feedback_nonce' ); ?>">
							<input type="submit" value="Email Feedback" class="button-primary">
						</form>
						<div class="response"></div>
					</td>
				</tr>
			<?php endwhile; ?>
			</tbody>
		</table>
	<?php endif;

	// Reset Post Data
	wp_reset_postdata();
}

function wpaesm_approve_document() {
	$post_data = array();
	parse_str( $_POST['field_values'], $post_data );

	if ( !wp_verify_nonce( $post_data['wpaesm_approve_document_nonce'], 'wpaesm_approve_document_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// give document "approved" status
	$approved = get_term_by( 'slug', 'approved', 'doc_status' );
	wp_set_post_terms( $post_data['docid'], $approved->term_id, 'doc_status', 0 );

	// in shift metadata, show that there is an approved document
	wpaesm_create_shift_serialized_array( $post_data['shiftid'] );
	update_post_meta( $post_data['shiftid'], '_wpaesm_doc_approved', 'approved' );

	// record the date the document was approved
	$fields = array('_wpaesm_created', '_wpaesm_updated', '_wpaesm_approved', '_wpaesm_feedback');
	$str = $fields;
	update_post_meta( $post_data['docid'], 'document_history_fields', $str );
	update_post_meta( $post_data['docid'], '_wpaesm_approved', current_time( 'Y-m-d' ) );

	// return a result
	$results = '<p class="success">Document Approved!</p>';

	die( $results );
}
add_action( 'wp_ajax_nopriv_wpaesm_approve_document', 'wpaesm_approve_document' );
add_action( 'wp_ajax_wpaesm_approve_document', 'wpaesm_approve_document' );


function wpaesm_send_document_feedback() {
	$post_data = array();
	parse_str( $_POST['field_values'], $post_data );

	if ( !wp_verify_nonce( $post_data['wpaesm_document_feedback_nonce'], 'wpaesm_document_feedback_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// give document "needs work" status
	$needs_work = get_term_by( 'slug', 'needs-work', 'doc_status' );
	wp_set_post_terms( $post_data['docid'], $needs_work->term_id, 'doc_status', 0 );

	// record the feedback
	$fields = array('_wpaesm_created', '_wpaesm_updated', '_wpaesm_approved', '_wpaesm_feedback');
	$str = $fields;
	update_post_meta( $post_data['docid'], 'document_history_fields', $str );
	update_post_meta( $post_data['docid'], '_wpaesm_feedback', sanitize_text_field( $post_data['doc-feedback'] ) );

	// email feedback to employee
	$options = get_option( 'wpaesm_options' );
	$from = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
	$to = $post_data['employee-email'];
	$subject = 'Feedback on your documentation';

	$message_text = '<p><strong>Feedback on the documentation for ' . get_the_title( $post_data['docid'] ) . '</strong></p>';
	$message_text .= '<p>' . sanitize_text_field( $post_data['doc-feedback'] ) . '</p>';
	$message_text .= '<p><a href="' . home_url() . '/your-profile/unapproved-documents">See all of your unapproved documents</a></p>';
	$message_text .= '<p><a href="' . get_the_permalink( $post_data['shiftid']) . '">See the details of this shift</a></p>';

	wpaesm_send_email( $from, $to, '', $subject, $message_text, '' );

	// return a result
	$results = '<p class="success">Feedback Sent!</p>';

	die( $results );
}
add_action( 'wp_ajax_nopriv_wpaesm_send_document_feedback', 'wpaesm_send_document_feedback' );
add_action( 'wp_ajax_wpaesm_send_document_feedback', 'wpaesm_send_document_feedback' );

function wpaesm_undocumented_shifts_page() { ?>

	<h1><?php _e( 'Shifts Without Documents', 'wpaesm' ); ?></h1>

	<?php $today = date( 'Y-m-d', current_time("timestamp") );

	// find all shifts in the past without documents
	$args = array(
		'post_type' => 'shift',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'shift_type',
				'field' => 'slug',
				'terms' => array( 'respite', 'school-shift' ),
				'include_children' => true,
				'operator' => 'IN'
			),
			array(
				'taxonomy' => 'shift_status',
				'field' => 'slug',
				'terms' => array( 'worked' ),
				'include_children' => true,
				'operator' => 'IN'
			),
		),
		'meta_key' => '_wpaesm_date',
		'orderby' => 'meta_value',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => '_wpaesm_doc_approved',
				'compare' => 'NOT EXISTS',
			),
			array(
				'key' => '_wpaesm_date',
				'value' => $today,
				'compare' => '<=',
			),
		),
	);

	$no_docs = new WP_Query( $args );

	// The Loop
	if ( $no_docs->have_posts() ) { ?>
		<table id="filtered-shifts" class="wp-list-table widefat fixed posts striped">
			<thead>
			<tr>
				<th data-sort='string'><span><?php _e( 'Shift', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Employee', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Client', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Shift Date', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Document Due Date', 'wpaesm' ); ?></span></th>
				<th data-sort='string'><span><?php _e( 'Reminder Date', 'wpaesm' ); ?></span></th>
				<th width="265px"><?php _e( 'Actions', 'wpaesm' ); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php while ( $no_docs->have_posts() ) : $no_docs->the_post();
				global $shift_metabox;
				$meta = $shift_metabox->the_meta();
				$shiftid = get_the_id();

				// get employee
				$employee_name = '';
				$employee_email = '';
				$users = get_users( array(
					'connected_type' => 'shifts_to_employees',
					'connected_items' => get_the_id(),
				) );
				foreach( $users as $user ) {
					$employee_name = $user->display_name;
					$employee_email = $user->user_email;
				}

				// get the client
				$clients = new WP_Query( array(
					'connected_type' => 'shifts_to_clients',
					'connected_items' => get_the_id(),
				) );
				if( $clients->have_posts() ) {
					while ( $clients->have_posts() ) : $clients->the_post();
						$client_name = get_the_title();
					endwhile;
				}
				wp_reset_postdata();
				?>

				<tr>
					<td class="shift">
						<a href="<?php echo get_the_permalink( $shiftid ); ?>" target="_blank"><?php echo get_the_title( $shiftid ); ?></a><br />
						<a href="<?php echo get_edit_post_link( $shiftid ); ?>" target="_blank">Edit Shift</a>
					</td>
					<td class="employee">
						<?php echo $employee_name; ?>
					</td>
					<td class="client">
						<?php echo $client_name; ?>
					</td>
					<td class="shift-date">
						<?php echo $meta['date']; ?>
					</td>
					<td class="doc-due-date">
						<?php echo wpaesm_doc_due_date( $meta['date'] ); ?>
					</td>
					<td class="doc-reminder">
						<?php if( isset( $meta['doc_reminder'] ) && '' !== $meta['doc_reminder'] ) {
							echo 'Email reminder last sent on: ' . $meta['doc_reminder'];
						} ?>
					</td>
					<td class="actions">
						<form id="doc-reminder-<?php echo $shiftid; ?>" class="doc-reminder-form">
							<input type="hidden" name="shiftid" value="<?php echo $shiftid; ?>">
							<input type="hidden" name="shift-date" value="<?php echo $meta['date']; ?>">
							<input type="hidden" name="employee-email" value="<?php echo $employee_email; ?>">
							<input type="hidden" name="wpaesm_doc_reminder_nonce" value="<?php echo wp_create_nonce( 'wpaesm_doc_reminder_nonce' ); ?>">
							<input type="submit" value="Email Reminder" class="button-primary doc-reminder">
						</form>
						<br />
						<form id="doc-unnecessary-<?php echo $shiftid; ?>" class="doc-unnecessary-form">
							<input type="hidden" name="shiftid" value="<?php echo $shiftid; ?>">
							<input type="hidden" name="wpaesm_doc_unnecessary_nonce" value="<?php echo wp_create_nonce( 'wpaesm_doc_unnecessary_nonce' ); ?>">
							<input type="submit" value="This Shift Doesn't Need Documents" class="button doc-unnecessary">
						</form>
						<div class="response"></div>
					</td>
				</tr>
			<?php endwhile; ?>
			</tbody>
		</table>
	<?php }
	wp_reset_postdata();
}

function wpaesm_send_document_reminder() {
	$post_data = array();
	parse_str( $_POST['field_values'], $post_data );

	if ( !wp_verify_nonce( $post_data['wpaesm_doc_reminder_nonce'], 'wpaesm_doc_reminder_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// email reminder to employee
	$options = get_option( 'wpaesm_options' );
	$from = $options['notification_from_name'] . " <" . $options['notification_from_email'] . ">";
	$to = $post_data['employee-email'];
	$subject = 'Documentation reminder';

	$message_text = '<p><strong>You need to complete your documentation for your shift on ' . $post_data['shift-date'] . '</strong></p>';
	$message_text .= '<p><a href="' . home_url() . '/your-profile/?tab=documentation">See all of your unfinished documents</a></p>';
	$message_text .= '<p><a href="' . get_the_permalink( $post_data['shiftid'] ) . '">See the details of this shift</a></p>';

	wpaesm_send_email( $from, $to, '', $subject, $message_text, '' );

	// in shift metadata, show that reminder was sent
	wpaesm_create_shift_serialized_array( $post_data['shiftid'] );
	update_post_meta( $post_data['shiftid'], '_wpaesm_doc_reminder', current_time( 'Y-m-d' ) );

	// return a result
	$results = '<p class="success">Email Reminder Sent!</p>';

	die( $results );
}
add_action( 'wp_ajax_nopriv_wpaesm_send_document_reminder', 'wpaesm_send_document_reminder' );
add_action( 'wp_ajax_wpaesm_send_document_reminder', 'wpaesm_send_document_reminder' );

function wpaesm_doc_unnecessary() {
	$post_data = array();
	parse_str( $_POST['field_values'], $post_data );

	if ( !wp_verify_nonce( $post_data['wpaesm_doc_unnecessary_nonce'], 'wpaesm_doc_unnecessary_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// in shift metadata, show that documents aren't needed
	wpaesm_create_shift_serialized_array( $post_data['shiftid'] );
	update_post_meta( $post_data['shiftid'], '_wpaesm_doc_approved', 'unnecessary' );


	// return a result
	$results = '<p class="success">Saved: this shift does not need documentation</p>
                <form id="undo-doc-unnecessary-' . $post_data['shiftid'] . '" class="undo-doc-unnecessary-form">
                    <input type="hidden" name="shiftid" value="' . $post_data['shiftid'] . '">
                    <input type="hidden" name="wpaesm_undo_doc_unnecessary_nonce" value="' . wp_create_nonce( 'wpaesm_undo_doc_unnecessary_nonce' ) . '">
                    <input type="submit" value="Undo" class="button doc-unnecessary">
                </form>';

	die( $results );
}
add_action( 'wp_ajax_nopriv_wpaesm_doc_unnecessary', 'wpaesm_doc_unnecessary' );
add_action( 'wp_ajax_wpaesm_doc_unnecessary', 'wpaesm_doc_unnecessary' );


function wpaesm_undo_doc_unnecessary() {
	$post_data = array();
	parse_str( $_POST['field_values'], $post_data );

	if ( !wp_verify_nonce( $post_data['wpaesm_undo_doc_unnecessary_nonce'], 'wpaesm_undo_doc_unnecessary_nonce' ) ) {
		exit( 'Permission Error.' );
	}

	// in shift metadata, show that documents aren't needed
	wpaesm_create_shift_serialized_array( $post_data['shiftid'] );
	delete_post_meta( $post_data['shiftid'], '_wpaesm_doc_approved' );


	// return a result
	$results = 'success';

	die( $results );
}
add_action( 'wp_ajax_nopriv_wpaesm_undo_doc_unnecessary', 'wpaesm_undo_doc_unnecessary' );
add_action( 'wp_ajax_wpaesm_undo_doc_unnecessary', 'wpaesm_undo_doc_unnecessary' );


function wpaesm_export_documents_page() { ?>

	<div class="wrap">

		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h2>Download Documents</h2>
		<p>Select the date range and client and download a Word document.</p>

		<form method='post' action='<?php echo admin_url( 'edit.php?post_type=document&page=export-documents' ); ?>' id='download-documents'>
			<table class="form-table">
				<tr>
					<th scope="row"><label>Date Range:</label></th>
					<td>
						From <input type="text" size="10" name="thisdate" id="thisdate" value="" required /> to <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" required />
						<p>Note: this is the date the document was created, not the date of the documented shift</p>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e('Client:', 'wpaesm'); ?></th>
					<td>
						<?php $args = array(
							'post_type' => 'client',
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order' => 'asc',
						);

						$clientquery = new WP_Query( $args );

						if ( $clientquery->have_posts() ) : ?>
							<select name="client" required>
								<option value=""></option>
								<?php while ( $clientquery->have_posts() ) : $clientquery->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endwhile; ?>
							</select>
						<?php endif;
						wp_reset_postdata(); ?>
					</td>
				</tr>
			</table>
			<p class="submit">
				<input type="hidden" name="wpaesm_download_document_doc" value="wpaesm_download_document_doc">
				<input type="submit" class="button-primary" value="<?php _e( 'Download Documents', 'wpaesm' ); ?>" />
			</p>
		</form>

	</div>
	<?php
}

add_action( 'admin_init', 'wpaesm_download_documents' );
function wpaesm_download_documents() {

	if( isset( $_POST['wpaesm_download_document_doc'] ) && 'wpaesm_download_document_doc' == $_POST['wpaesm_download_document_doc'] ) {

		if( $_POST ) {
			if( ( $_POST['thisdate'] == '____-__-__' ) || ( $_POST['repeatuntil'] == '____-__-__' ) ) {
				wp_die( __( 'You must enter both a start date and an end date to create a report.', 'wpaesm' ) );
			} elseif( $_POST['thisdate'] > $_POST['repeatuntil'] ) {
				wp_die( __( 'The report end date must be after the report begin date.', 'wpaesm' ) );
			} elseif( !isset( $_POST['client'] ) || '' == $_POST['client'] ) {
				wp_die( __( 'You must select a client.', 'wpaesm' ) );
			}
		}



		$client = get_post( $_POST['client'] );
		$clientname = $client->post_title;
		wp_reset_postdata();



		$args = array(
			'post_type'      => 'document',
			'posts_per_page' => - 1,
			'order'          => 'ASC',
			'date_query'     => array(
				array(
					'after'     => $_POST['thisdate'],
					'before'    => $_POST['repeatuntil'],
					'inclusive' => true,
				),
			),
		);

		if ( isset( $_POST['client'] ) && '' !== $_POST['client'] ) {
			$args['connected_type']  = 'documents_to_clients';
			$args['connected_items'] = $_POST['client'];
		}

		$the_query = new WP_Query( $args );

		// The Loop
		if ( $the_query->have_posts() ) {

			header( 'Content-type: application/vnd.ms-word' );
			header( 'Content-Disposition: attachment;Filename=documentation.doc' );

			echo '<html>
				<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
				<body>
					<h1>Documentation for ' . $clientname . ' between ' . $_POST['thisdate'] . ' and ' . $_POST['repeatuntil'] . '</h1>';

			while ( $the_query->have_posts() ) : $the_query->the_post();

				echo '<h3>' . get_the_title() . '</h3>';

				$docid = get_the_id();
				$content = get_the_content();
				global $document_metabox;
				$docmeta = $document_metabox->the_meta();

				$employees = get_users( array(
					'connected_type' => 'documents_to_employees',
					'connected_items' => $docid,
				) );
				if( !empty( $employees ) ) {
					foreach( $employees as $employee ) {
						$this_employee = $employee->display_name;
					}
					echo '<p><strong>Employee: </strong>' . $this_employee . '</p>';
				}

				$shifts = new WP_Query( array(
					'connected_type' => 'documents_to_shifts',
					'connected_items' => $docid,
				) );
				if( $shifts->have_posts() ) {
					while ( $shifts->have_posts() ) : $shifts->the_post();
						echo '<p><strong>Shift Date: </strong>' . get_post_meta( get_the_id(), '_wpaesm_date', true ) . '</p>';
					endwhile;
				}
				wp_reset_postdata();

				echo '<p><strong>Notes:</strong><br />' . $content . '</p>';


				if( isset( $docmeta['ratings'] ) ) {
					$ratings = $docmeta['ratings'];
					if( !empty( $ratings) ) {
						echo '<p><strong>Behavior Goals: </strong><ul>';
						foreach( $ratings as $rating ) {
							echo '<li>' . $rating['goal'] . ': ' . $rating['score'] . '</li>';
						}
						echo '</ul></p>';
					}
				}

			endwhile;

			echo '</body>
			</html>';
			die();
		} else {
			wp_die( 'No documents were found' );
		};

		// Reset Post Data
		wp_reset_postdata();


	}
}

/**
 * Schedule cron job to check for late documentation
 *
 * @since 2.2.0
 */
function wpaesm_look_for_late_documentation() {
	if ( ! wp_next_scheduled( 'wpaesm_late_docs_message' ) ) {
		wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'wpaesm_late_docs_message' );
	}
}
add_action( 'init', 'wpaesm_look_for_late_documentation' );

add_action( 'wpaesm_late_docs_message', 'wpaesm_find_late_docs' );

/**
 * Look for shifts that need a text reminder to do documentation
 */
function wpaesm_find_late_docs() {
	$yesterday = date( "Y-m-d", strtotime( '- 1 day', current_time( "timestamp" ) ) );

	$args = array(
		'post_type' => 'shift',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'shift_type',
				'field' => 'slug',
				'terms' => array( 'respite', 'school-shift' ),
				'include_children' => true,
				'operator' => 'IN'
			),
			array(
				'taxonomy' => 'shift_status',
				'field' => 'slug',
				'terms' => array( 'worked' ),
				'include_children' => true,
				'operator' => 'IN'
			),
		),
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => '_wpaesm_doc_approved',
				'compare' => 'NOT EXISTS',
			),
            array(
                'key' => '_wpaesm_late_docs_notification',
                'compare' => 'NOT EXISTS'
            ),
			array(
				'key' => '_wpaesm_date',
				'value' => $yesterday,
				'compare' => '<=',
			),
		),
	);

	$missing_docs = new WP_Query( $args );

	// The Loop
	if ( $missing_docs->have_posts() ) {
		while ( $missing_docs->have_posts() ) : $missing_docs->the_post();

			// check to see if there are documents associated with this shift
            $docs = get_posts( array(
	            'connected_type' => 'documents_to_shifts',
	            'connected_items' => get_the_id(),
	            'nopaging' => true,
	            'suppress_filters' => false
            ) );

            if( !is_array( $docs ) || empty( $docs ) ) {
                // check if the shift ended more than 24 hours ago
                $shift_end = strtotime( get_post_meta( get_the_id(), '_wpaesm_date', true ) . get_post_meta( get_the_id(), '_wpaesm_endtime', true ) );
	            $now = current_time( 'timestamp' );

	            if( ( $now - $shift_end ) > 86400 ) {
		            wpaesm_text_late_docs_notification( get_the_id() );
                }

            }

		endwhile;
	}

	wp_reset_postdata();

}

/**
 * Send text notification
 *
 * @param $shift
 * @param $in_or_out
 */
function wpaesm_text_late_docs_notification( $shift ) {

	$users = get_users( array(
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $shift
	) );
	foreach( $users as $user ) {
		$recipient = $user->ID;
	}

	$message = 'You have overdue shift documentation.  Shift details: ' . get_the_permalink( $shift );

	$text = new Shiftee_Text_Notifications_Admin( 'Text Messaging', '1.0.0' );
	$text->send_text_message( $recipient, $message );

	add_post_meta( $shift, '_wpaesm_late_docs_notification', current_time( 'timestamp' ) );

}

?>