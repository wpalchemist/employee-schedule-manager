<?php  // Incident Report Form

if ( ! defined( 'ABSPATH' ) ) exit;



/**
 * Make a dropdown list of all non-archived clients
 *
 * @param $form
 *
 * @return mixed
 */
function scc_populate_client_dropdown( $form ) {

	foreach ( $form['fields'] as &$field ) {

		if ( $field->type != 'select' || strpos( $field->cssClass, 'client-names' ) === false ) {
			continue;
		}

		$args = array(
			'numberposts' => -1,
			'post_type' => 'client',
			'tax_query' => array(
				array(
					'taxonomy' => 'client_category',
					'field'    => 'slug',
					'terms'    => 'archived',
					'operator' => 'NOT IN',
				),
			),
			'orderby' => 'title',
			'order' => 'ASC',
		);

		$posts = get_posts( $args );

		$choices = array();

		foreach ( $posts as $post ) {
			$choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
		}

		$field->placeholder = 'Select the Client\'s Name';
		$field->choices = $choices;

	}

	return $form;
}
add_filter( 'gform_pre_render_3', 'scc_populate_client_dropdown' );
add_filter( 'gform_pre_validation_3', 'scc_populate_client_dropdown' );
add_filter( 'gform_pre_submission_filter_3', 'scc_populate_client_dropdown' );
add_filter( 'gform_admin_pre_render_3', 'scc_populate_client_dropdown' );


/**
 * When the incident report form is submitted through Gravity Forms, create a Word doc of the entry
 *
 * @param $entry
 * @param $form
 *
 * @link http://sebsauvage.net/wiki/doku.php?id=word_document_generation
 */
function scc_save_word_doc( $entry, $form ) {

	$uploads = wp_upload_dir();
	$filename = $entry[25] . '-' . $entry[2] . '.doc';
	$file = $uploads['basedir'] . "/incident-reports/" . $filename;
	$file_link = $uploads['baseurl'] . "/incident-reports/" . $filename;

	$handle = fopen( $file, 'w') or die('Cannot open file:  '.$file );

	$file_contents = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>
		<head><title>Incident Report</title>
		<style><!-- 
		@page
		{
		    size:21cm 29.7cm;  /* A4 */
		    margin:1cm 1cm 1cm 1cm; /* Margins: 2.5 cm on each side */
		    mso-page-orientation: portrait;  
		}
		@page Section1 { }
		div.Section1 { page:Section1; }
		--></style>
		</head>
		<body>
		<div class=Section1>
		<h1>Incident Report</h1>
		<h2>Submitted at " . $entry['date_created'] . "</h2>
		" . scc_parse_fields( $entry, $form ) . "
		</div>
		</body>
		</html>";

	fwrite( $handle, $file_contents );
	fclose( $handle );

	scc_save_report_to_client( $file_link, $entry );
	scc_save_report_to_employee( $file_link, $entry );

}
add_action( 'gform_after_submission_3', 'scc_save_word_doc', 10, 2 );


function scc_parse_fields( $entry, $form ) {

	$fields_array = array();

	foreach( $form['fields'] as $field ) {

		if( 'checkbox' == $field->type ) {
			$i = 1;
			foreach( $field->choices as $choice ) {
				$fields_array[] = array(
					'id' => $field->id . '.' . $i,
					'label' => $field->label,
				);
				$i++;
			}
		}  else {

			$fields_array[] = array(
				'id' => $field->id,
				'label' => $field->label,
			);

		}
	}

	$report_details = '';


	foreach( $fields_array as $form_field ) {
		if( isset( $entry[$form_field['id']] ) && '' !== $entry[$form_field['id']] ) {
			$answer = $entry[$form_field['id']];
			if( is_serialized( $answer ) ) {
				$items = unserialize( $answer );
				$answer = '<ul>';
				foreach( $items as $item ) {
					$answer .= '<li>' . $item . '</li>';
				}
				$answer .= '</ul>';
			}
			$report_details .= '<p><b>' . $form_field['label'] . '</b><br />' . $answer . '</p>';
		}
	}

	return $report_details;

}

function scc_save_report_to_client( $file_link, $entry ) {

	$clients = get_posts( array(
		'title' => $entry[25],
		'post_type' => 'client'
	) );

	if( is_array( $clients ) && !empty( $clients ) ) {
		foreach( $clients as $client ) {
			add_post_meta( $client->ID, '_wpaesm_incident_reports', $file_link );
		}
	}
}

function scc_save_report_to_employee( $file_link, $entry ) {

	$employee = get_user_by( 'login', $entry[24] );

	if( $employee ) {
		$existing_reports = get_user_meta( $employee->ID, '_wpaesm_incident_reports', true );
		if( !is_array( $existing_reports ) ) {
			$existing_reports = array();
		}

		$existing_reports[] = $file_link;
		add_user_meta( $employee->ID, '_wpaesm_incident_reports', $existing_reports );
	}

}

add_action( 'show_user_profile', 'scc_show_employee_incident_reports' );

function scc_show_employee_incident_reports( $user ) { ?>
	<h3>Incident Reports</h3>

	<table class="form-table">
		<tr>
			<th><label for="reports"><?php _e("Incident Reports"); ?></label></th>
			<td>
				<?php $reports = get_user_meta( $user->ID, '_wpaesm_incident_reports' );
				if( is_array( $reports[0] ) && !empty( $reports[0] ) ) {
					echo '<ul>';
					foreach( $reports[0] as $report ) {
						echo '<li><a href="' . $report . '">' . $report . '</a>';
					}
					echo '</ul>';
				}
				?>
			</td>
		</tr>
	</table>
	<?php
}