<?php

/*
 * Instructions: http://ajaxray.com/blog/store-data-to-google-sheets-using-php/#comment-68986
 * Also, be sure to share the document with the Service Account ID email address
 */

require_once plugin_dir_path( __FILE__) . 'google-api/vendor/autoload.php';

// This Sheet MUST BE shared with service account email
define('SHEET_ID', '1DnKPnGFk-leOvjJmSkf90hACDf-2IQ9pOO80y4OI7uY');
define('APPLICATION_NAME', 'Winter Camp');

define('CLIENT_SECRET_PATH', plugin_dir_path( __FILE__) . 'google-api/Winter-Camp-e9b6d6fccf3a.json');
define('ACCESS_TOKEN', 'e9b6d6fccf3ae0a6bfb121309cbc777e99a82123');
define('SCOPES', implode(' ', [Google_Service_Sheets::SPREADSHEETS]));


/* Replace XXX with your Gravity Form ID. e.g., gform_after_submission_2 for Form 2 */
add_action('gform_after_submission_8', 'employee_scheduler_add_to_google_spreadsheet', 10, 2);

function employee_scheduler_add_to_google_spreadsheet($entry, $form) {

	// Create Google API Client
	$client = new Google_Client();
	$client->setApplicationName(APPLICATION_NAME);
	$client->setScopes(SCOPES);
	$client->setAuthConfig(CLIENT_SECRET_PATH);

	$service = new Google_Service_Sheets($client);

	$options = array('valueInputOption' => 'RAW');
	$values = [
		[
			rgar($entry, '2'),                              // Camper Name
			wpaesm_funding_source( $entry ),                // Funding Source
			rgar($entry, '28'),                             // Funder
			rgar($entry, '32'),                             // Funder email
			wpaesm_get_staff_ratio( $entry ),               // ratio
			'',                     // Rate
			'',                     // Unit #
			'',                     // Total
			'',                     // Ck#
			'',                     // Inv#
			rgar($entry, '42'),     // Transportation
			'',                     // Camp
			'',                     // Notes
			'',                     // YC Approved
			'',                     // EC Approved
			'',                     // Final Approval
			wpaesm_x_the_date( rgar($entry, '38.1') ),      // 2/19/2018
			wpaesm_x_the_date( rgar($entry, '38.2') ),      // 2/20/2018
			wpaesm_x_the_date( rgar($entry, '38.3') ),      // 2/21/2018
			wpaesm_x_the_date( rgar($entry, '38.4') ),      // 2/22/2018
			wpaesm_x_the_date( rgar($entry, '38.5') ),      // 2/23/2018
			wpaesm_x_the_date( rgar($entry, '38.6') ),      // 4/9/2018
			wpaesm_x_the_date( rgar($entry, '38.7') ),      // 4/10/2018
			wpaesm_x_the_date( rgar($entry, '38.8') ),      // 4/11/2018
			wpaesm_x_the_date( rgar($entry, '38.9') ),      // 4/12/2018
			wpaesm_x_the_date( rgar($entry, '38.11') ),     // 4/13/2018
			wpaesm_x_the_date( rgar($entry, '38.12') ),     // Week 1
			wpaesm_x_the_date( rgar($entry, '38.13') ),     // Week 2
			wpaesm_x_the_date( rgar($entry, '38.14') ),     // Week 3
			wpaesm_x_the_date( rgar($entry, '38.15') ),     // Week 4
			wpaesm_x_the_date( rgar($entry, '38.16') ),     // Week 5
			wpaesm_x_the_date( rgar($entry, '38.17') ),     // Week 6
			wpaesm_x_the_date( rgar($entry, '38.18') ),     // Week 7
			wpaesm_x_the_date( rgar($entry, '38.19') ),     // Week 8
			wpaesm_x_the_date( rgar($entry, '38.21') ),     // Week 9
		],
	];
	$body   = new Google_Service_Sheets_ValueRange(['values' => $values]);

	$result = $service->spreadsheets_values->append(SHEET_ID, 'A3:S3', $body, $options);


}



function wpaesm_get_staff_ratio( $entry ) {

	$new_ratio = rgar( $entry, '36' );
	if( isset( $new_ratio ) && '' !== $new_ratio ) {
		return $new_ratio;
	}

	$returning_ratio = rgar( $entry, '37' );
	if( isset( $returning_ratio ) && '' !== $returning_ratio ) {
		return $returning_ratio;
	}

	return '';

}

function wpaesm_x_the_date( $value ) {
	if( '' == $value ) {
		return '';
	} else {
		return 'X';
	}
}

function wpaesm_funding_source( $entry ) {

	$source = array();

	if( '' !== rgar( $entry, '62.1' ) ) {
		$source[] =  rgar( $entry, '62.1' );
	}

	if( '' !== rgar( $entry, '62.2' ) ) {
		$source[] =  rgar( $entry, '62.2' );
	}

	if( '' !== rgar( $entry, '62.3' ) ) {
		$source[] =  rgar( $entry, '62.3' );
	}

	if( '' !== rgar( $entry, '62.4' ) ) {
		$source[] =  rgar( $entry, '62.4' );
	}

	if( '' !== rgar( $entry, '62.5' ) ) {
		$source[] =  rgar( $entry, '62.5' );
	}

	if( '' !== rgar( $entry, '62.6' ) ) {
		$source[] =  rgar( $entry, '62.6' );
	}

	if( '' !== rgar( $entry, '62.7' ) ) {
		$source[] =  rgar( $entry, '62.7' );
	}

	if( '' !== rgar( $entry, '62.8' ) ) {
		$source[] =  rgar( $entry, '62.8' );
	}

	if( '' !== rgar( $entry, '62.9' ) ) {
		$source[] =  rgar( $entry, '62.9' );
	}

	if( '' !== rgar( $entry, '62.10' ) ) {
		$source[] =  rgar( $entry, '62.10' );
	}

	if( !empty( $source ) ) {
		$value = implode( ', ', $source );
		return $value;
	} else {
		return '';
	}

}