<?php  // Clock-in/out text reminders

if ( ! defined( 'ABSPATH' ) ) exit;

function wpaesm_custom_cron_interval( $schedules ){
	$schedules["15min"] = array(
		'interval' => 15*60,
		'display' => __( 'Every 15 minutes' )
	);
	return $schedules;
}
add_filter( 'cron_schedules','wpaesm_custom_cron_interval' );

/**
 * Schedule cron job.
 *
 * @since 2.2.0
 */
function wpaesm_look_for_delinquents() {
	if ( ! wp_next_scheduled( 'wpaesm_late_clock_message' ) ) {
		wp_schedule_event( current_time( 'timestamp' ), '15min', 'wpaesm_late_clock_message' );
	}
}
add_action( 'init', 'wpaesm_look_for_delinquents' );

add_action( 'wpaesm_late_clock_message', 'wpaesm_find_late_clocks' );

/**
 * Look for shifts that need a text reminder to clock in/out
 */
function wpaesm_find_late_clocks() {
	$now = date( "Y-m-d", current_time( "timestamp" ) );

	// find all the shifts scheduled for today that do not have clock in and clock out times
	$args = array(
	    'post_type' => 'shift',
	    'posts_per_page' => -1,
	    'meta_query' => array(
		    'relation' => 'AND',
		    array(
			    'key'     => '_wpaesm_date',
			    'value'   => $now,
			    'compare' => '=',
		    ),
		    array(
		    	'key' => '_wpaesm_late_text_notification',
			    'compare' => 'NOT EXISTS'
		    ),
		    array(
			    'relation' => 'OR',
			    array(
				    'key' => '_wpaesm_clockin',
				    'compare' => 'NOT EXISTS',
			    ),
			    array(
				    'key' => '_wpaesm_clockout',
				    'compare' => 'NOT EXISTS',
			    ),
		    ),
	    ),


	);

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post();

			$date = get_post_meta( get_the_id(), '_wpaesm_date', true );
			$in = get_post_meta( get_the_id(), '_wpaesm_clockin', true );
			if( !isset( $in ) || '' == $in ) {
				$scheduled = strtotime( $date . get_post_meta( get_the_id(), '_wpaesm_starttime', true ) );
				$in_or_out = 'in';
			} else {
				$scheduled = strtotime( $date . get_post_meta( get_the_id(), '_wpaesm_endtime', true ) );
				$in_or_out = 'out';
			}

			$now = current_time( 'timestamp' );

			if( ( $now - $scheduled ) > 1800 ) {
				wpaesm_text_late_notification( get_the_id(), $in_or_out );
				wp_mail( 'morgan@ran.ge', 'late clock ' . $in_or_out . ' notification', get_edit_post_link( get_the_id() ) );
			}

		endwhile;
	endif;

	wp_reset_postdata();

}

/**
 * Send text notification
 *
 * @param $shift
 * @param $in_or_out
 */
function wpaesm_text_late_notification( $shift, $in_or_out ) {

	wp_mail( 'morgan@ran.ge', 'late clock ' . $in_or_out . ' notification', get_edit_post_link( $shift ) );

	$users = get_users( array(
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $shift
	) );
	foreach( $users as $user ) {
		$recipient = $user->ID;
	}

	$message = 'You were scheduled to clock ' . $in_or_out . ' more than 30 minutes ago.  Shift details: ' . get_the_permalink( $shift );

	$text = new Shiftee_Text_Notifications_Admin( 'Text Messaging', '1.0.0' );
	$text->send_text_message( $recipient, $message );

	add_post_meta( $shift, '_wpaesm_late_text_notification', current_time( 'timestamp' ) );

}
