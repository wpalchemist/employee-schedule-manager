<?php  // SCC only functions

if ( ! defined( 'ABSPATH' ) ) exit;

// Invoice report 
require_once( plugin_dir_path( __FILE__ ) . 'invoice-report.php' );
// Documentation
require_once( plugin_dir_path( __FILE__ ) . 'documentation.php' );
// Archive
require_once( plugin_dir_path( __FILE__ ) . 'archive.php' );
// Incident report
require_once( plugin_dir_path( __FILE__ ) . 'incident-report.php' );
// Clock-in reminder text messages
require_once( plugin_dir_path( __FILE__) . 'clock-cron.php' );
// Connect Gravity Forms to Google Docs
require_once( plugin_dir_path( __FILE__) . 'google-sheets.php' );
