<div class="my_meta_control" id="shiftpublish">
	<?php global $post;
	$in_series = false;
	// find out if this is a parent page or child page
	$parent = $post->post_parent;
	if( isset( $parent ) && $parent > 0 ) {
		// this is a parent post
		$in_series = true;
	}
	$args = array( 'post_parent' => $post->ID, 'post_type' => 'shift', 'posts_per_page' => -1 );
	$children = get_posts( $args );
	if( !empty( $children ) ) {
		// this post has children
		$in_series = true;
	}

	if( $in_series == true ) {
		// we are in a series, so let's make some useful buttons ?>
		<p><?php _e( '<strong>Save your changes first!</strong><br />  Click the button below to make all shifts in this series of repeating shifts match this one.  Note: this will only apply to shifts with a scheduled date later than this one.', 'wpaesm'); ?></p>
		<p><a class="submit button" id="subsequent" href="#"><?php _e( 'Update All Shifts in This Series', 'wpaesm' ); ?></a></p>
		<div id="results"></div>

		<p><?php _e( 'Click the button below to delete subsequent shifts in this series.', 'wpaesm' ); ?></p>
		<?php $message = __( 'Are you sure you want to delete all of these shifts?', 'wpaesm' );
		$url = wpaesm_get_action_url( $_GET['post'] ); ?>
		<p><a class="submit delete button" href="#" onclick="if(confirm('<?php echo $message; ?>')) { window.location='<?php echo $url; ?>'; }"><?php _e( 'Delete Subsequent Shifts In This Series', 'wpaesm' ); ?></a></p>

	<?php } else {
		// we are not in a series, so don't bother doing anything
		_e( 'This shift is not part of a repeating series.', 'wpaesm' );
	}

	?>


</div>