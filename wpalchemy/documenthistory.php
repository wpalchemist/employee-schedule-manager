<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="my_meta_control" id="shiftinfo">

<table class="form-table">
	<tr>
		<th scope="row"><label><?php _e('Date Created', 'wpaesm'); ?></label></th>
		<td>
			<input id="thisdate" class="required" type="text" size="10" name="<?php $metabox->the_name('created'); ?>" value="<?php $metabox->the_value('created'); ?>"/>
		</td>
	</tr>

	<tr>
		<th scope="row"><label><?php _e('Last Updated', 'wpaesm'); ?></label></th>
		<td>
			<input id="thisdate" class="required" type="text" size="10" name="<?php $metabox->the_name('updated'); ?>" value="<?php $metabox->the_value('updated'); ?>"/>
		</td>
	</tr>

	<tr>
		<th scope="row"><label><?php _e('Date Approved', 'wpaesm'); ?></label></th>
		<td>
			<input id="thisdate" class="required" type="text" size="10" name="<?php $metabox->the_name('approved'); ?>" value="<?php $metabox->the_value('approved'); ?>"/>
		</td>
	</tr>

	<tr>
		<th scope="row"><label><?php _e('Admin Feedback', 'wpaesm'); ?></label>
		<td>
			<?php $metabox->the_field('feedback'); ?>
			<textarea name="<?php $metabox->the_name(); ?>" rows="5"><?php $metabox->the_value(); ?></textarea>
		</td>
	</tr>


</table>
