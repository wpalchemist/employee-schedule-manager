jQuery(document).ready(function ($) {

    // apply changes to all of the shifts in this series
    $('#subsequent').on('click', function (e) {
        
        var url = shiftajax.ajaxurl;
        var shift = $('#post_ID').val();

        var data = {
            'action': 'wpaesm_apply_changes_to_series',
            'shift': shift,
        };

        $.post(url, data, function (response) {
            $("#results").html(response);

        });

    });

    var flag_ok = false;
    $('#publish').on('click', function (e) {
        // only do this if avoid_conflicts option is on
        if( "1" == shiftajax.avoid_conflicts ) {
            if ( ! flag_ok ) {
                $('#confirm-availability').show();
                e.preventDefault();
                
                var url = shiftajax.ajaxurl;
                var shift = $('#post_ID').val();
                var day = $('#thisdate').val();
                var start = $('#starttime').val();
                var end = $('#endtime').val();
                var post = $('#post_ID').val();
                // there is probably a better way to get the employee
                // also, known bug: if the employee is the currently-logged-in-user, this doesn't work
                var employeeurl = $("a[href*='user-edit.php?user_id']").attr('href');
                if (!employeeurl) {
                    employee = '';
                } else {
                    employee = employeeurl.substr(employeeurl.indexOf("=") + 1);
                }
                // console.log(employee);
                clientInfo = $('*[data-p2p_type="shifts_to_clients"]').children(".p2p-connections").attr('style');
                if ( 'display:none' === clientInfo ) {
                    client = false;
                } else {
                    client = true;
                }

                var data = {
                    'action': 'wpaesm_check_for_schedule_conflicts_before_publish',
                    'shift': shift,
                    'day': day,
                    'start': start,
                    'end': end,
                    'employee': employee,
                    'post': post,
                    'client': client
                };

                $.post(url, data, function (response) {
                    console.log(response);
                    $('#confirm-availability').hide();
                    if( response.action == 'go' ) {
                        // there aren't any scheduling conflicts, so we can publish the post
                        flag_ok = true;
                        $('#publish').trigger('click');
                    } else {
                        if (confirm(response.message)) {
                            flag_ok = true;
                            $('#publish').trigger('click');
                        } else {
                            // do nothing
                        }
                    }
                });
            }
        }

    });

});