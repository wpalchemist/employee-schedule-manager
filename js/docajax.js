jQuery(document).ready(function() {

	// employee saves new document
    jQuery('.save-document-form').on('submit', function (e) {
    	e.preventDefault();

    	thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;           
        var data = { 
 	        'action': 'wpaesm_create_new_document', // this is the name of the PHP function    
            'field_values': jQuery(this).serialize(),
 	    };
        
        jQuery.post(url, data, function (response) { 
        	jQuery(thisform).hide();
        	jQuery(thisform).closest('fieldset').children('.result').html(response);
 	    });
    });

    // employee updates existing document
    jQuery('.update-document-form').on('submit', function (e) {
    	e.preventDefault();

    	thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;           
        var data = { 
 	        'action': 'wpaesm_update_document', // this is the name of the PHP function    
            'field_values': jQuery(this).serialize(),
 	    };
        
        jQuery.post(url, data, function (response) { 
        	jQuery(thisform).hide();
        	jQuery(thisform).closest('fieldset').children('.result').html(response);
 	    });
    });

    // admin approves document
    jQuery('.approve-document-form').on('submit', function (e) {
    	e.preventDefault();

    	thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;           
        var data = { 
 	        'action': 'wpaesm_approve_document', // this is the name of the PHP function    
            'field_values': jQuery(this).serialize(),
 	    };
        
        jQuery.post(url, data, function (response) { 
        	jQuery(thisform).hide();
        	jQuery(thisform).closest('.actions').children('.needs-work-button').hide();
        	jQuery(thisform).closest('.actions').children('.response').html(response);
 	    });
    });

    // admin clicks "needs work" button
    jQuery('.show-needs-work-form').on('click', function (e) {
    	e.preventDefault();
        jQuery(this).closest('.actions').children('.approve-document-form').hide();
        jQuery(this).closest('.actions').children('.needs-work-button').hide();
    	jQuery(this).closest('.actions').children('.needs-work-form').show();
    });

    // admin sends feedback
    jQuery('.needs-work-form').on('submit', function (e) {
        e.preventDefault();

        thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;           
        var data = { 
            'action': 'wpaesm_send_document_feedback', // this is the name of the PHP function    
            'field_values': jQuery(this).serialize(),
        };
        
        jQuery.post(url, data, function (response) { 
            jQuery(thisform).hide();
            jQuery(thisform).closest('.actions').children('.response').html(response);
        });
    });

    // admin sends email reminder
    jQuery('.doc-reminder-form').on('submit', function (e) {
        e.preventDefault();

        thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;           
        var data = { 
            'action': 'wpaesm_send_document_reminder', // this is the name of the PHP function    
            'field_values': jQuery(this).serialize(),
        };
        
        jQuery.post(url, data, function (response) { 
            jQuery(thisform).hide();
            jQuery(thisform).closest('.actions').children('.doc-unnecessary-form').hide();
            jQuery(thisform).closest('.actions').children('.response').html(response);
        });
    });

    // admin says shift doesn't need documents
    jQuery('.doc-unnecessary-form').on('submit', function (e) {
        e.preventDefault();

        thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;
        var data = {
            'action': 'wpaesm_doc_unnecessary', // this is the name of the PHP function
            'field_values': jQuery(this).serialize(),
        };

        jQuery.post(url, data, function (response) {
            jQuery(thisform).hide();
            jQuery(thisform).closest('.actions').children('.doc-reminder-form').hide();
            jQuery(thisform).closest('.actions').children('.response').html(response);
        });
    });

    // admin undoes shift doesn't need documents
    jQuery(document.body).on('submit', '.undo-doc-unnecessary-form' ,function(e){
        e.preventDefault();

        thisform = jQuery(this);

        var url = wpaesmajax.ajaxurl;
        var data = {
            'action': 'wpaesm_undo_doc_unnecessary', // this is the name of the PHP function
            'field_values': jQuery(this).serialize(),
        };

        jQuery.post(url, data, function (response) {
            jQuery(thisform).hide();
            jQuery(thisform).closest('.actions').children('.doc-reminder-form').show();
            jQuery(thisform).closest('.actions').children('.response').hide();
        });
    });

});