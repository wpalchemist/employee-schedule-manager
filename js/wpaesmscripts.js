jQuery(document).ready(function() {

    if ( jQuery.isFunction(jQuery.fn.datetimepicker) ) {
        jQuery('#thisdate').datetimepicker({
            timepicker: false,
            mask: true,
            format: 'Y-m-d'
        });
        jQuery('#thisdate2').datetimepicker({
            timepicker: false,
            mask: true,
            format: 'Y-m-d'
        });
        jQuery('#repeatuntil2').datetimepicker({
            timepicker: false,
            mask: true,
            format: 'Y-m-d'
        });
        jQuery('#repeatuntil').datetimepicker({
            timepicker: false,
            mask: true,
            format: 'Y-m-d'
        });
        jQuery('#starttime').datetimepicker({
            datepicker: false,
            // mask: true,
            format: 'H:i',
            step: 15,
        });
        jQuery('#endtime').datetimepicker({
            datepicker: false,
            // mask: true,
            format: 'H:i',
            step: 15,
        });
        jQuery('.starttime').datetimepicker({
            datepicker: false,
            // mask: true,
            format: 'H:i',
            step: 15,
        });
        jQuery('.endtime').datetimepicker({
            datepicker: false,
            // mask: true,
            format: 'H:i',
            step: 15,
        });
        jQuery('#clockin').datetimepicker({
            datepicker: false,
            // mask: true,
            format: 'H:i',
            step: 15,
        });
        jQuery('#clockout').datetimepicker({
            datepicker: false,
            // mask: true,
            format: 'H:i',
            step: 15,
        });
    }

	if(jQuery("#repeat").is(':checked'))
    	jQuery("#repeatfields").show();  // checked
	else
	    jQuery("#repeatfields").hide();  // unchecked
	jQuery('#repeat').onchange = function() {
	    jQuery('#repeatfields').style.display = this.checked ? 'block' : 'none';
	};

    jQuery(document.body).on('change', '.reason' ,function(){
        if( jQuery(this).val()==="Other"){
            jQuery(".clock-other").show()
        }
        else{
            jQuery(".clock-other").hide()
        }
    });

    jQuery(document.body).on('change', '#mileage' ,function(){
        jQuery( ".where" ).show();
    });

    jQuery(document).on('click','.clock-button',function( e ){

        e.preventDefault();

        // go ahead and submit if the note field is filled out
        var reason = jQuery( '.reason' ).val();

        if (typeof reason === 'undefined') {
            var scheduled = jQuery('#scheduled').val() * 1000;
            var now_utc = Date.now();
            var date = new Date()
            var offset = date.getTimezoneOffset() * 60 * 1000;
            var now = now_utc - offset;
            var form = jQuery('#form_name').val();
            var ontime = true;

            if (( now - scheduled ) > 900000) {
                // if now is more than 15 minutes after scheduled time
                var off = 'late';
                var ontime = false;
            } else if(( scheduled - now ) > 900000) {
                // if now is more that 15 minutes before scheduled time
                var off = 'early';
                var ontime = false;
            } else {
                jQuery('form.clock-form').submit();
            }


            if( !ontime ) {
                var url = shiftajax.ajaxurl;
                var data = {
                    'action': 'wpaesm_early_or_late_form',
                    'form': form,
                    'offset' : off,
                };

                jQuery.post(url, data, function (response) {
                    jQuery(".extra-clock-fields").html(response);
                });
            }

        } else {
            jQuery('form.clock-form').submit();
        }


    });
});