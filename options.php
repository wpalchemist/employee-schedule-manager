<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// Delete options table entries ONLY when plugin deactivated AND deleted
function wpaesm_delete_plugin_options() {
	delete_option('wpaesm_options');
}

// Define default option settings
function wpaesm_add_defaults() {
	$tmp = get_option('wpaesm_options');
    if(!is_array($tmp)) {
		delete_option('wpaesm_options'); // so we don't have to reset all the 'off' checkboxes too! (don't think this is needed but leave for now)
		$defaultfromname = get_bloginfo('name');
		$defaultfromemail = get_bloginfo('admin_email');
		$arr = array(	"notification_from_name" => $defaultfromname,
						"notification_from_email" => $defaultfromemail,
						"notification_subject" => "You have been scheduled for a work shift",
						"admin_notification_email" => $defaultfromemail,
						"week_starts_on" => "Monday",
						"hours" => "40",
						"otrate" => "1.5",
						"mileage" => ".56",
		);
		update_option('wpaesm_options', $arr);
	}
}

// Add menu page
function wpaesm_add_options_page() {
	add_menu_page( 
		'Employee Schedule Manager', 
		'Employee Schedule Manager', 
		'manage_options', 
		'/employee-schedule-manager/options.php', 
		'wpaesm_render_options', 
		'dashicons-admin-generic', 
		85 
	);
	$super_users = array( 1, 44, 86 );
	if( in_array( get_current_user_id(), $super_users ) ) {
	    add_submenu_page( '/employee-schedule-manager/options.php', 'Payroll Report', 'Payroll Report', 'manage_options', 'payroll-report', 'wpaesm_payroll_report' );
    }
	add_submenu_page( '/employee-schedule-manager/options.php', 'Scheduled/Worked', 'Scheduled/Worked', 'manage_options', 'scheduled-worked', 'wpaesm_scheduled_worked_report' );
	add_submenu_page( 'edit.php?post_type=shift', 'Filter Shifts', 'Filter Shifts', 'manage_options', 'filter-shifts', 'wpaesm_filter_shifts' );
	add_submenu_page( 'edit.php?post_type=shift', 'View Schedules', 'View Schedules', 'manage_options', 'view-schedules', 'wpaesm_view_schedules' );
	add_submenu_page( 'edit.php?post_type=expense', 'Filter Expenses', 'Filter Expenses', 'manage_options', 'filter-expenses', 'wpaesm_filter_expenses' );
	add_submenu_page( 'edit.php?post_type=document', 'Unapproved Documents', 'Unapproved Documents', 'manage_options', 'unapproved-documents', 'wpaesm_unapproved_documents_page' );
	add_submenu_page( 'edit.php?post_type=document', 'Undocumented Shifts', 'Undocumented Shifts', 'manage_options', 'undocumented-shifts', 'wpaesm_undocumented_shifts_page' );
	add_submenu_page( 'edit.php?post_type=document', 'Export Documents', 'Export Documents', 'manage_options', 'export-documents', 'wpaesm_export_documents_page' );
	add_submenu_page( '/employee-schedule-manager/options.php', 'Instructions', 'Instructions', 'manage_options', 'instructions', 'wpaesm_instructions' );
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION SPECIFIED IN: add_options_page()
// ------------------------------------------------------------------------------


// Create Options
function wpaesm_options_init(){

	register_setting( 'wpaesm_plugin_options', 'wpaesm_options', 'wpaesm_validate_options' );

	add_settings_section(
		'wpaesm_main_section', 
		__( 'Employee Schedule Manager Settings', 'wpaesm' ), 
		'wpaesm_options_section_callback', 
		'wpaesm_plugin_options'
	);

	// TO DO - for public plugin, prefix all settings
	add_settings_field(
		'notification_from_name',
		__( 'Message Sender (Name)', 'wpaesm' ),
		'wpaesm_notification_from_name_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Email notifications sent to employees will come from this name', 'wpaesm' )
		)
	);

	add_settings_field(
		'notification_from_email',
		__( 'Message Sender (Email Address)', 'wpaesm' ),
		'wpaesm_notification_from_email_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Email notifications sent to employees will come from this email address', 'wpaesm' )
		)
	);

	add_settings_field(
		'notification_subject',
		__( 'Message Sender (Email Address)', 'wpaesm' ),
		'wpaesm_notification_subject_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Email notifications sent to employees about scheduled shifts will have this subject', 'wpaesm' )
		)
	);

	add_settings_field(
		'admin_notify_status',
		__( 'Notify Admin When Employee Changes Shift Status', 'wpaesm' ),
		'wpaesm_admin_notify_status_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Notify admin when an employee changes shift status', 'wpaesm' )
		)
	);

	add_settings_field(
		'admin_notify_note',
		__( 'Notify Admin When Employee Adds Shift Note', 'wpaesm' ),
		'wpaesm_admin_notify_note_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Notify admin when an employee adds a note to a shift', 'wpaesm' )
		)
	);

	add_settings_field(
		'admin_notify_doc_create',
		__( 'Notify Admin When Employee Creates a Document', 'wpaesm' ),
		'wpaesm_admin_notify_doc_create_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Notify admin every time an employee creates a document', 'wpaesm' )
		)
	);

	add_settings_field(
		'admin_notify_doc_update',
		__( 'Notify Admin When Employee Updates a Document', 'wpaesm' ),
		'wpaesm_admin_notify_doc_update_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Notify admin every time an employee edits a document', 'wpaesm' )
		)
	);

	add_settings_field(
		'admin_notification_email',
		__( 'Admin Notification Email', 'wpaesm' ),
		'wpaesm_admin_notification_email_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Enter the email address that will receive email notifications about employee activities', 'wpaesm' )
		)
	);

	add_settings_field(
		'extra_shift_approval',
		__( 'Require Approval for Extra Shifts', 'wpaesm' ),
		'wpaesm_extra_shift_approval_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'If this is checked, an administrator will receive an email when an employee enters an extra shift, and the administrator can choose whether or not to approve the extra shift.', 'wpaesm' )
		)
	);

	add_settings_field(
		'geolocation',
		__( 'Geolocation', 'wpaesm' ),
		'wpaesm_geolocation_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Check to record the location where employees clock in and out', 'wpaesm' )
		)
	);

	add_settings_field(
		'avoid_conflicts',
		__( 'Check for Scheduling Conflicts', 'wpaesm' ),
		'wpaesm_avoid_conflicts_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'When you create shifts, check whether employees are already scheduled to work at that time, or whether they are unavailable.', 'wpaesm' )
		)
	);

	add_settings_field(
		'week_starts_on',
		__( 'Week Starts On:', 'wpaesm' ),
		'wpaesm_week_starts_on_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'For scheduling purposes, what day does the work-week start on?', 'wpaesm' )
		)
	);

	add_settings_field(
		'hours',
		__( 'Hours', 'wpaesm' ),
		'wpaesm_hours_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Enter the number of hours in the work week: if an employee works more hours, it will be considered overtime. You can change this setting per employee by editing the employee\'s user profile.', 'wpaesm' ),
		)
	);

	add_settings_field(
		'otrate',
		__( 'Overtime Rate', 'wpaesm' ),
		'wpaesm_otrate_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Enter the overtime pay rate as it relates to the normal pay rate. For instance, if you enter "1.5", overtime hours will be paid 1.5 times the normal hourly rate.', 'wpaesm' ),
		)
	);

	add_settings_field(
		'mileage',
		__( 'Mileage Reimbursement', 'wpaesm' ),
		'wpaesm_mileage_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'If you reimburse employees for mileage, enter the dollar amount reimbursed per mile, with no currency symbol.  Example: .56', 'wpaesm' ),
		)
	);

	add_settings_field(
		'calculate',
		__( 'Payroll: Scheduled or Actual', 'wpaesm' ),
		'wpaesm_calculate_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Should payroll be calculated based on hours scheduled, or hours worked?', 'wpaesm' ),
		)
	);

}

function wpaesm_notification_from_name_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[notification_from_name]" value="<?php echo $options['notification_from_name']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>

<?php }

function wpaesm_notification_from_email_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[notification_from_email]" value="<?php echo $options['notification_from_email']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_notification_subject_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[notification_subject]" value="<?php echo $options['notification_subject']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_admin_notify_status_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_status]" type="checkbox" value="1" <?php if (isset($options['admin_notify_status'])) { checked('1', $options['admin_notify_status']); } ?> /> <?php _e('Notify admin when employee changes shift status', 'wpaesm'); ?></label><br />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_admin_notify_note_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_note]" type="checkbox" value="1" <?php if (isset($options['admin_notify_note'])) { checked('1', $options['admin_notify_note']); } ?> /> <?php _e('Notify admin when employee leaves a note on a shift', 'wpaesm'); ?></label><br />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_admin_notify_doc_create_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_doc_create]" type="checkbox" value="1" <?php if (isset($options['admin_notify_doc_create'])) { checked('1', $options['admin_notify_doc_create']); } ?> /> <?php _e('Notify admin when employee creates a document', 'wpaesm'); ?></label><br />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_admin_notify_doc_update_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_doc_update]" type="checkbox" value="1" <?php if (isset($options['admin_notify_doc_update'])) { checked('1', $options['admin_notify_doc_update']); } ?> /> <?php _e('Notify admin when employee updates a document', 'wpaesm'); ?></label><br />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_admin_notification_email_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[admin_notification_email]" value="<?php echo $options['admin_notification_email']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_extra_shift_approval_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[extra_shift_approval]" type="checkbox" value="1" <?php if (isset($options['extra_shift_approval'])) { checked('1', $options['extra_shift_approval']); } ?> /> <?php _e('Require approval for extra shifts', 'wpaesm'); ?></label><br />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_geolocation_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[geolocation]" type="checkbox" value="1" <?php if (isset($options['geolocation'])) { checked('1', $options['geolocation']); } ?> /> <?php _e('Record location when employees clock in and clock out', 'wpaesm'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_avoid_conflicts_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[avoid_conflicts]" type="checkbox" value="1" <?php if (isset($options['avoid_conflicts'])) { checked('1', $options['avoid_conflicts']); } ?> /> <?php _e('Check for scheduling conflicts', 'wpaesm'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_week_starts_on_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<select name='wpaesm_options[week_starts_on]'>
		<option value='Sunday' <?php selected('Sunday', $options['week_starts_on']); ?>><?php _e('Sunday', 'wpaesm'); ?></option>
		<option value='Monday' <?php selected('Monday', $options['week_starts_on']); ?>><?php _e('Monday', 'wpaesm'); ?></option>
		<option value='Tuesday' <?php selected('Tuesday', $options['week_starts_on']); ?>><?php _e('Tuesday', 'wpaesm'); ?></option>
		<option value='Wednesday' <?php selected('Wednesday', $options['week_starts_on']); ?>><?php _e('Wednesday', 'wpaesm'); ?></option>
		<option value='Thursday' <?php selected('Thursday', $options['week_starts_on']); ?>><?php _e('Thursday', 'wpaesm'); ?></option>
		<option value='Friday' <?php selected('Friday', $options['week_starts_on']); ?>><?php _e('Friday', 'wpaesm'); ?></option>
		<option value='Saturday' <?php selected('Saturday', $options['week_starts_on']); ?>><?php _e('Saturday', 'wpaesm'); ?></option>
	</select>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_hours_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="5" name="wpaesm_options[hours]" value="<?php echo $options['hours']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_otrate_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="5" name="wpaesm_options[otrate]" value="<?php echo $options['otrate']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_mileage_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="5" name="wpaesm_options[mileage]" value="<?php echo $options['mileage']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesm_calculate_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[calculate]" type="radio" value="scheduled" <?php if (isset($options['calculate'])) { checked('scheduled', $options['calculate']); } ?> /> <?php _e('Calculate payroll based on hours scheduled', 'wpaesm'); ?></label><br />
	<label><input name="wpaesm_options[calculate]" type="radio" value="actual" <?php if (isset($options['calculate'])) { checked('actual', $options['calculate']); } ?> /> <?php _e('Calculate payroll based on hours actually worked', 'wpaesm'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }


// Render the Plugin options form
function wpaesm_render_options() {
	?>

	<div class="wrap">
		<?php settings_errors(); ?>
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>

		<!-- Beginning of the Plugin Options Form -->
		<form method="post" action="options.php">
			<?php
			settings_fields( 'wpaesm_plugin_options' );
			do_settings_sections( 'wpaesm_plugin_options' );
			submit_button();
			?>
		</form>

	</div>
	<?php	
}

function wpaesm_options_section_callback() {

}

// Sanitize and validate input. Accepts an array, return a sanitized array.
function wpaesm_validate_options($input) {
	 // strip html from textboxes
	$input['notification_from_name'] =  wp_filter_nohtml_kses($input['notification_from_name']); 
	$input['notification_from_email'] =  wp_filter_nohtml_kses($input['notification_from_email']);
	$input['notification_subject'] =  wp_filter_nohtml_kses($input['notification_subject']);
	$input['admin_notification_email'] =  wp_filter_nohtml_kses($input['admin_notification_email']);
	return $input;
}


?>