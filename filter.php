<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// ------------------------------------------------------------------------
// FILTER SHIFTS                                        
// ------------------------------------------------------------------------

function wpaesm_filter_shifts() { ?>
	<div class="wrap">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h2><?php _e('Filter Shifts', 'wpaesm'); ?></h2>

		
		<form method='post' action='<?php echo admin_url( 'admin.php?page=filter-shifts'); ?>' id='filter-shifts'>
			<table class="form-table">
				<tr>
					<th scope="row"><?php _e('Select Date Range:', 'wpaesm'); ?></th>
					<td>
						<?php _e( 'From', 'wpaesm' ); ?> <input type="text" size="10" name="thisdate" id="thisdate" value="" /> 
						<?php _e( 'to', 'wpaesm' ); ?> <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Shift Type:', 'wpaesm'); ?></th>
					<td>
						<select name="type" id="type">
							<option value=""> </option>
							<?php $shifttypes = get_terms( 'shift_type', 'hide_empty=0&orderby=name' );
							foreach ( $shifttypes as $type ) { ?>
								<option value="<?php echo $type->slug; ?>" ><?php echo $type->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Shift Status:', 'wpaesm'); ?></th>
					<td>
						<select name="status" id="status">
							<option value=""> </option>
							<?php $statuses = get_terms( 'shift_status', 'hide_empty=0&orderby=name' );
							foreach ( $statuses as $status ) { ?>
								<option value="<?php echo $status->slug; ?>" ><?php echo $status->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Employee:', 'wpaesm'); ?></th>
					<td>
						<select name="employee">
							<option value=""></option>
							<?php $employees = array_merge( get_users( 'role=employee&orderby=nicename' ), get_users( 'role=administrator&orderby=nicename' ), get_users( 'role=archived&orderby=nicename' ) );
							usort( $employees, 'wpaesm_alphabetize' );
							foreach ( $employees as $employee ) { ?>
								<option value="<?php echo $employee->ID; ?>" ><?php echo $employee->display_name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Client:', 'wpaesm'); ?></th>
					<td>
						<?php $args = array( 
						    'post_type' => 'client',  
						    'posts_per_page' => -1,  
						    'orderby' => 'name',
						    'order' => 'asc',
						);

						$clientquery = new WP_Query( $args );

						if ( $clientquery->have_posts() ) : ?>
							<select name="client">
								<option value=""></option>
								<?php while ( $clientquery->have_posts() ) : $clientquery->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endwhile; ?>
							</select>
						<?php endif;
						wp_reset_postdata(); ?>
					</td>					
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Filter Shifts', 'wpaesm' ); ?>" />
			</p>
		</form>

		<?php if($_POST) { 

			if( isset( $_POST['thisdate'] ) && isset( $_POST['repeatuntil'] ) && ( $_POST['thisdate'] > $_POST['repeatuntil'] ) ) {
				$message = __( 'The end date must be after the begin date.', 'wpaesm');
				wp_die( $message );
			}

			// our basic args
			$args = array( 
				'post_type' => 'shift',
				'posts_per_page' => -1,
				);

			$criteria = '';

			// add start date and end date to args
			if( isset( $_POST['thisdate'] ) || isset( $_POST['repeatuntil'] ) ) {
				$args['meta_query'] = array();
				if( isset( $_POST['thisdate'] ) && '____-__-__' !== $_POST['thisdate'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['thisdate'],
				         'type' => 'CHAR',
				         'compare' => '>='
						);
					$criteria .= '<li>' . __( 'Begin date: ', 'wpaesm' ) . $_POST['thisdate'] . '</li>';
				}
				
				if( isset( $_POST['repeatuntil'] ) && '____-__-__' !== $_POST['repeatuntil'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['repeatuntil'],
				         'type' => 'CHAR',
				         'compare' => '<='
						);
					$criteria .= '<li>' . __( 'End date: ', 'wpaesm' ) . $_POST['repeatuntil'] . '</li>';
				}
			}

			// add shift type and shift status to args
			if( isset( $_POST['status'] ) || isset( $_POST['type'] ) ) {
				$args['tax_query'] = array(
					'relation' => 'AND',
					);
				if( isset( $_POST['status'] ) && '' !== $_POST['status'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'shift_status',
				        'field' => 'slug',
				        'terms' => $_POST['status'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Status: ', 'wpaesm' ) . $_POST['status'] . '<li>';
				}
				if( isset( $_POST['type'] ) && '' !== $_POST['type'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'shift_type',
				        'field' => 'slug',
				        'terms' => $_POST['type'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Type: ', 'wpaesm' ) . $_POST['type'] . '<li>';
				}
			}

			// add employee and client to args
			$need_additional_client_search = false;
			if( ( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) && ( isset( $_POST['client'] ) && '' !== $_POST['client'] ) ) {
				// we have both client and employee, but for now we're just going to search for employee
				if( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) {
					$args['connected_type'] = 'shifts_to_employees';
					$args['connected_items'] = $_POST['employee'];
					$criteria .= '<li>' . __( 'Employee: ', 'wpaesm' ) . $_POST['employee'] . '<li>';
				}

				// we need to search for client later
				$need_additional_client_search = true;
				$criteria .= '<li>' . __( 'Client: ', 'wpaesm' ) . $_POST['client'] . '<li>';

			} elseif( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) {
				// add employee to args
				$args['connected_type'] = 'shifts_to_employees';
				$args['connected_items'] = $_POST['employee'];
				$criteria .= '<li>' . __( 'Employee: ', 'wpaesm' ) . $_POST['employee'] . '<li>';
			} elseif( isset( $_POST['client'] ) && '' !== $_POST['client'] ) {
				// add client to args
				$args['connected_type'] = 'shifts_to_clients';
				$args['connected_items'] = $_POST['client'];
				$criteria .= '<li>' . __( 'Client: ', 'wpaesm' ) . $_POST['client'] . '<li>';
			}

			echo '<p class="criteria">' . __( 'You searched for: ', 'wpaesm' ) . '<ul>' . $criteria . '</ul></p>' ;

			
			$filter = new WP_Query( $args );
			
			// The Loop
			if ( $filter->have_posts() ) { ?>
				<table id="filtered-shifts" class="wp-list-table widefat fixed posts striped">
					<thead>
						<tr>
							<th data-sort='string'><span><?php _e( 'Shift', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Date', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Time', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Employee', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Client', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Status', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Type', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Hours Scheduled/Worked', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Documents', 'wpaesm' ); ?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php while ( $filter->have_posts() ) : $filter->the_post(); 
							$postid = get_the_id();
							global $shift_metabox;
							$meta = $shift_metabox->the_meta(); 
							// get employee associated with this shift
							$users = get_users( array(
								'connected_type' => 'shifts_to_employees',
								'connected_items' => $postid,
							) );
							if( isset( $users ) ) {
								foreach( $users as $user ) {
									$employee = $user->display_name;
									$employeeid = $user->ID;
								}
							}
							// get client associated with this shift
							$clients = get_posts( array(
								'connected_type' => 'shifts_to_clients',
								'connected_items' => $postid,
								'nopaging' => true,
								'suppress_filters' => false
							) );
							if( isset( $clients ) ) { 
								foreach($clients as $client) {
									$clientname = $client->post_title;
									$clientid = $client->ID;
								}
							}
							// do additional client filter if required 
							if( true == $need_additional_client_search ) {
								if( !isset( $clientid ) ) {
									continue;
								}
								if( intval( $clientid ) !== intval( $_POST['client'] ) ) {
									continue;
								}
							}


							// get documents associated with this shift
							$documents = get_posts( array(
								'connected_type' => 'documents_to_shifts',
								'connected_items' => $postid,
								'nopaging' => true,
								'suppress_filters' => false
							) );
							if( isset( $documents ) ) { 
								foreach($documents as $document) {
									$docname = $document->post_title;
									$docid = $document->ID;
									$docstatuses = wp_get_post_terms( $docid, 'doc_status',  array( "fields" => "names" ) );
									if( !empty( $docstatuses ) ) {
										$docstatus = 'Status: ' . implode( ', ', $docstatuses );
									}
								}
							}
							?>
							<tr>
								<td class="title">	
									<?php the_title(); ?><br />
									<a href="<?php echo get_edit_post_link(); ?>"><?php _e( 'Edit', 'wpaesm' ); ?></a> |
									<a href="<?php the_permalink(); ?>"><?php _e( 'View', 'wpaesm' ); ?></a>
								</td>
								<td class="date">
									<?php if( isset( $meta['date'] ) ) {
										echo $meta['date'];
									} ?>
								</td>
								<td class="time">
									<?php if( isset( $meta['starttime'] ) && isset( $meta['endtime'] ) ){
										echo $meta['starttime']; ?> - <?php echo $meta['endtime'];
									} ?>
								</td>
								<td class="employee">
									<?php if( isset( $employeeid ) ) { ?>
										<a href="<?php echo get_edit_user_link( $employeeid ); ?>"><?php echo $employee; ?></a>
										<?php unset( $employeeid);
									} ?>
								</td>
								<td class="client">
									<?php if( isset( $clientid ) ) { ?>
										<a href="<?php echo get_edit_post_link( $clientid ); ?>"><?php echo $clientname; ?></a>
									<?php unset( $clientid);
									} ?>
								</td>
								<td class="status">
									<?php $statuslist =  wp_get_post_terms( $postid, 'shift_status' );
									if( isset( $statuslist ) ) {
										foreach($statuslist as $status) {  ?>
											<a href="<?php echo admin_url( 'edit.php?shift_status='.$status->slug.'&post_type=shift' ); ?>"><?php echo $status->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="type">
									<?php $typelist =  wp_get_post_terms( $postid, 'shift_type' );
									if( isset( $typelist ) ) {
										foreach($typelist as $type) {  ?>
											<a href="<?php echo admin_url( 'edit.php?shift_type='.$type->slug.'&post_type=shift' ); ?>"><?php echo $type->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="hours">
									<?php if( isset( $meta['starttime'] ) && $meta['starttime'] !== '____-__-__' && isset( $meta['endtime'] ) && $meta['endtime'] !== '____-__-__') {
										$startTime = date_create( date( "H:i",strtotime( $meta['starttime'] ) ) );
										$endTime = date_create( date( "H:i",strtotime( $meta['endtime'] ) ) );
										$timeInterval=date_diff($startTime, $endTime);
										$getHours=$timeInterval->format('%h');
										$getMinutes=$timeInterval->format('%I');
										$duration=$getHours.":".$getMinutes;
										_e('Scheduled: ' . $duration . '<br />', 'wpaesm');
									} ?>
									<?php if( isset( $meta['clockin'] ) && $meta['clockin'] !== '____-__-__' && isset( $meta['clockout'] ) && $meta['clockout'] !== '____-__-__') {
										$startTime = date_create( date( "H:i",strtotime( $meta['clockin'] ) ) );
										$endTime = date_create( date( "H:i",strtotime( $meta['clockout'] ) ) );
										$timeInterval=date_diff($startTime, $endTime);
										$getHours=$timeInterval->format('%h');
										$getMinutes=$timeInterval->format('%I');
										$duration=$getHours.":".$getMinutes;
										_e('Worked: ' . $duration . '<br />', 'wpaesm');
									} ?>
								</td>
								<td class="documents">
									<?php if( isset( $docid ) ) { ?>
										<a href="<?php echo get_edit_post_link( $docid ); ?>"><?php echo $docname; ?></a><br />
										<?php if( isset( $docstatus ) ) {
											echo '<br/>' . $docstatus;
										} ?>
										<?php unset($docid);
									} ?>
								</td>
							</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			<?php } else {
				_e( 'No results found.', 'wpaesm' );
			} 
			
			// Reset Post Data
			wp_reset_postdata();

		} ?>


	</div>
<?php } 


// ------------------------------------------------------------------------
// FILTER EXPENSES                          
// ------------------------------------------------------------------------

function wpaesm_filter_expenses() { ?>
	<div class="wrap">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h2><?php _e('Filter Expenses', 'wpaesm'); ?></h2>

		
		<form method='post' action='<?php echo admin_url( 'admin.php?page=filter-expenses'); ?>' id='filter-expenses'>
			<table class="form-table">
				<tr>
					<th scope="row"><?php _e('Select Date Range:', 'wpaesm'); ?></th>
					<td>
						<?php _e( 'From', 'wpaesm' ); ?> <input type="text" size="10" name="thisdate" id="thisdate" value="" /> 
						<?php _e( 'to', 'wpaesm' ); ?> <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Expense Category', 'wpaesm'); ?></th>
					<td>
						<select name="category" id="category">
							<option value=""> </option>
							<?php $expense_categories = get_terms( 'expense_category', 'hide_empty=0&orderby=name' );
							foreach ( $expense_categories as $category ) { ?>
								<option value="<?php echo $category->slug; ?>" ><?php echo $category->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Expense Status:', 'wpaesm'); ?></th>
					<td>
						<select name="status" id="status">
							<option value=""> </option>
							<?php $statuses = get_terms( 'expense_status', 'hide_empty=0&orderby=name' );
							foreach ( $statuses as $status ) { ?>
								<option value="<?php echo $status->slug; ?>" ><?php echo $status->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Employee:', 'wpaesm'); ?></th>
					<td>
						<select name="employee">
							<option value=""></option>
							<?php $employees = array_merge( get_users( 'role=employee&orderby=nicename' ), get_users( 'role=administrator&orderby=nicename' ) );
							usort( $employees, 'wpaesm_alphabetize' );
							foreach ( $employees as $employee ) { ?>
								<option value="<?php echo $employee->ID; ?>" ><?php echo $employee->display_name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Client:', 'wpaesm'); ?></th>
					<td>
						<?php $args = array( 
						    'post_type' => 'client',  
						    'posts_per_page' => -1,  
						    'orderby' => 'name',
						    'order' => 'asc',
						);

						$clientquery = new WP_Query( $args );

						if ( $clientquery->have_posts() ) : ?>
							<select name="client">
								<option value=""></option>
								<?php while ( $clientquery->have_posts() ) : $clientquery->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endwhile; ?>
							</select>
						<?php endif;
						wp_reset_postdata(); ?>
					</td>					
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Filter Expenses', 'wpaesm' ); ?>" />
			</p>
		</form>

		<?php if($_POST) { 

			if( isset( $_POST['thisdate'] ) && isset( $_POST['repeatuntil'] ) && ( $_POST['thisdate'] > $_POST['repeatuntil'] ) ) {
				$message = __( 'The end date must be after the begin date.', 'wpaesm');
				wp_die( $message );
			}

			// our basic args
			$args = array( 
				'post_type' => 'expense',
				'posts_per_page' => -1,
				);

			$criteria = '';

			// add start date and end date to args
			if( isset( $_POST['thisdate'] ) || isset( $_POST['repeatuntil'] ) ) {
				$args['meta_query'] = array();
				if( isset( $_POST['thisdate'] ) && '____-__-__' !== $_POST['thisdate'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['thisdate'],
				         'type' => 'CHAR',
				         'compare' => '>='
						);
					$criteria .= '<li>' . __( 'Begin date: ', 'wpaesm' ) . $_POST['thisdate'] . '</li>';
				}
				
				if( isset( $_POST['repeatuntil'] ) && '____-__-__' !== $_POST['repeatuntil'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['repeatuntil'],
				         'type' => 'CHAR',
				         'compare' => '<='
						);
					$criteria .= '<li>' . __( 'End date: ', 'wpaesm' ) . $_POST['repeatuntil'] . '</li>';
				}
			}

			// add expense category and expense status to args
			if( isset( $_POST['status'] ) || isset( $_POST['category'] ) ) {
				$args['tax_query'] = array(
					'relation' => 'AND',
					);
				if( isset( $_POST['status'] ) && '' !== $_POST['status'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'expense_status',
				        'field' => 'slug',
				        'terms' => $_POST['status'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Status: ', 'wpaesm' ) . $_POST['status'] . '<li>';
				}
				if( isset( $_POST['category'] ) && '' !== $_POST['category'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'expense_type',
				        'field' => 'slug',
				        'terms' => $_POST['category'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Category: ', 'wpaesm' ) . $_POST['type'] . '<li>';
				}
			}

			// add employee and client to args
			if( ( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) || ( isset( $_POST['client'] ) && '' !== $_POST['client'] ) ) {
				$args['connected_type'] = array();
				$args['connected_items'] = array();
				if( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) {
					$args['connected_type'][] = 'expenses_to_employees';
					$args['connected_items'][] = $_POST['employee'];
					$criteria .= '<li>' . __( 'Employee: ', 'wpaesm' ) . $_POST['employee'] . '<li>';
				}

				// add client to args 
				if( isset( $_POST['client'] ) && '' !== $_POST['client'] ) {
					// this will overwrite employee?
					$args['connected_type'][] = 'expenses_to_clients';
					$args['connected_items'][] = $_POST['client'];
					$criteria .= '<li>' . __( 'Client: ', 'wpaesm' ) . $_POST['client'] . '<li>';
				}
			}

			echo '<p class="criteria">' . __( 'You searched for: ', 'wpaesm' ) . '<ul>' . $criteria . '</ul></p>' ;

			$filter = new WP_Query( $args );
			
			// The Loop
			if ( $filter->have_posts() ) { ?>
				<table id="filtered-expenses" class="wp-list-table widefat fixed posts striped">
					<thead>
						<tr>
							<th data-sort='string'><span><?php _e( 'Expense', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Date', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Employee', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Client', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Status', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Category', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Amount', 'wpaesm' ); ?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php while ( $filter->have_posts() ) : $filter->the_post(); 
							$postid = get_the_id();
							global $expense_metabox;
							$meta = $expense_metabox->the_meta(); 
							// get employee associated with this shift
							$users = get_users( array(
								'connected_type' => 'expenses_to_employees',
								'connected_items' => $postid,
							) );
							if( isset( $users ) ) {
								foreach( $users as $user ) {
									$employee = $user->display_name;
									$employeeid = $user->ID;
								}
							}
							// get client associated with this shift
							$clients = get_posts( array(
								'connected_type' => 'expenses_to_clients',
								'connected_items' => $postid,
								'nopaging' => true,
								'suppress_filters' => false
							) );
							if( isset( $clients ) ) { 
								foreach($clients as $client) {
									$clientname = $client->post_title;
									$clientid = $client->ID;
								}
							}
							?>
							<tr>
								<td class="title">	
									<?php the_title(); ?><br />
									<a href="<?php echo get_edit_post_link(); ?>"><?php _e( 'Edit', 'wpaesm' ); ?></a> |
									<a href="<?php the_permalink(); ?>"><?php _e( 'View', 'wpaesm' ); ?></a>
								</td>
								<td class="date">
									<?php if( isset( $meta['date'] ) ) {
										echo $meta['date'];
									} ?>
								</td>
								<td class="employee">
									<?php if( isset( $employeeid ) ) { ?>
										<a href="<?php echo get_edit_user_link( $employeeid ); ?>"><?php echo $employee; ?></a>
										<?php unset( $employeeid);
									} ?>
								</td>
								<td class="client">
									<?php if( isset( $clientid ) ) { ?>
										<a href="<?php echo get_edit_post_link( $clientid ); ?>"><?php echo $clientname; ?></a>
									<?php unset( $clientid);
									} ?>
								</td>
								<td class="status">
									<?php $statuslist =  wp_get_post_terms( $postid, 'expense_status' );
									if( isset( $statuslist ) ) {
										foreach($statuslist as $status) {  ?>
											<a href="<?php echo admin_url( 'edit.php?expense_status='.$status->slug.'&post_type=expense' ); ?>"><?php echo $status->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="category">
									<?php $receipt = false;
									$categorylist =  wp_get_post_terms( $postid, 'expense_category' );
									if( isset( $categorylist ) ) {
										foreach($categorylist as $category ) {  
											if( 'receipt' == $category->slug ) {
												$receipt = true;
												// probably also need to check if receipt is the parent
											} ?>
											<a href="<?php echo admin_url( 'edit.php?expense_category='.$category->slug.'&post_type=expense' ); ?>"><?php echo $category->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="amount">
									<?php if( isset( $meta['amount'] ) ) {
										if( true == $receipt ) {
											echo "$";
										}
										echo $meta['amount'];
										unset( $receipt );
									} ?>
								</td>
							</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			<?php } else {
				_e( 'No results found.', 'wpaesm' );
			} 
			
			// Reset Post Data
			wp_reset_postdata();

		} ?>


	</div>
<?php } 

?>